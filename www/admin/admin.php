<?php

use Framework\Router;
use Framework\Http\Request;


//设置时区
date_default_timezone_set('Asia/Shanghai');

//设置项目根目录
define('ROOT', realpath(__DIR__.'/../../').'/');

//引入自动加载
include ROOT.'backend/include/vendor'.DIRECTORY_SEPARATOR.'autoload.php';

if (ENVIRONMENT === 'dev') {
    Router::run(function (Request $request) {
        switch ((int) $_SERVER['SERVER_PORT']) {
            case 8002:
                return 'api';
                break;
            case 8080:
                return 'admin';
                break;
            default:
                return 'admin';
                break;
        }
    });
} else {
    Router::run(function (Request $request) {
        $host = $request->host();
        return substr($host, 0, strpos($host, '.'));
    });
}







