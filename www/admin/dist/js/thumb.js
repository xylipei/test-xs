
(function($){



    var test = function (options, $this) {

        var $url = 'http://p70yxa8jz.bkt.clouddn.com/';

        var $options = $.extend({}, {
            action: 'test',
            inputName: 'thumb',
            thumb: {},
            aspectRatio: 5/3,
            minWidth: 100,
            minHeight: 100
        }, $.isPlainObject(options) && options);

        console.log($options);

        var $cropperOptions = {
            aspectRatio: $options.aspectRatio,
            dragMode: 'move',
            movable: false,
            zoomable: false,
            zoomOnTouch: false,
            zoomOnWheel: false,
            toggleDragModeOnDblclick: false,
            minCropBoxWidth: 50
        };

        //console.log($cropperOptions);

        var $URL = window.URL || window.webkitURL;
        var $uploadedImageURL;
        var $cropper = null;


        var main = {
            unsetInput: function () {
                $this.find('.input').html('');
            },
            setInput: function (data) {
                var params = $.extend({}, {
                    uri: "",
                    w: 0,
                    h: 0,
                    ave: "",
                    mime: ""
                }, $.isPlainObject(data) && data);
                var html = '';
                $.each(params, function(name, value){
                    html+= '<input name="'+$options.inputName+'['+name+']" type="hidden" value="'+value+'">';
                });
                $this.find('.input').html(html);
            },

            init: function () {
                var url = '/dist/css/250x150.png';
                if (!$.isEmptyObject($options.thumb)) {
                    url = $url+$options.thumb.uri;
                }
                var html = '<div class="thumb" style="position: relative;">';
                    html += '<div class="thumb-cropper" style="position: absolute; z-index: 10000;"><img src=""></div>';
                    html += '<div class="input hidden"></div>';
                    html += '<label style="cursor: pointer;">';
                    html += '<img width="250" height="150" class="preview" src="'+url+'">';
                    html += '<input class="sr-only" type="file" accept="image/jpg,image/jpeg,image/png,image/gif">';
                    html += '</label>';
                    html += '</div>';
                $this.html(html);

                $cropper = $($this.selector+' .thumb-cropper img');

                if (!$.isEmptyObject($options.thumb)) {
                    this.setInput($options.thumb);
                }
                this.bindEvents();
            },

            bindEvents: function () {
                var self = this;
                $this.on('change', 'input[type=file]', function() {
                    var files = this.files;
                    var file;
                    if (files && files.length) {
                        file = files[0];
                        self.onChange(file);
                    } else {
                        window.alert('请选个文件.');
                    }
                });

                $cropper.parent().on('click', '.btn-save', function () {
                    $(this).attr('disabled', 'disabled');
                    self.onSave();
                });
            },

            cropperInit: function () {
                if ($cropper.data('cropper')) {
                    console.log('cropper init 已经初始化过了');
                    return;
                } else {
                    console.log('cropper.init');
                }
                $cropper.on({
                    ready: function (e) {
                        $('.cropper-view-box').html('<span style="z-index: 1000; position: absolute; left: 50%; top: 50%; margin: -15px 0 0 -35px;" class="btn btn-sm btn-primary btn-save">保存图片</span>')
                    },
                    cropstart: function (e) {
                        console.log(e.type, e.action);
                    },
                    cropmove: function (e) {
                        console.log(e.type, e.action);
                    },
                    cropend: function (e) {
                        console.log(e.type, e.action);
                    },
                    crop: function (e) {
                        if (e.width < $options.minWidth || e.height < $options.minHeight) {
                            $cropper.parent().find('.btn-save').attr('disabled', 'disabled');
                        } else {
                            $cropper.parent().find('.btn-save').removeAttr('disabled');
                        }
                        console.log(e.type, e.x, e.y, e.width, e.height, e.rotate, e.scaleX, e.scaleY);
                    },
                    zoom: function (e) {
                        console.log(e.type, e.ratio);
                    }
                }).cropper($cropperOptions);
            },
            cropperReload: function () {
                console.log('cropper.reload');
                $cropper.cropper('destroy').attr('src', $uploadedImageURL).cropper($cropperOptions);
            },

            cropperShow: function () {
                console.log('cropper.show');
                $cropper.parent().removeClass('hide');
            },

            cropperHide: function () {
                console.log('cropper.hide');
                $cropper.parent().addClass('hide');
            },

            onChange: function (file) {
                this.cropperInit();
                this.cropperShow();
                this.unsetInput();
                if (!$cropper.data('cropper')) {
                    alert('裁切异常 刷新页面重试');
                    return;
                }

                if (/^image\/\w+$/.test(file.type)) {
                    if ($uploadedImageURL) {
                        $URL.revokeObjectURL($uploadedImageURL);
                    }
                    $uploadedImageURL = $URL.createObjectURL(file);
                    this.cropperReload();
                } else {
                    alert('请选择图片文件');
                }
            },

            onSave: function () {
                console.log('onSave');
                var self = this;
                var thumb = $cropper.cropper('getCroppedCanvas').toDataURL('image/png');
                var base64 = thumb.substring(23);
                var md5 = SparkMD5.hash($.base64.decode(base64)).toLowerCase();
                var key = $options.action+'/'+md5+'.png';
                var qiniu = window.location.protocol.split(':')[0] == 'https' ? 'https://upload.qbox.me' : 'http://upload.qiniu.com';

                $.post('/upload', {name: key, action: $options.action}, function(response){
                    if (response.status == 0) {
                        $.ajax({
                            type: 'POST',
                            url: qiniu + '/putb64/-1/key/' + $.base64.encode(key),
                            data: base64,
                            beforeSend: function(XMLHttpRequest) {
                                XMLHttpRequest.setRequestHeader('Content-Type', 'application/octet-stream');
                                XMLHttpRequest.setRequestHeader('Authorization', 'UpToken ' + response.token);
                            },
                            dataType: 'json',
                            success: function(response) {
                                $this.find('.preview').attr('src', $url+response.uri);
                                self.setInput(response);
                                console.log(response);

                                $cropper.parent().find('.btn-save').removeAttr('disabled');
                                self.cropperHide();
                            },
                            error: function(XMLHttpRequest, msg) {
                                alert('上传错误! 请重新保存!');
                                $cropper.parent().find('.btn-save').removeAttr('disabled');
                                self.cropperHide();
                            }
                        });
                    } else {
                        alert(response.msg);
                    }
                }, 'json');
            },
        };
        main.init();
    };

    $.fn.thumb = function (options) {
        new test(options, $(this));
    };
}(jQuery));