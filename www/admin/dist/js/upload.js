
(function($){

    var uploader = function (options, $this) {

        var $url = 'http://p70yxa8jz.bkt.clouddn.com/';

        var $options = $.extend({}, {
            accept: "*/*"
        }, $.isPlainObject(options) && options);

        var main = {

            init: function () {
                var html = '';
                    html += '<input class="sr-only" type="file" accept="'+$options.accept+'">';
                $this.after(html);
                this.bindEvents();
            },

            bindEvents: function () {
                var self = this;
                $this.click(function(){
                    $this = $(this);
                    $this.parent().find('input[type=file]').click();
                });

                $this.parent().on('change', 'input[type=file]', function() {
                    var files = this.files;
                    var file;
                    if (files && files.length) {
                        file = files[0];
                        self.onChange(file);
                    } else {
                        window.alert('请选个文件.');
                    }
                });
            },
            checkExt: function (ext)
            {
                if (typeof($options.allowExt) != 'undefined') {
                    return $options.allowExt.indexOf(ext) != -1;
                } else {
                    return true;
                }
            },

            onUpload: function (result) {
                $this.parent().parent().find('input[type=text]').val(result.url);
            },
            onChange: function (file) {
                var self = this;
                var name = file.name;
                var ext = name.substr(name.lastIndexOf(".")+1).toLowerCase();
                if (!self.checkExt(ext)) {
                    alert('文件类型错误');
                    return;
                }

                fileMd5(file, function(err, md5) {
                    if (md5) {
                        $.post('/upload', {name: md5+'.'+ext}, function (response) {
                            if (response.status == 0) {
                                var title = $this.html();
                                var i = 0;
                                qiniu.upload(file, response.result.name, response.result.token, {}, {}).subscribe({
                                    error: function (result) {
                                        alert('上传失败');
                                    },
                                    next: function (result) {
                                        i++;
                                        if (i % 2 == 1) {
                                            $this.css('color', '#FFD700');
                                        }
                                        if (i % 2 == 0) {
                                            $this.css('color', '#FF4500');
                                        }
                                    },
                                    complete: function (result) {
                                        self.onUpload(result);
                                        $this.css('color', '#006400');
                                    }
                                });
                            } else {
                                alert('上传失败');
                            }
                        });
                    } else {
                        alert('上传失败');
                    }
                });
            },
        };
        main.init();
    };

    $.fn.uploader = function (options) {
        new uploader(options, $(this));
    };
}(jQuery));