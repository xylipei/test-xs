
$.fn.insertAtCaret = function(text) {
    return this.each(function() {
        if (document.selection && this.tagName == 'TEXTAREA') {
            //IE textarea support
            this.focus();
            sel = document.selection.createRange();
            sel.text = text;
            this.focus();
        } else if (this.selectionStart || this.selectionStart == '0') {
            //MOZILLA/NETSCAPE support
            startPos = this.selectionStart;
            endPos = this.selectionEnd;
            scrollTop = this.scrollTop;
            this.value = this.value.substring(0, startPos) + text + this.value.substring(endPos, this.value.length);
            this.focus();
            this.selectionStart = startPos + text.length;
            this.selectionEnd = startPos + text.length;
            this.scrollTop = scrollTop;
        } else {
            // IE input[type=text] and other browsers
            this.value += text;
            this.focus();
            this.value = this.value;    // forces cursor to end
        }
    });
};

$.fn.moveCaretToEnd = function() {
    return this.each(function() {
        var len = this.value.length;
        if (document.selection && this.tagName == 'TEXTAREA') {
            //IE textarea support
            this.focus();
            sel = document.selection.createRange();
            sel.moveStart('character',len);
            sel.collapse();
            sel.select();

        } else if (this.selectionStart || this.selectionStart == '0') {
            this.selectionStart = this.selectionEnd = len;
        } else {
            this.focus();
            this.value = this.value;
        }
    });
};


$.fn.extend({
    textareaAutoHeight: function() {
        this.init = function() {
            $(this).keyup(this.resetHeight).change(this.resetHeight).focus(this.resetHeight);
        };
        this.resetHeight = function() {
            if (!$.browser.msie) {
                $(this).height(0);
            }
            var h = parseFloat(this.scrollHeight);
            $(this).height(h).scrollTop(h);
            $(this).css("overflow-y", "hidden");
        };
        this.init();
    }
});



(function($){
    var manager = function (options, $this) {

        var $options = $.extend({}, {
            content_id: '',
            container: $('body'),
            textarea: $('textarea'),
            data: {},
            allowedFormats: {
                'image': ['png', 'jpeg', 'jpg', 'gif']
            },
        }, $.isPlainObject(options) && options);
        var $textarea = $options.textarea;
        var $container = $this;
        var $attachmentListContainer;
        var $data = {};
        var $dataLength = 0;

        var $formats = {};


        var main = {
            init: function () {
                var self = this;
                var template = this.template().toString().split('\n').slice(1,-1).join('\n');
                $options.container.html(template);
                $('select.mime').selectpicker({
                    style: 'btn btn-xs btn-info'
                });
                $attachmentListContainer = $container.find('tbody.attachment-list');

                $.each($options.allowedFormats, function(mime, exts) {
                    if (exts.length > 0) {
                        $.each(exts, function (i, ext) {
                            if ($formats.hasOwnProperty(ext)) {
                                $formats[ext].push(mime);
                            } else {
                                $formats[ext] = new Array();
                                $formats[ext].push(mime);
                            }
                        });
                    }
                });

                $.each($options.data, function (i, item) {
                    self.addData(item);
                });

                this.initUploader();
                this.bindEvents();
                this.updateTextArea();
                $textarea.moveCaretToEnd();

            },


            addData: function (item) {
                $data[item.id] = item;
                ++$dataLength;
                this.renderList();
            },

            insertData: function(id) {
                if ($data.hasOwnProperty(id)) {
                    var text = "\n"+'[attachment]['+$data[id].id+']['+$data[id].name+']'+"\n";
                    $textarea.insertAtCaret(text);
                }
            },

            updateData: function(id, field, value) {
                var self = this;
                if ($data.hasOwnProperty(id)) {
                    if ($data[id][field] !== value) {
                        var post = {};
                        post['id'] = id;
                        post[field] = value;
                        $.post('/board/content/attachment/update', post, function (response) {
                            if (response.status === 0) {
                                $data[id][field] = value;
                                self.renderList();
                            } else {
                                alert(response.msg);
                            }
                        }, 'json');
                    }
                }
            },

            updateTextArea: function () {

                var self = this;

                var text = $textarea.val();
                var insertedAttachmentUniqueIds = [];
                var insertedAttachmentIds = text.match(/\[attachment\]\[([a-zA-Z0-9]{24})\]/g);
                if (insertedAttachmentIds !== null) {
                    insertedAttachmentIds.map(function(str) {
                        var id = str.substr(13, 24);
                        if (!insertedAttachmentUniqueIds.includes(id)) {
                            insertedAttachmentUniqueIds.push(id);
                        }
                    });
                }

                $.each($data, function(i, item){
                    if (insertedAttachmentUniqueIds.includes(item.id)) {
                        self.updateData(item.id, 'inserted', true);
                    } else {
                        self.updateData(item.id, 'inserted', false);
                    }
                });

                if (!$.browser.msie) {
                    $textarea.height(0);
                }
                var h = parseFloat($textarea.prop('scrollHeight'));
                $textarea.height(h).scrollTop(h);
                $textarea.css("overflow-y", "hidden");
            },

            bindEvents: function () {
                var self = this;
                $(document).on('click', '.attachment-insert', function(){
                    var id = $(this).data('id');
                    self.insertData(id);
                });


                $(document).on('change', 'select.attachment-edit-mime', function(){
                    var id = $(this).data('id');
                    var mime = $(this).val();
                    self.updateData(id, 'mime', mime);
                });

                $textarea.keyup(function(){
                    self.updateTextArea();
                }).change(function(){
                    self.updateTextArea();
                }).focus(function(){
                    self.updateTextArea();
                });
            },

            renderList: function () {
                if ($dataLength > 0) {
                    var html = '';
                    $.each($data, function (i, item) {

                        var ext = 'unknown';
                        if (item.hasOwnProperty('name')) {
                            ext = item.name.split('.').slice(-1)[0];
                        }

                        if (!$formats.hasOwnProperty(ext)) {
                            alert('格式不支持:'+ext);
                        }

                        html += '<tr>';
                        html += '<td>' + item.name + '</td>';
                        html += '<td>' + item.id + '</td>';
                        html += '<td>' + item.mime + '</td>';
                        html += '<td>';

                        if ($formats[ext].length === 1) {
                            html += '<select data-id="'+item.id+'" disabled class="mime attachment-edit-mime">';
                        } else {
                            html += '<select data-id="'+item.id+'" class="mime attachment-edit-mime">';
                        }

                        $.each($formats[ext], function (i, name) {
                            var mime = name+'/'+ext;
                            if (item.mime === mime) {
                                html += '<option selected value="' + mime + '">';
                            } else {
                                html += '<option value="' + mime + '">';
                            }
                            switch (name) {
                                case 'audio':
                                    html += '音频';
                                    break;
                                case 'video':
                                    html += '视频';
                                    break;
                                case 'image':
                                    html += '图片';
                                    break;
                            }
                            html += '</option>';
                        });

                        html += '</select>';

                        html += '</td>';
                        if (item.inserted) {
                            html += '<td>已插入</td>';
                        } else {
                            html += '<td>未插入</td>';
                        }
                        html += '<td>';
                        html += '<span class="btn btn-xs btn-primary attachment-insert" data-id="'+item.id+'">插入</span>';
                        html += '</td>';
                        html += '</tr>';
                    });

                    $attachmentListContainer.html(html);

                    $('select.mime').selectpicker({
                        style: 'btn btn-xs btn-info',
                        width: 'fit'
                    }).selectpicker('refresh');
                }
            },

            initUploader: function () {
                var self = this;
                var uploader = new qq.FineUploader({
                    element: document.getElementById("uploader"),
                    autoUpload: true,
                    request: {
                        endpoint: '/board/content/attachment/add',
                        inputName: 'file',
                        params: {
                            'content_id': $options.content_id
                        }
                    },
                    classes: {
                        success: "bg-success",
                        fail: "bg-danger"
                    },
                    text: {
                        failUpload: "上传失败"
                    },
                    validation: {
                        allowedExtensions: Object.keys($formats)
                    },
                    callbacks: {
                        onComplete: function (id, name, response, xhr) {
                            if (response.success) {
                                response.attachment.name = name;
                                response.attachment.inserted = false;
                                self.addData(response.attachment);
                                uploader.getItemByFileId(id).remove();
                            }
                        }
                    }
                });
            },

            template: function () {
                return function () {/*
                <div class="panel panel-info">
                        <div class="panel-heading">
                            附件列表
                        </div>
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="bg-warning">
                                    <td>文件名</td>
                                    <td class="col-lg-1">ID</td>
                                    <td>MIME</td>
                                    <td>类型</td>
                                    <td>状态</td>
                                    <td>操作</td>
                                </tr>
                            </thead>
                            <tbody class="attachment-list">
                                <tr>
                                    <td colspan="6" style="text-align:center;">
                                    😄😄😄😄😄😄😄😄😄😄
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="uploader"></div>


                    <div id="qq-template" class="hide">
                        <div class="panel panel-info qq-uploader-selector qq-uploader">
                            <div class="panel-heading">
                                <div class="qq-upload-button-selector qq-upload-button">
                                    <button class="btn btn-primary btn-xs">上传附件</button>
                                </div>
                            </div>
                            <table class="table table-bordered">
                                <tbody class=" qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
                                <tr>
                                    <td>
                                    <span class="qq-progress-bar-container-selector">
                                        <span role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></span>
                                    </span>
                                    </td>
                                    <td>
                                        <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                                        <span class="qq-upload-file-selector qq-upload-file"></span>
                                        <span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Edit filename"></span>
                                    </td>
                                    <td>
                                        <span class="qq-upload-size-selector qq-upload-size"></span>
                                    </td>
                                    <td>
                                        <span class="btn btn-xs btn-primary qq-upload-cancel-selector qq-upload-cancel">取消</span>
                                        <span class="btn btn-xs btn-primary qq-upload-retry-selector qq-upload-retry">重试</span>
                                        <span class="btn btn-xs btn-primary qq-upload-delete-selector qq-upload-delete">删除</span>
                                    </td>
                                    <td>
                                        <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <td>类型</td>
                                        <td>支持后缀</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>图片</td>
                                        <td>jpg、png、webp、gif、bmp</td>
                                    </tr>
                                    <tr>
                                        <td>音频</td>
                                        <td>3gp、mp4、m4a、flac、mp3、mkv、wav、ogg、gsm、aac</td>
                                    </tr>
                                    <tr>
                                        <td>视频</td>
                                        <td>3gp、mp4、mkv</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                */}
            }

        };

        main.init();
    };

    $.fn.attachmentManager = function (options) {
        new manager(options, $(this));
    };

}(jQuery));