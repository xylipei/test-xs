(function($){


    var test = function (options, $this) {
        var $options = $.extend({}, {
            boards: [],
            categorys: [],
            board_id: '',
            category_id: ''
        }, $.isPlainObject(options) && options);


        var main = {

            bindEvents: function () {
                var self = this;
                $this.on('change', 'select.board', function() {
                    var board_id = $(this).val();
                    $options.board_id = board_id;
                    $options.category_id = '';
                    self.render();
                });

                $this.on('change', 'select.category', function() {
                    var category_id = $(this).val();
                    $options.category_id = category_id;
                    self.render();
                });
            },

            render: function () {
                this.renderBoards();
                this.renderCategorys();
                this.bindEvents();
                $('select.board-select').selectpicker('render');
            },

            renderBoards: function () {
                var html = '';
                html += '<div class="pull-left board-box">';
                html += '<select class="board board-select" name="board_id">';
                html += '<option value="">请选择所属社区</option>';

                $.each($options.boards, function(i, board) {

                    var disabled = board.hasOwnProperty('status') && board.status === 'accepted' ? '' : 'disabled';

                    if ($options.board_id == board._id) {
                        html += '<option '+disabled+' selected value="' + board._id + '">' + board.name + '</option>';
                    } else {
                        html += '<option '+disabled+' value="' + board._id + '">' + board.name + '</option>';
                    }
                });
                html += '</select>';
                html += '</div>';
                html += '<div class="col-lg-2 category-box"></div>';

                $this.html(html);
            },

            renderCategorys: function () {
                var board_id = $options.board_id;
                var html = '';
                if ($options.categorys.hasOwnProperty(board_id)) {
                    html += '<select name="category_id" class="category board-select">';
                    html += '<option value="">请选择分类</option>';
                    $.each($options.categorys[board_id], function (i, category) {

                        var disabled = category.hasOwnProperty('status') && category.status === 'accepted' ? '' : 'disabled';

                        var model = '没有模型';

                        if (category.hasOwnProperty('model')) {
                            switch (category.model) {
                                case 'mix':
                                    model = '图文混排';
                                    break;
                                case 'album':
                                    model = '图片集';
                                    break;
                                case 'video':
                                    model = '视频模式'
                                    break;
                                default:
                                    break;
                            }
                        }

                        if ($options.category_id == category._id) {
                            html += '<option '+disabled+' selected value="' + category._id + '">' + category.name + ' : '+model+ '</option>';
                        } else {
                            html += '<option '+disabled+'  value="' + category._id + '">' + category.name + ' : '+model+'</option>';
                        }
                    });
                    html += '</select>';
                }

                $this.find('div.category-box').html(html);
            },
        };

        main.render();
    };

    $.fn.boardselector = function (options) {
        console.log('aaaaa');
        new test(options, $(this));
    };

}(jQuery));