<?php

use Framework\Router;
use Framework\Http\Request;

define('ROOT', realpath(__DIR__.'/../../').'/');
include ROOT.'backend/include/vendor'.DIRECTORY_SEPARATOR.'autoload.php';




if (ENVIRONMENT === 'dev') {
    Router::run(function (Request $request) {
        switch ((int) $_SERVER['SERVER_PORT']) {
            case 8002:
                return 'api';
                break;
            case 8080:
                return 'admin';
                break;
            default:
                return 'api';
                break;
        }
    });
} else {
    Router::run(function (Request $request) {
        $host = $request->host();
        return substr($host, 0, strpos($host, '.'));
    });
}



