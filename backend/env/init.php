<?php

use Respect\Validation\Validator;

date_default_timezone_set('Asia/Shanghai');
define('ENVIRONMENT', strtolower($_SERVER['ENVIRONMENT'] ?? 'DEV'));
define('UPLOAD', 'http://175.6.5.122:8010/');
define('USER_NOT_START', 1000);
define('USER_NOT_LOGIN', 1001);
define('USER_NOT_LOGOUT', 1002);
define('USER_NOT_BALANCE', 1003);
define('NEED_UPDATE', 1004);
define('USER_NOT_VIP', 1005);
define('EVERY_DAY_BALANCE', 100);
define('CHAPTER_PRICE', 19);

function ip() {
    $ip = '';
    if (isset($_SERVER)) {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
    } else {
        if (getenv("HTTP_X_FORWARDED_FOR")) {
            $ip = getenv( "HTTP_X_FORWARDED_FOR");
        } elseif (getenv("HTTP_CLIENT_IP")) {
            $ip = getenv("HTTP_CLIENT_IP");
        } else {
            $ip = getenv("REMOTE_ADDR");
        }
    }

    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_RES_RANGE | FILTER_FLAG_NO_PRIV_RANGE)) {
        return $ip;
    }

    return false;
}


define('BOOK_CONTENT_PRICE', 15);

Validator::with('Framework\\Validator\\', true);

