<?php

return [
    'default' => [
        'uri' => 'mongodb://127.0.0.1:27017/admin',
        'database' => 'xs',
    ],

    'admin' => [
        'uri' => 'mongodb://127.0.0.1:27017/admin',
        'database' => 'xs_admin',
    ],
];
