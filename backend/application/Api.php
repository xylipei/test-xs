<?php

namespace Application;

use Framework\Application;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Json;
use Framework\Middleware;
use Framework\Middleware\Handler;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use Composer\Semver\Comparator;

use Model\Api\User as ModelUser;
use Model\Admin\Package as ModelPackage;


class Api extends Application
{
    public function setApplicationMiddleware(Middleware $middleware) : void
    {
        $middleware->add(function (Request $request, Handler $next) {
            $handler = $request->attribute()->get('handlerName');
            $method = strtolower($request->method());
            if (!class_exists($handler) || !method_exists($handler, $method)) {
                return Json::error('NOT FOUND');
            }
            $request->attribute()->set('handler', new $handler);
            return $next->handle($request);
        });

        $middleware->add(function (Request $request, Handler $next) : Response {
            if (($request->attribute()->get('handler')->isApi ?? true)) {
                try {
                    Validator::x()->assert($request->x());
                } catch (NestedValidationException $exception) {
                    return Json::error('公共参数错误');
                }
                if (ENVIRONMENT == 'production' || (bool) $request->x('encrypt')) {
                    Json::setAes($request->x('sign'));
                }
            }

            $version = $request->x('version');
            $src = $request->x('src');
            $package_id = $request->x('package_id');
            $package = ModelPackage::findOne([
                'src_id' => new ObjectId($src),
                '_id' => new ObjectId($package_id)
            ]);

            if (!empty($package['version'])) {
                unset($package['keystore'], $package['_id']);
                if (Comparator::greaterThan($package['version'], $version)) {
                    return Json::error('must update!', NEED_UPDATE, [
                        'version' => $package['version'] ?? '',
                        'download' => UPLOAD.$package['download'] ?? '',
                        'sign' => $package['sign'] ?? '',
                        'package_name' => $package['package_name'] ?? ''
                    ]);
                }
            }
            return $next->handle($request);
        });


        $middleware->add(function (Request $request, Handler $next) : Response {
            $handler = $request->attribute()->get('handler');
            $accessRole = $handler->{$request->method().'AccessRole'} ?? null;
            $accessRole = $accessRole === null ? ($handler->accessRole ?? ['started']) : $accessRole;
            $session = $request->x('session') ?: new ObjectId();
            if (in_array('none', $accessRole)) {
                return $next->handle($request);
            }
            if ($accessRole){
                $user = ModelUser::findOneAndUpdate(
                    [
                        'session' => new ObjectID($session)
                    ],
                    [
                        '$set' => [
                            'active.last' => new UTCDateTime(),
                        ],
                        '$inc' => [
                            'active.num' => 1
                        ]
                    ]
                );
                if (empty($user)){
                    return Json::error('请登录后访问', USER_NOT_LOGIN);
                }
                $request->setUser($user);
            }
            return $next->handle($request);
        });
    }
}




