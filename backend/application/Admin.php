<?php
namespace Application;

use Application\Admin\Me\Authenticator;
use Application\Admin\Me\Authenticator\Valid;
use Application\Admin\Me\Password;
use Framework\Application;
use Framework\DB;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;

use Framework\Middleware;
use Framework\Middleware\Handler;

use Framework\View;
use MongoDB\BSON\ObjectId;
use Respect\Validation\Validator;

use Model\Admin\Account as ModelAccount;
use Model\Admin\History\Action as ModelAction;


class Admin extends Application
{
    public function setApplicationMiddleware(Middleware $middleware): void
    {
        $middleware->add(function (Request $request, Handler $next) : Response {
            $handlerName = $request->attribute()->get('handlerName');
            $method = $request->method();

            if (!class_exists($handlerName) || !method_exists($handlerName, $method)) {
                return Html::error('NOT FOUND', '页面不存在', '/');
            }

            $request->attribute()->set('handler', new $handlerName);
            return $next->handle($request);
        });

        $middleware->add(function (Request $request, Handler $next) {

            if (($request->attribute()->get('handler')->isPublic ?? false)) {
                return $next->handle($request);
            } else {
                $session = $request->cookie()->get('session');

                if (Validator::id()->validate($session)) {
                    $session = new ObjectId($session);
                    $user = ModelAccount::findOne(['session' => $session]);

                    if (!empty($user)) {

                        ModelAction::new([
                            'account_id' => new ObjectId($user['_id']),
                            'session' => $session,
                            'action' => strtolower($request->attribute()->get('handlerName'))
                        ]);


                        if (($user['status'] ?? 'disabled') !== 'ok') {
                            return Html::error('账号已禁用', '请联系管理员', '/me/forbidden');
                        }

                        $request->setUser($user);
                        View::bindGlobal('_user', $user);

                        //检查是否要强制修改密码
                        if ((time() - ($user['password']['last_reset_time'] ?? 0)) > 30 * 86400 || ($user['password']['must_reset'] ?? false)) {
                            if (!$request->attribute()->get('handler') instanceof Password) {
                                return Html::error('请修改密码', '密码长时间未修改', '/me/password');
                            }
                        } elseif (($user['authenticator']['bind'] ?? false) === false) {
                            if (!$request->attribute()->get('handler') instanceof Authenticator) {
                                return Html::error('开启二步验证', '请绑定二步验证确保安全', '/me/authenticator');
                            }
                        } elseif ((time() - ($user['authenticator']['last_valid_time'] ?? 0)) > 86400 || ($user['authenticator']['must_valid'] ?? false)) {
                            if (!$request->attribute()->get('handler') instanceof Valid) {
                                return Html::error('需要二步验证', '请输入二步验证码确保账号安全', '/me/authenticator/valid');
                            }
                        }

                        //检查是否有操作权限
                        $roles = $request->attribute()->get('handler')->isRole ?? [];
                        if (!empty($roles) && !in_array((($user['role'] ?? 'none')), $roles)) {
                            return Html::error('没有权限', '没有权限访问此页面');
                        } else {
                            return $next->handle($request);
                        }
                    }
                }

                return Html::error('尚未登录', '前往登录页面', '/me/login');
            }
        });

    }

    public function get(Request $request) : Response
    {
        $html = $request->html();
        $html->db = DB::instance()->all();
        return $html;
    }
}
