<?php


namespace Application\Admin;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\View\Pagination;
use Framework\Middleware;
use Framework\Middleware\Handler;
use Framework\Http\Response\Html;
use Respect\Validation\Validator;
use Respect\Validation\Exceptions\NestedValidationException;
use MongoDB\BSON\ObjectId;

use Model\Api\Book as ModelBook;
use Model\Api\Book\Category as ModelBookCategory;

class Book
{
    public function setGetMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next): Response {
            $validator = Validator::keySet(
                Validator::key('category_id', Validator::oneOf(Validator::id(), Validator::equals('')), false),
                Validator::key('status', Validator::in(['', 'draft', 'accepted', 'trashed', 'deleted']), false),
                Validator::key('vip', Validator::in(['', 0, 1]), false),
                Validator::key('free', Validator::in(['', 0, 1]), false),
                Validator::key('author', Validator::oneOf(Validator::stringType(), Validator::equals('')), false),
                Validator::key('search', Validator::stringType(), false),
                Validator::key('sort', Validator::in(['', 'create_time', 'num.view', 'num.comment']), false),
                Validator::key('page', Validator::numeric(), false)
            );

            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function get(Request $request): Response
    {
        $html = $request->html();
        $sort = [
            'update_time' => -1
        ];
        $params = ['category_id' => '', 'author' => '', 'status' => '', 'search' => '', 'sort' => 'create_time', 'vip' => '', 'free' => ''];
        $params = $html->params = array_replace($params, $request->data());
        $params = array_filter($params);
        $filter = [];
        if (!empty($params)) foreach ($params as $name => $value) {
            switch ($name) {
                case 'category_id':
                    $filter[$name] = new ObjectId($value);
                    break;
                case 'status':
                    $filter[$name] = $value;
                    break;
                case 'search':
                    $filter['title'] = ['$regex' => $value];
                    break;
                case 'author':
                    $filter['author'] = ['$regex' => $value];
                    break;
                case 'vip':
                case 'free':
                    $filter[$name] =  (int) $value;
                    break;
                case 'sort':
                    $sort = [$value => -1];
                    break;
            }
        }
        $html->categorys = ModelBookCategory::find([])->toArray();
        $html->count = ModelBook::count($filter);
        $html->pagination = new Pagination($html->count, 50);
        $html->books = ModelBook::findWithJoin($filter,[
            'sort' => $sort,
            'skip' => $html->pagination->skip(),
            'limit' => $html->pagination->limit()
        ]);
        return $html;
    }
}