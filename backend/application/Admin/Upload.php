<?php

namespace Application\Admin;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Raw;

class Upload
{
    public function post(Request $request) : Response
    {
        $file = $_FILES['file'] ?? null;
        if (empty($file['name']) || empty($file['tmp_name']) || (!isset($file['error']) || $file['error'] != 0)) {
            return $this->error('上传失败');
        }

        $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
        if (!in_array($ext, ['jpeg', 'jpg', 'png','mp4'])) {
            return $this->error('文件不允许');
        }

        $name = md5_file($file['tmp_name']).'.'.$ext;
        $dir = 'upload/'.date('Ym').'/';
        $new = ROOT.'www/'.$dir.$name;
        $img_dir = date('Ym').'/';


        if (!is_dir(ROOT.'www/'.$dir)) {
            mkdir(ROOT.'www/'.$dir, 0777, true);
        }

        if (move_uploaded_file($file['tmp_name'], $new)) {
            return $this->success(UPLOAD.$img_dir.$name);
        }

        return $this->error();
    }


    public function success($url)
    {
        return Raw::success(json_encode(['error' => 0, 'url' => $url]));
    }

    public function error($message = '上传失败')
    {
        return Raw::error(json_encode(['error' => 1, 'message' => $message]));
    }
}