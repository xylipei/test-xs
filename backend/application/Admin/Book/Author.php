<?php


namespace Application\Admin\Book;

use Framework\Http\Request;
use Framework\View\Pagination;

use Model\Api\Book\Author as ModelAuthor;

class Author
{
    public function get(Request $request)
    {
        $html = $request->html();
        $name = $request->data('author');
        $filter = [];
        if (!empty($name)){
            $filter['name'] = ['$regex' => $name];
        }
        $html->name = $name;
        $html->count = ModelAuthor::count($filter);
        $html->pagination = new Pagination($html->count, 50);
        $html->authors = ModelAuthor::find($filter,[
            'sort' => [
                'initials' => 1
            ],
            'skip' => $html->pagination->skip(),
            'limit' => $html->pagination->limit()
        ])->toArray();
        return $html;
    }
}