<?php
namespace Application\Admin\Book\Ranking;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\Middleware\Handler;

use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use Overtrue\Pinyin\Pinyin;

use Model\Api\Book as ModelBook;



class Edit
{

    public $isRole = ['administrator'];

    public function get(Request $request)
    {
        $html = $request->html();
        $book_id = $request->data('book_id');
        $html->book = ModelBook::findOne(['_id' => new ObjectId($book_id)]);
        return $html;
    }

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next): Response {
            $validator = Validator::keySet(
                Validator::key('book_id', Validator::id(), true),
                Validator::key('sort', Validator::numeric(), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request): Response
    {
        $book_id = new ObjectId($request->data('book_id'));
        $sort = $request->data('sort');
        $ranking = ModelBook::findOneAndUpdate(
            [
                '_id' => new ObjectId($book_id),
                'status' => 'accepted',
            ],
            [
                '$set' => [
                    'ranking.sort' => (int) $sort,
                ]
            ]
        );
        if ($ranking) {
            ModelBook::updateMany(
                [
                    '_id' => [
                        '$nin' => [ new ObjectId($book_id) ]
                    ],
                    'status' => 'accepted',
                    'ranking' => [
                        '$exists' => true
                    ],
                    'ranking.sort' => [
                        '$gte' => (int) $sort
                    ]
                ],
                [
                    '$inc' => [
                        'ranking.sort' => 1
                    ]
                ]
            );
            return Html::success('修改成功', '前往风云榜', '/book/ranking');
        } else {
            return Html::error('修改失败', '返回重新输入');
        }
    }
}