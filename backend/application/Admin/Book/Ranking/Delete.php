<?php


namespace Application\Admin\Book\Ranking;

use Framework\Http\Request;
use Framework\Middleware;
use Framework\Middleware\Handler;
use Framework\Http\Response;
use MongoDB\BSON\ObjectId;
use Respect\Validation\Validator;
use Respect\Validation\Exceptions\NestedValidationException;
use Framework\Http\Response\Html;

use Model\Api\Book as ModelBook;
use Model\Api\Book\Chapter as ModelBookChapter;

class Delete
{
    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next): Response {
            $validator = Validator::keySet(
                Validator::key('book_id', Validator::id(), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request)
    {
        $book_id = $request->data('book_id');
        $book = ModelBook::findOne(
            [
                '_id' => new ObjectId($book_id),
                'ranking' => [
                    '$exists' => true
                ]
            ]
        );
        if (empty($book)) {
            return Html::error('不存在','返回重新选择');
        }
        $book_status = ModelBook::findOneAndUpdate(
            [
                '_id' => new ObjectId($book_id)
            ],
            [
                '$unset' => [
                    'ranking' => ''
                ]
            ],
            [
                'returnDocument' => 2,
                'upsert' => true
            ]
        );
        if (empty($book_status)){
            return Html::error('操作失败','返回重新操作');
        }
        ModelBook::updateMany(
            [
                '_id' => [
                    '$nin' => [ new ObjectId($book_id) ]
                ],
                'status' => 'accepted',
                'ranking' => [
                    '$exists' => true
                ],
                'ranking.sort' => [
                    '$gte' => (int) $book['ranking']['sort']
                ]
            ],
            [
                '$inc' => [
                    'ranking.sort' => -1
                ]
            ]
        );
        return Html::success('操作成功', '返回风云榜','/book/ranking');
    }
}