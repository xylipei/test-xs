<?php


namespace Application\Admin\Book;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\View\Pagination;

use MongoDB\BSON\ObjectId;

use Model\Api\Book as ModelBook;
use Model\Api\Book\Chapter as ModelBookChapter;

class Ranking
{
    public function get(Request $request): Response
    {
        $html = $request->html();
        $title = $request->data('title');
        $filter['ranking'] = [
            '$exists' => true
        ];
        if (!empty($title)){
            $filter['title'] = ['$regex' => $title];
        }
        $html->title = $title;
        $html->count = ModelBook::count($filter);
        $html->pagination = new Pagination($html->count, 50);
        $html->books = ModelBook::find($filter,[
            'sort' => [
                'ranking.sort' => 1
            ],
            'skip' => $html->pagination->skip(),
            'limit' => $html->pagination->limit()
        ])->toArray();
        return $html;
    }
}