<?php


namespace Application\Admin\Book;

use Framework\Http\Response;
use Framework\Http\Response\Html;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDateTime;

use Model\Api\Book as ModelBook;
use Model\Api\Book\Chapter as ModelChapter;


class Upset
{
    public $isRole = ['administrator', 'editor'];

    public function get():Response
    {
        $books = ModelBook::find([])->toArray();
        foreach ($books as $book ){
            $new_chapter = ModelChapter::newChapter($book['_id']);
            $upset = ModelBook::findOneAndUpdate(
                [
                    '_id' =>  new ObjectId($book['_id'])
                ],
                [
                    '$set' =>[
//                        'create_time' => new UTCDateTime(ModelBook::randomDate(date('Y-m-d H:i:s ',(time()-365 * 86400)),date('Y-m-d H:i:s ',time()),false)* 1000)
                        'content_update_time' => new UTCDateTime()
                    ]
                ]
            );
            if (empty($upset)){
                return Html::error('乱序失败', '返回重新点击');
            }
        }
        return Html::success('乱序成功', '前往漫画列表', '/book');
    }
}
