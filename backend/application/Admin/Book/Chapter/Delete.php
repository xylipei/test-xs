<?php


namespace Application\Admin\Book\Chapter;

use Framework\Http\Request;
use Framework\Middleware;
use Framework\Middleware\Handler;
use Framework\Http\Response;
use MongoDB\BSON\ObjectId;
use Respect\Validation\Validator;
use Respect\Validation\Exceptions\NestedValidationException;
use Framework\Http\Response\Html;

use Model\Api\Book as ModelBook;
use Model\Api\Book\Chapter as ModelBookChapter;

class Delete
{
    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next): Response {
            $validator = Validator::keySet(
                Validator::key('chapter_id', Validator::id(), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request)
    {
        $chapter_id = $request->data('chapter_id');
        $chapter = ModelBookChapter::findOne(
            ['_id' => new ObjectId($chapter_id)]
        );
        if (empty($chapter)) {
            return Html::error('章节不存在','返回重新选择');
        }
        if ($chapter['status'] != 'draft'){
            return Html::error('请先保存草稿，再做删除操作','返回重新选择');
        }
        $chapter_status = ModelBookChapter::findOneAndUpdate(
            [
                '_id' => new ObjectId($chapter_id)
            ],
            [
                '$set' => [
                    'status' => 'deleted'
                ]
            ],
            [
                'returnDocument' => 2,
                'upsert' => true
            ]
        );
        if (empty($chapter_status)){
            return Html::error('操作失败','返回重新操作');
        }
        return Html::success('操作成功', '返回章节列表','/book/chapter?book_id='.$chapter_status['book_id']);
    }
}