<?php


namespace Application\Admin\Book\Chapter;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Middleware;
use Framework\Middleware\Handler;
use Framework\Http\Response\Html;
use Respect\Validation\Validator;
use Respect\Validation\Exceptions\NestedValidationException;
use MongoDB\BSON\ObjectId;

use Model\Api\Book\Chapter as ModelBookChapter;
use Model\Api\Book as ModelBook;

class Status
{
    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next): Response {
            $validator = Validator::keySet(
                Validator::key('chapter_id', Validator::id(), true),
                Validator::key('status', Validator::in(['trashed', 'accepted']), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新修改');
            }
        });
    }

    public function post(Request $request)
    {
        $chapter_id = new ObjectId($request->data('chapter_id'));
        $status = $request->data('status');
        $chapter = ModelBookChapter::findOneAndUpdate(
            ['_id' => $chapter_id],
            [
                '$set' => [
                    'status' => $status
                ]
            ]
        );
        if ($chapter) {
            if($chapter['status'] == 'accepted'){
                ModelBook::findOneAndUpdate(['_id' => new ObjectId($chapter['book_id'])],['$inc' => ['num.chapter' => -1]]);
            }
            if($status == 'accepted'){
                ModelBook::findOneAndUpdate(['_id' => new ObjectId($chapter['book_id'])],['$inc' => ['num.chapter' => 1]]);
            }
            return Html::success('修改成功', '返回分类列表', '/book/chapter/edit?chapter_id='.$request->data('chapter_id'));
        }
        return Html::error('修改失败', '返回重新修改','/book/chapter/edit?chapter_id='.$request->data('chapter_id'));
    }
}