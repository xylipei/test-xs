<?php


namespace Application\Admin\Book\Chapter;

use Framework\Middleware;
use Framework\Http\Request;
use Framework\Middleware\Handler;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Http\Response\Raw;
use Respect\Validation\Validator;
use Respect\Validation\Exceptions\NestedValidationException;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime;


use Model\Api\Book\Chapter\Content as ModelContent;
use Model\Api\Book\Chapter as ModelChapter;


class Content
{
    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next): Response {
            $validator = Validator::keySet(
                Validator::key('content_id',Validator::id(),true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request): Response
    {
        $content_id = new ObjectId($request->data('content_id'));
        $content = ModelContent::findOne(['_id' => $content_id]);
        if (empty($content)){
            return $this->error();
        }
        $chapter = ModelChapter::findOne(['content_id' => $content_id]);
        $content['title'] = $chapter['title'];
        return $this->success($content);
    }

    public function success($data)
    {
        return Raw::success(json_encode(['error' => 0, 'content' => $data]));
    }

    public function error($message = '获取失败')
    {
        return Raw::error(json_encode(['error' => 1, 'message' => $message]));
    }
}