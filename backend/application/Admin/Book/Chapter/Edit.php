<?php


namespace Application\Admin\Book\Chapter;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\Middleware\Handler;
use Respect\Validation\Validator;
use Respect\Validation\Exceptions\NestedValidationException;
use MongoDB\BSON\UTCDateTime;
use MongoDB\BSON\ObjectId;

use Model\Api\Book as ModelBook;
use Model\Api\Book\Chapter as ModelBookChapter;
use Model\Api\Book\Chapter\Content as ModelBookChapterContent;


class Edit
{
    public function get(Request $request)
    {
        $html = $request->html();
        $chapter_id = $request->data('chapter_id');
        $chapter = ModelBookChapter::findOne([
            '_id' => new ObjectId($chapter_id)
        ]);
        if (empty($chapter)) {
            return Html::error('非法ID', '返回重新选择则');
        }
        $html->chapter = $chapter;
        $html->content = ModelBookChapterContent::findOne([
            'chapter_id' => new ObjectId($chapter_id)
        ]);
        return $html;
    }

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next): Response {
            $validator = Validator::keySet(
                Validator::key('chapter_id', Validator::id(), true),
                Validator::key('content_id', Validator::id(), true),
                Validator::key('title', Validator::stringType(), true),
                Validator::key('content', Validator::stringType(), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request): Response
    {
        $chapter_id = new ObjectId($request->data('chapter_id'));
        $content_id = new ObjectId($request->data('content_id'));
        $content = $request->data('content');
        $content_content = ModelBookChapterContent::findOneAndUpdate(
            [
                '_id' => $content_id
            ],
            [
                '$set' => [
                    'content' => $content
                ]
            ],
            [
                'returnDocument' => 2,
                'upsert' => true
            ]
        );
        if (empty($content_content)){
            return Html::error('修改失败', '返回重新输入');
        }
        $params['title'] = $request->data('title');
        $params['update_time'] = new UTCDateTime();
        $params['num']['char'] = mb_strlen($content);
        $params['status'] = 'draft';
        if ($chapter = ModelBookChapter::findOneAndUpdate(['_id' => $chapter_id], ['$set' => $params])) {
            if($chapter['status'] == 'accepted'){
                ModelBook::findOneAndUpdate(['_id' => new ObjectId($chapter['book_id'])],['$inc' => ['num.chapter' => -1]]);
            }
            return Html::success('修改成功', '前往章节列表', '/book/chapter?book_id=' . $chapter['book_id']);
        } else {
            return Html::error('修改失败', '返回重新输入');
        }
    }
}