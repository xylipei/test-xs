<?php

namespace Application\Admin\Book\Chapter;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Http\Response\Raw;

use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime;

use Model\Api\Book as ModelBook;
use Model\Api\Book\Chapter as ModelBookChapter;
use Model\Api\Book\Chapter\Content as ModelBookChapterContent;



class Add
{
    public $isRole = ['administrator', 'editor'];

    public function post(Request $request) : Response
    {
        $file = $_FILES['file'] ?? null;
        if (empty($file['name']) || empty($file['tmp_name']) || (!isset($file['error']) || $file['error'] != 0)) {
            return $this->error('上传失败');
        }
        $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
        if ($ext != 'txt') {
            return $this->error('文件不允许');
        }
        $book_id = $request->data('book_id');
        $free_chapter_num = (int) (($request->data('free_chapter_num')) ?? 200);
        $txt = @file_get_contents($file['tmp_name']);
        if (empty($txt)){
            return $this->error('内容为空');
        }
        $chapter['num']['char'] = mb_strlen($txt);
        $filename = basename($file['name'], '.txt');
        $name = explode('-', $filename,2);
        $chapter['title'] = $name[1];
        $chapter['book_id'] = new ObjectId($book_id);
        $chapter['sort'] = (int) $name[0];
        $chapter['free'] = 0;
        if ($free_chapter_num > $chapter['sort']){
            $chapter['free'] = 1;
        }
        $chapter['status'] = 'draft';
        $chapter['create_time'] = new UTCDateTime();
        $chapter['update_time'] = new UTCDateTime();
        $chapter_id = ModelBookChapter::insertOne($chapter);
        if (empty($chapter_id)){
            return $this->error();
        }
        $content['content'] = $txt;
        $content['chapter_id'] = new ObjectId($chapter_id);
        $content['book_id'] = new ObjectId($book_id);
        $content_id = ModelBookChapterContent::insertOne($content);
        if (empty($content_id)){
            return $this->error();
        }
        $chapter_update = ModelBookChapter::findOneAndUpdate(
            [
                '_id' => $content['chapter_id']
            ],
            [
                '$set' => [
                    'content_id' => new ObjectId($content_id)
                ]
            ],
            [
                'returnDocument' => 2,
                'upsert' => true
            ]
        );
        if (!empty($chapter_update)){
            ModelBook::findOneAndUpdate(
                [
                    '_id' => new ObjectId($book_id)
                ],
                [
                    '$set' => [
                        'content_update_time' => $chapter['create_time']
                    ]
                ]
            );
            return $this->success($chapter_update);
        }
        return $this->error();
    }


    public function success($data)
    {
        return Raw::success(json_encode(['error' => 0, 'chapter' => $data]));
    }

    public function error($message = '上传失败')
    {
        return Raw::error(json_encode(['error' => 1, 'message' => $message]));
    }

}