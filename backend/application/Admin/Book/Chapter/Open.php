<?php


namespace Application\Admin\Book\Chapter;


use Framework\Http\Request;
use Framework\Http\Response\Html;
use MongoDB\BSON\ObjectId;

use Model\Api\Book\Chapter as ModelChapter;
use Model\Api\Book as ModelBook;

class Open
{
    public function get(Request $request)
    {
        $book_id = $request->data('book_id');
        $chapter = ModelChapter::updateMany(
            [
                'book_id' => new ObjectId($book_id)
            ],
            [
                '$set' => [
                    'status' => 'accepted'
                ]
            ]
        );
        if (empty($chapter)){
            return Html::error('公开失败','返回重新操作','/book/chapter?book_id=' . $book_id);
        }
        ModelBook::findOneAndUpdate(
            [
                '_id' => new ObjectId($book_id)
            ],
            [
                '$inc' => [
                    'num.chapter' => $chapter
                ]
            ]
        );
        return Html::success('公开成功','返回章节列表','/book/chapter?book_id=' . $book_id);
    }
}