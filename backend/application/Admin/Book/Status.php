<?php


namespace Application\Admin\Book;

use Framework\Http\Request;
use Framework\Middleware;
use Framework\Middleware\Handler;
use Framework\Http\Response;
use MongoDB\BSON\ObjectId;
use Respect\Validation\Validator;
use Respect\Validation\Exceptions\NestedValidationException;
use Framework\Http\Response\Html;

use Model\Api\Book as ModelBook;
use Model\Api\Book\Category as ModelBookCategory;
use Model\Api\Book\Chapter as ModelBookChapter;
use Model\Api\Book\Author as ModelBookAuthor;
use Model\Api\Book\Bookshelf as ModelBookshelf;


class Status
{
    public $isRole = ['administrator'];

    public function setGetMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next): Response {
            $validator = Validator::keySet(
                Validator::key('book_id', Validator::id(), true),
                Validator::key('status', Validator::in(['accepted', 'draft', 'trashed']), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function get(Request $request)
    {
        $status = $request->data('status');
        $book_id = $request->data('book_id');
        $book = ModelBook::findOne(
            ['_id' => new ObjectId($book_id)]
        );
        if (empty($book)) {
            return Html::error('小说不存在','返回重新选择');
        }
        if ($status == 'accepted'){
            $chapter_count = ModelBookChapter::count([
                'book_id' => new ObjectId($book_id),
                'status' => 'accepted'
            ]);
            if ($chapter_count == 0){
                return Html::error('没有已发布章节','请先发布章节');
            }
        }
        $book_status = ModelBook::findOneAndUpdate(
            ['_id' => new ObjectId($book_id)],
            [
                '$set' => [
                    'status' => $status
                ]
            ],
            [
                'returnDocument' => 2
            ]
        );
        if (empty($book_status)){
            return Html::error('操作失败','返回重新操作');
        }
        if($book['status'] == 'accepted' && $book_status['status'] != 'accepted'){
            ModelBookAuthor::findOneAndUpdate(
                ['_id' => new ObjectId($book_status['author_id'])],
                ['$inc' => ['num.book' => -1]]
            );
            ModelBookCategory::findOneAndUpdate(
                ['_id' => new ObjectId($book_status['category_id'])],
                ['$inc' => ['num.book' => -1]]
            );
            ModelBookshelf::updateMany(
                [
                    'book_id' => new ObjectId($book_id)
                ],
                [
                    '$set' => [
                        'status' => 'draft'
                    ]
                ]
            );
        }
        if($book['status'] != 'accepted' && $book_status['status'] == 'accepted'){
            ModelBookAuthor::findOneAndUpdate(
                ['_id' => new ObjectId($book_status['author_id'])],
                ['$inc' => ['num.book' => 1]]
            );
            ModelBookCategory::findOneAndUpdate(
                ['_id' => new ObjectId($book_status['category_id'])],
                ['$inc' => ['num.book' => 1]]
            );
            ModelBookshelf::updateMany(
                [
                    'book_id' => new ObjectId($book_id)
                ],
                [
                    '$set' => [
                        'status' => 'accepted'
                    ]
                ]
            );
        }
        return Html::success('操作成功', '返回章节管理','/book/chapter?book_id='.$book_id);
    }
}