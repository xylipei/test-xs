<?php


namespace Application\Admin\Book\Category;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\Middleware\Handler;


use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

use Model\Api\Book\Category as ModelBookCategory;



class Edit
{
    public $isRole = ['administrator'];

    public function setGetMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next): Response {
            $validator = Validator::keySet(
                Validator::key('category_id', Validator::id(), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function get(Request $request)
    {
        $html = $request->html();
        $category_id = $request->data('category_id');
        $html->category = ModelBookCategory::findOne([
            '_id' => new ObjectId($category_id)
        ]);
        return $html;
    }

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next): Response {
            $validator = Validator::keySet(
                Validator::key('category_id',Validator::id(),true),
                Validator::key('title', Validator::nickname(), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request): Response
    {
        $category_id = new ObjectId($request->data('category_id'));
        $params['title'] = $request->data('title');
        $is_exist = ModelBookCategory::findOne([
            'title' => $params['title'],
            '_id' => [
                '$ne' => $category_id
            ]
        ]);
        if (!empty($is_exist)){
            return Html::error('分类已存在', '返回重新输入', '/book/category');
        }
        $params['update_time'] = new UTCDateTime();
        $params['status'] = 'draft';
        if (ModelBookCategory::findOneAndUpdate(['_id' => $category_id],['$set' => $params])) {
            return Html::success('修改成功', '前往分类列表', '/book/category');
        } else {
            return Html::error('修改失败', '返回重新输入');
        }
    }
}