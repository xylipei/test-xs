<?php

namespace Application\Admin\Book;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\Middleware\Handler;

use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use Overtrue\Pinyin\Pinyin;

use Model\Api\Book as ModelBook;
use Model\Api\Book\Category as ModelBookCategory;
use Model\Api\Book\Author as ModelBookAuthor;

class Add
{
    public $isRole = ['administrator'];

    public function get(Request $request)
    {
        $html = $request->html();
        $html->categorys = ModelBookCategory::find(['status' => 'accepted'])->toArray();
        $html->authors = ModelBookAuthor::find([], [
            'sort' => [
                'update_time' => -1
            ]
        ])->toArray();
        return $html;
    }

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next): Response {
            $validator = Validator::keySet(
                Validator::key('title', Validator::nickname(), true),
                Validator::key('category', Validator::stringType()->notEmpty(), true),
                Validator::key('author', Validator::nickname(), true),
                Validator::key('memo', Validator::stringType()->notEmpty(), true),
                Validator::key('thumb', Validator::regex('/[0-9a-z\/\.]/'), true),
                Validator::key('finish', Validator::in([0, 1]), true),
                Validator::key('vip', Validator::in([0, 1]), true),
                Validator::key('free', Validator::in([0, 1]), true),
                Validator::key('free_chapter_num', Validator::numeric(), true),
                Validator::key('chapter_balance_price', Validator::numeric(), true),
                Validator::key('file', Validator::stringType(), false)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request): Response
    {
        $params = $request->data();unset($params['file']);
        $params['author'] = preg_replace("/[\s ]+/", '', $params['author']);
        $pinyin = new Pinyin();
        $initials = $pinyin->abbr($params['author'], PINYIN_KEEP_ENGLISH);
        $initials = strtoupper($initials);
        $initials = $initials[0];
        $author = ModelBookAuthor::findOneAndUpdate(
            ['name' => $request->data('author')],
            [
                '$set' => [
                    'update_time' => new UTCDateTime()
                ],
                '$setOnInsert' => [
                    'name' => $params['author'],
                    'initials' => $initials,
                    'create_time' => new UTCDateTime(),
                    'num.book' => 0,
                    'num.view' => 0
                ]
            ],
            [
                'returnDocument' => 2,
                'upsert' => true
            ]
        );

        $category = ModelBookCategory::findOneAndUpdate(
            ['title' => $request->data('category')],
            [
                '$set' => [
                    'update_time' => new UTCDateTime(),
                    'thumb' => $params['thumb']
                ],
                '$setOnInsert' => [
                    'title' => $request->data('category'),
                    'create_time' => new UTCDateTime(),
                    'num.book' => 0,
                    'status' => 'draft'
                ]
            ],
            [
                'returnDocument' => 2,
                'upsert' => true
            ]
        );
        $params['finish'] = isset($params['finish']) ? (int)$params['finish'] : 1;
        $params['free_chapter_num'] = isset($params['free_chapter_num']) ? (int)$params['free_chapter_num'] : 200;
        $params['vip'] = isset($params['vip']) ? (int)$params['vip'] : 1;
        $params['free'] = isset($params['free']) ? (int)$params['free'] : 1;
        $params['chapter_balance_price'] = isset($params['chapter_balance_price']) ? (int)$params['chapter_balance_price'] : 1;
        $params['author'] = $author['name'];
        $params['author_id'] = new ObjectId($author['_id']);
        $params['category'] = $category['title'];
        $params['category_id'] = new ObjectId($category['_id']);
        $params['num'] = ['chapter' => 0];
        $params['create_time'] = new UTCDateTime();
        $params['update_time'] = new UTCDateTime();
        $params['status'] = 'draft';
        if ($book_id = ModelBook::insertOne($params)) {
            return Html::success('添加成功', '前往章节管理', '/book/chapter?book_id=' . $book_id);
        } else {
            return Html::error('添加失败', '返回重新输入');
        }
    }
}