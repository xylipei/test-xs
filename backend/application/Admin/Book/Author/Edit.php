<?php

namespace Application\Admin\Book\Author;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\Middleware\Handler;

use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use Overtrue\Pinyin\Pinyin;

use Model\Api\Book\Author as ModelBookAuthor;

class Edit
{
    public $isRole = ['administrator'];

    public function setGetMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next): Response {
            $validator = Validator::keySet(
                Validator::key('author_id', Validator::id(), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function get(Request $request)
    {
        $html = $request->html();
        $category_id = $request->data('author_id');
        $html->author = ModelBookAuthor::findOne([
            '_id' => new ObjectId($category_id)
        ]);
        return $html;
    }

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next): Response {
            $validator = Validator::keySet(
                Validator::key('author_id',Validator::id(),true),
                Validator::key('name', Validator::nickname(), true),
                Validator::key('is_hot', Validator::in([0, 1]),true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request): Response
    {
        $params = $request->data(); unset($params['author_id']);
        $author_id = new ObjectId($request->data('author_id'));
        $is_exist = ModelBookAuthor::findOne([
            'name' => $params['name'],
            '_id' => [
                '$ne' => $author_id
            ]
        ]);
        if (!empty($is_exist)){
            return Html::error('用户已存在', '返回重新输入', '/book/author');
        }
        $params['is_hot'] = (int) $params['is_hot'];
        $pinyin = new Pinyin();
        $initials = $pinyin->abbr($params['name'], PINYIN_KEEP_ENGLISH);
        $initials = strtoupper($initials);
        $params['initials'] = $initials[0];
        $params['update_time'] = new UTCDateTime();
        if (ModelBookAuthor::findOneAndUpdate(['_id' => $author_id],['$set' => $params])) {
            return Html::success('修改成功', '前往作者列表', '/book/author');
        } else {
            return Html::error('修改失败', '返回重新输入');
        }
    }
}