<?php

namespace Application\Admin\Book;

use Framework\Http\Request;
use Framework\Middleware;
use Framework\Middleware\Handler;
use Framework\Http\Response;
use MongoDB\BSON\ObjectId;
use Respect\Validation\Validator;
use Respect\Validation\Exceptions\NestedValidationException;
use Framework\Http\Response\Html;

use Model\Api\Book\Category as ModelCategory;

class Category
{
    public $isRole = ['administrator'];

    public function get(Request $request)
    {
        $html = $request->html();
        $html->categorys = ModelCategory::find([])->toArray();
        return $html;
    }

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request,Handler $next) {
            $validator = Validator::keySet(
                Validator::key('id',  Validator::arrayVal()->each(
                    Validator::id()
                ), true),
                Validator::key('sort', Validator::arrayVal()->each(
                    Validator::numeric()
                ), true)
            );

            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request)
    {
        $params = $request->data();
        if (!empty($params['id']) && count($params['id']) == count($params['sort'])) {
            foreach ($params['id'] as $k => $id) {
                ModelCategory::updateOne(
                    ['_id' => new ObjectId($id)],
                    ['$set' => ['sort' => (int) ($params['sort'][$k] ?? PHP_INT_MAX)]]
                );
            }
        }
        return Html::success('修改成功', '前往分类列表', '/book/category');
    }
}