<?php


namespace Application\Admin\Book;

use Framework\Http\Request;
use Framework\Middleware;
use Framework\Middleware\Handler;
use Framework\Http\Response;
use MongoDB\BSON\ObjectId;
use Respect\Validation\Validator;
use Respect\Validation\Exceptions\NestedValidationException;
use Framework\Http\Response\Html;

use Model\Api\Book as ModelBook;

class Delete
{
    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next): Response {
            $validator = Validator::keySet(
                Validator::key('book_id', Validator::id(), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request)
    {
        $book_id = $request->data('book_id');
        $book = ModelBook::findOne(
            ['_id' => new ObjectId($book_id)]
        );
        if (empty($book)) {
            return Html::error('小说不存在','返回重新选择');
        }
        if ($book['status'] != 'draft'){
            return Html::error('请先保存草稿，再做删除操作','返回重新选择');
        }
        $book_status = ModelBook::findOneAndUpdate(
            [
                '_id' => new ObjectId($book_id)
            ],
            [
                '$set' => [
                    'status' => 'deleted'
                ]
            ],
            [
                'returnDocument' => 2,
                'upsert' => true
            ]
        );
        if (empty($book_status)){
            return Html::error('操作失败','返回重新操作');
        }
        return Html::success('操作成功', '返回小说列表','/book');
    }
}