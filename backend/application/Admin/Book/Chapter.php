<?php


namespace Application\Admin\Book;

use Framework\Http\Request;
use Framework\Http\Response\Html;

use MongoDB\BSON\ObjectId;

use Model\Api\Book as ModelBook;
use Model\Api\Book\Chapter as ModelBookChapter;

class Chapter
{

    public function get(Request $request)
    {
        $html = $request->html();
        $book_id = $request->data('book_id');
        $book = ModelBook::findOne(['_id' => new ObjectId($book_id)]);
        if (empty($book)){
            return Html::error('非法ID', '返回重新选择', '/book');
        }
        if ($book['status'] == 'deleted'){
            return Html::error('该小说为删除状态，请先保存为草稿', '返回重新选择', '/book');
        }
        $html->book = $book;
        $html->chapters = ModelBookChapter::find(
            [
                'book_id' => new ObjectId($book_id)
            ],
            [
                'sort' => [
                    'sort' => 1
                ]
            ]
        )->toArray();
        return $html;
    }
}