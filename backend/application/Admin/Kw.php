<?php

namespace Application\Admin;


use Framework\Middleware\Handler;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\View\Pagination;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

use Model\Admin\Kw as ModelKw;

class Kw
{
    public $isRole = ['administrator', 'editor'];

    public function get(Request $request) : Response
    {
        $html = $request->html();
        $html->pagination = new Pagination(ModelKw::count([]), 50);
        $html->kws = ModelKw::find([],[
            'skip' => $html->pagination->skip(),
            'limit' => $html->pagination->limit(),
            'sort' => [
                'view_num' => -1
            ]
        ])->toArray();
        return $html;
    }

}