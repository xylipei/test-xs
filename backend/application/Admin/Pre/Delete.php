<?php
namespace Application\Admin\Pre;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\Middleware\Handler;
use MongoDB\BSON\ObjectId;

use MongoDB\BSON\UTCDateTime;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

use Model\Api\Order\Pre as ModelPre;

class Delete
{

    public $isRole = ['administrator'];

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request,Handler $next) {
            $validator = Validator::create()->arrayType()
                ->key('id', Validator::id(), true);
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request): Response
    {
        $delete = ModelPre::deleteOne([
            '_id' => new ObjectID($request->data('id'))
        ]);

        if (!empty($delete)) {
            return Html::success('删除成功', '前往套餐列表', '/pre');
        } else {
            return Html::error('删除失败', '返回重新删除');
        }
    }
}


