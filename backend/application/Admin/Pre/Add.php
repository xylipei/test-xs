<?php
namespace Application\Admin\Pre;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\Middleware\Handler;

use MongoDB\BSON\UTCDateTime;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

use Model\Api\Order\Pre as ModelPre;

class Add
{
    public $isRole = ['administrator'];

    public function get(Request $request)
    {
        $html = $request->html();
        return $html;
    }

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request,Handler $next) {
            $validator = Validator::keySet(
                Validator::key('action', Validator::in(['vip', 'balance']), true),
                Validator::key('name', Validator::stringType()->notEmpty(), true),
                Validator::key('tip', Validator::stringType(), false),
                Validator::key('memo',Validator::stringType()->notEmpty(),true),
                Validator::key('num',Validator::numeric(),true),
                Validator::key('amount',Validator::numeric(),true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request): Response
    {
        $params = $request->data();
        $params['num'] = (int) $params['num'];
        $params['create_time'] = new UTCDateTime();
        $params['update_time'] = new UTCDateTime();
        switch ($params['action']){
            case 'vip':
                $params['num'] = $params['num'] * 86400;
                break;
            case 'balance':
                $params['num'] = (int) $params['num'];
                break;
        }
        $params['amount'] = (int) $params['amount'];
        $params['status'] = 'draft';
        $video = ModelPre::insertOne($params);
        if(!$video) return Html::error('添加失败','返回重新添加','/pre/add');
        return Html::success('添加成功','前往修改','/pre/edit?pre_id=' . $video );
    }
}