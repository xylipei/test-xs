<?php


namespace Application\Admin\Pre;


use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\Middleware\Handler;
use MongoDB\BSON\ObjectId;

use MongoDB\BSON\UTCDateTime;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

use Model\Api\Order\Pre as ModelPre;


class Edit
{
    public $isRole = ['administrator'];

    public function get(Request $request)
    {
        $pre_id = $request->data('pre_id');
        $html = $request->html();
        $html->pre = ModelPre::findOne(
            [
                '_id' =>  new ObjectId($pre_id)
            ]
        );
        return $html;
    }

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request,Handler $next) {
            $validator = Validator::keySet(
                Validator::key('pre_id', Validator::id(), true),
                Validator::key('action', Validator::in(['vip', 'balance']), true),
                Validator::key('name', Validator::stringType(), true),
                Validator::key('tip', Validator::stringType(), false),
                Validator::key('memo',Validator::stringType(),true),
                Validator::key('num',Validator::stringType(),true),
                Validator::key('amount',Validator::numeric(),true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request): Response
    {
        $params = $request->data();
        $pre_id = $params['pre_id'];unset($params['pre_id']);
        $params['num'] = (int) $params['num'];
        $params['update_time'] = new UTCDateTime();
        switch ($params['action']){
            case 'vip':
                $params['num'] = $params['num'] * 86400;
                break;
            case 'balance':
                $params['num'] = (int) $params['num'];
                break;
        }
        $params['amount'] = (int) $params['amount'];
        $params['status'] = 'draft';
        $pre = ModelPre::findOneAndUpdate(
            [
                '_id' => new ObjectId($pre_id)
            ],
            [
                '$set' => $params,
            ],
            [
                'returnDocument' => 2
            ]
        );
        if(!$pre) return Html::error('修改失败','返回套餐列表','/pre?action=' . $params['action']);
        return Html::success('修改成功','返回套餐列表','/pre?action=' . $pre['action']);
    }
}