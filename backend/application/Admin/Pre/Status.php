<?php


namespace Application\Admin\Pre;


use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\Middleware\Handler;
use MongoDB\BSON\ObjectId;

use MongoDB\BSON\UTCDateTime;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

use Model\Api\Order\Pre as ModelPre;

class Status
{
    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request,Handler $next) {
            $validator = Validator::keySet(
                Validator::key('pre_id', Validator::id(), true),
                Validator::key('status', Validator::in(['trashed', 'accepted']), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request): Response
    {
        $pre_id = new ObjectId($request->data('pre_id'));
        $status = $request->data('status');
        $pre = ModelPre::findOne(['_id' => $pre_id]);

        if (empty($pre)) {
            return Html::error('套餐不存在','返回套餐列表', '/pre');
        }

        $pre = ModelPre::findOneAndUpdate(
            ['_id' => $pre_id],
            ['$set' => ['status' => $status]],
            ['returnDocument' => 2]
        );

        if ($pre) {
            return Html::success('修改成功', '返回套餐列表', '/pre?action=' . $pre['action']);
        }
        return Html::error('修改失败', '返回重新修改');
    }
}