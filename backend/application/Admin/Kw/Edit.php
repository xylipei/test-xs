<?php

namespace Application\Admin\Kw;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\Middleware\Handler;
use Framework\Http\Response\Json;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDateTime;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

use Model\Admin\Kw as ModelKw;
use Model\Api\Book as ModelBook;


class Edit
{
    public $isRole = ['administrator'];

    public function setGetMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request,Handler $next) {
            $validator = Validator::create()->arrayType()
                ->key('kw_id', Validator::id(), true);
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('数据不合法', '返回重新选择');
            }
        });
    }

    public function get(Request $request): Response
    {
        $html = $request->html();
        $kw_id = $request->data('kw_id');
        $kw = ModelKw::findOne(['_id' => new ObjectId($kw_id)]);
        if (empty($kw)) {
            return Html::error('书单不存在', '返回重新选择');
        }
        $html->kw = $kw;
        return $html;
    }

    public function setPostMiddleware(Middleware $middleware): void
    {
        $middleware->add(function (Request $request,Handler $next) {
            $validator = Validator::keySet(
                Validator::key('kw_id', Validator::id(), true),
                Validator::key('title', Validator::stringType()->notEmpty(), true),
                Validator::key('book_id', Validator::id(), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request): Response
    {
        $params = $request->data(['title', 'kw_id', 'book_id']);
        $is_bl = ModelKw::findOne(['_id' => new ObjectId($params['kw_id'])]);
        if (empty($is_bl)) {
            return Html::error('非法关键词', '前往关键词列表', '/kw');
        }
        $is_bl = ModelKw::find(['title' => $params['title']]);
        if (!empty($is_bl) && (empty(ModelKw::findOne(['_id' => new ObjectId($params['kw_id']), 'title' => $params['title']])))) {
            return Html::error('关键词已存在', '前往关键词列表', '/kw');
        }
        $book = ModelBook::findOne(['_id' => new ObjectId($params['book_id'])]);
        if (empty($book)) {
            return Html::error('非法ID', '前往关键词列表', '/kw');
        }
        $params['book_id'] = new ObjectID($params['book_id']);
        $params['update_time'] = new UTCDateTime();
        $kw_id = $params['kw_id'];
        unset($params['kw_id']);
        $book_bl = ModelKw::findOneAndUpdate(
            ['_id' => new ObjectId($kw_id)],
            ['$set' => $params]
        );
        if (!empty($book_bl)) {
            return Html::success('修改成功', '前往关键词列表', '/kw');
        } else {
            return Html::error('修改失败', '返回重新输入');
        }
    }
}