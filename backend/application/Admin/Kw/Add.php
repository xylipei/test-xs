<?php

namespace Application\Admin\Kw;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\Middleware\Handler;
use Framework\Http\Response\Json;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDateTime;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

use Model\Admin\Kw as ModelKw;
use Model\Api\Book as ModelBook;

class Add
{
    public $isRole = ['administrator'];

    public function get(Request $request): Response
    {
        $html = $request->html();
        return $html;
    }

    public function setPostMiddleware(Middleware $middleware): void
    {
        $middleware->add(function (Request $request,Handler $next) {
            $validator = Validator::keySet(
                Validator::key('title', Validator::stringType()->notEmpty(), true),
                Validator::key('book_id', Validator::id(), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request): Response
    {
        $params = $request->data(['title', 'book_id']);
        $is_topic = ModelKw::findOne(['title' => $params['title']]);
        if (!empty($is_topic)) {
            return Html::error('关键词已存在', '前往关键词列表', '/kw');
        }
        $book = ModelBook::findOne([
            '_id' => new ObjectId($params['book_id']),
            'status' => 'accepted'
        ]);
        if (empty($book)) {
            return Html::error('非法ID', '前往关键词列表', '/kw');
        }
        $params['book_id'] = new ObjectID($params['book_id']);
        $params['package'] = [];
        $params['create_time'] = new UTCDateTime();
        $params['view_num'] = (int)0;
        $params['status'] = 'accepted';
        $kw = ModelKw::insertOne($params);
        if (!empty($kw)) {
            return Html::success('添加成功', '前往关键词列表', '/kw');
        } else {
            return Html::error('添加失败', '返回重新输入');
        }
    }
}