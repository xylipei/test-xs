<?php

namespace Application\Admin\Kw;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\Middleware\Handler;
use Framework\Http\Response\Json;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDateTime;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

use Model\Admin\Kw as ModelKw;

class Delete
{
    public $isRole = ['administrator'];
    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, $next) {
            $validator = Validator::create()->arrayType()
                ->key('kw_id', Validator::id(), true);
            try {
                $validator->assert($request->data());
                return $next($request);
            } catch (NestedValidationException $exception) {
                return Html::error('数据不合法', '返回重新选择');
            }
        });
    }
    public function post(Request $request) : Response
    {
        $params = $request->data();
        $deleted = ModelKw::findOneAndUpdate(
            ['_id' => new ObjectId($params['kw_id'])],
            ['$set' => ['status' => 'deleted']]
        );
        if (!empty($deleted)) {
            return Html::success('删除成功', '前往关键词列表', '/kw');
        } else {
            return Html::error('删除失败', '返回重新删除');
        }
    }
}