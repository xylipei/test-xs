<?php

namespace Application\Admin\Feedback;

use Framework\Http\Request;
use Framework\Http\Response;


class Setting
{
    public $isRole = ['administrator'];

    public function get(Request $request): Response
    {
        $html = $request->html();
        return $html;
    }
}