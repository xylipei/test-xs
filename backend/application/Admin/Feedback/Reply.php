<?php
namespace Application\Admin\Feedback;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Middleware;
use Framework\Middleware\Handler;
use Framework\Arr;
use Framework\Http\Response\Html;
use Respect\Validation\Validator;
use Respect\Validation\Exceptions\NestedValidationException;
use MongoDB\BSON\ObjectId;

use MongoDB\BSON\UTCDateTime;
use Model\Api\Feedback as ModelFeedback;


class Reply
{
    public $isRole = ['administrator', 'editor'];

    public function setGetMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request,Handler $next) {
            $validator = Validator::keySet(
                Validator::key('feedback_id', Validator::id(), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('数据不合法', '返回重新选择');
            }
        });
    }

    public function get(Request $request): Response
    {
        $html = $request->html();
        $params = $request->data(['feedback_id']);
        $question = ModelFeedback::findOne(['_id' => new ObjectID($params['feedback_id'])]);
        if (empty($question)) {
            return Html::error('反馈非法', '返回重新选择');
        }
        $new_reply = ModelFeedback::find(
            [],
            [
                'projection' => [
                    'reply' => 1,
                    '_id' => 0
                ],
            ]
        )->toArray();
        $array_reply = [];
        if (!empty($new_reply)){
            foreach ($new_reply as $reply){
                $array_reply =  array_merge($array_reply,$reply['reply']??[]);
            }
            if (!empty($array_reply)){
                $array_reply = Arr::array_sort($array_reply, 'create_time', SORT_DESC);
                $array_reply = Arr::unique_multidim_array($array_reply,'content');
                $array_reply = array_slice($array_reply,0,10);
            }
        }
        $html->replys = $array_reply;
        $html->feedback = $question;
        return $html;
    }

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request,Handler $next) {
            $validator = Validator::keySet(
                Validator::key('feedback_id', Validator::id(), true),
                Validator::key('content', Validator::stringType(), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新添加');
            }
        });
    }

    public function post(Request $request)
    {
        $params = $request->data(['content', 'feedback_id']);
        $params['admin_name'] = $request->user('nickname');
        $params['create_time'] = new UTCDateTime();
        $id = new ObjectId($params['feedback_id']);unset($params['feedback_id']);
        $reply = ModelFeedback::findOneAndUpdate(
            ['_id' => new ObjectId($id)],
            [
                '$set' => [
                    'status' => 'ok',
                    'update_time' => $params['create_time']
                ],
                '$addToSet' => [
                    'reply' =>$params
                ],
            ]
        );

        if (empty($reply)) {
            return Html::error('回复失败', '返回重新回复');
        }
        return Html::success('回复成功', '返回反馈列表', '/feedback');
    }
}
