<?php


namespace Application\Admin\App;

use Framework\Http\Request;
use Framework\Middleware;
use Framework\Middleware\Handler;
use Framework\Http\Response;
use MongoDB\BSON\ObjectId;
use Respect\Validation\Validator;
use Respect\Validation\Exceptions\NestedValidationException;
use Framework\Http\Response\Html;

use Model\Admin\App as ModelApp;
use Model\Api\App\Category as ModelAppCategory;


class Status
{
    public $isRole = ['administrator'];

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next): Response {
            $validator = Validator::keySet(
                Validator::key('app_id', Validator::id(), true),
                Validator::key('status', Validator::in(['accepted', 'draft', 'trashed']), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request)
    {
        $status = $request->data('status');
        $app_id = $request->data('app_id');
        $app = ModelApp::findOne(
            ['_id' => new ObjectId($app_id)]
        );
        if (empty($app)) {
            return Html::error('APP不存在','返回重新选择');
        }
        $app_status = Modelapp::findOneAndUpdate(
            ['_id' => new ObjectId($app_id)],
            [
                '$set' => [
                    'status' => $status
                ]
            ],
            [
                'returnDocument' => 2
            ]
        );
        if (empty($app_status)){
            return Html::error('操作失败','返回重新操作');
        }
        if($app['status'] == 'accepted' && $app_status['status'] != 'accepted'){
            ModelAppCategory::findOneAndUpdate(
                ['_id' => new ObjectId($app_status['category_id'])],
                ['$inc' => ['num.app' => -1]]
            );
        }
        if($app['status'] != 'accepted' && $app_status['status'] == 'accepted'){
            ModelAppCategory::findOneAndUpdate(
                ['_id' => new ObjectId($app_status['category_id'])],
                ['$inc' => ['num.app' => 1]]
            );
        }
        return Html::success('操作成功', '返回应用管理','/app');
    }
}