<?php


namespace Application\Admin\App;


use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\Middleware\Handler;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use MongoDB\BSON\UTCDateTime;
use MongoDB\BSON\ObjectId;

use Model\Admin\App as ModelApp;
use Model\Api\App\Category as ModelAppCategory;


class Edit
{
    public $isRole = ['administrator'];

    public function get(Request $request)
    {
        $html = $request->html();
        $app_id = $request->data('app_id');
        $html->app = ModelApp::findOne(['_id' => new ObjectId($app_id)]);
        $html->categorys = ModelAppCategory::find([
            'status' => 'accepted'
        ])->toArray();
        return $html;
    }

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next) : Response {
            $validator = Validator::keySet(
                Validator::key('app_id', Validator::id(), true),
                Validator::key('category', Validator::stringType(), true),
                Validator::key('memo', Validator::stringType(), true),
                Validator::key('app_name', Validator::nickname(), true),
                Validator::key('package_name', Validator::stringType(), true),
                Validator::key('apk_url', Validator::url(), true),
                Validator::key('icon', Validator::regex('/[0-9a-z\/\.]/'), true),
                Validator::key('file', Validator::stringType(), false)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request)
    {
        $params = $request->data(['app_id', 'memo', 'app_name', 'apk_url', 'icon', 'package_name', 'category']);
        $app_id = $params['app_id']; unset($params['app_id']);
        $app = ModelApp::findOne([
            '_id' => [
                '$ne' => new ObjectId($app_id)
            ],
            'apk_url' => $params['apk_url']
        ]);
        if (!empty($app)){
            return Html::error('应用已存在', '返回重新输入');
        }
        $app = ModelApp::findOne(['_id' => new ObjectId($app_id)]);
        if (empty($app)){
            return Html::error('非法ID', '返回重新选择');
        }
        $category = ModelAppCategory::findOneAndUpdate(
            ['title' => $request->data('category')],
            [
                '$set' => [
                    'update_time' => new UTCDateTime()
                ],
                '$setOnInsert' => [
                    'title' => $params['category'],
                    'create_time' => new UTCDateTime(),
                    'num.book' => 0,
                    'status' => 'draft'
                ]
            ],
            [
                'returnDocument' => 2,
                'upsert' => true
            ]
        );
        $params['update_time'] = new UTCDateTime();
        $params['category'] = $category['title'];
        $params['category_id'] = new ObjectId($category['_id']);
        $params['status'] = 'draft';
        $app_update = ModelApp::findOneAndUpdate(
            [
                '_id' => new ObjectId($app_id)
            ],
            [
                '$set' => $params
            ],
            [
                'returnDocument' => 2
            ]
        );
        if (empty($app_update)) {
            return Html::error('修改失败', '返回重新输入');
        }
        if($app['status'] == 'accepted'){
            ModelAppCategory::findOneAndUpdate(
                ['_id' => new ObjectId($app['category_id'])],
                ['$inc' => ['num.app' => -1]]
            );
        }
        return Html::success('修改成功', '前往应用列表', '/app');
    }
}