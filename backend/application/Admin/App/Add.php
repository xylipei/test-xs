<?php
namespace Application\Admin\App;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\Middleware\Handler;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

use MongoDB\BSON\UTCDateTime;
use MongoDB\BSON\ObjectId;

use Model\Admin\App as ModelApp;
use Model\Api\App\Category as ModelAppCategory;

class Add
{
    public $isRole = ['administrator'];

    public function get(Request $request)
    {
        $html = $request->html();
        $html->categorys = ModelAppCategory::find([
            'status' => 'accepted'
        ])->toArray();
        return $html;
    }

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next) : Response {
            $validator = Validator::keySet(
                Validator::key('app_name', Validator::nickname(), true),
                Validator::key('category', Validator::stringType(), true),
                Validator::key('package_name', Validator::stringType(), true),
                Validator::key('memo', Validator::stringType(), false),
                Validator::key('apk_url', Validator::url(), true),
                Validator::key('icon', Validator::regex('/[0-9a-z\/\.]/'), true),
                Validator::key('file', Validator::stringType(), false)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request)
    {
        $params = $request->data(['app_name', 'apk_url', 'icon', 'memo', 'package_name', 'category']);
        $app = ModelApp::findOne(['apk_url' => $params['apk_url']]);
        if (!empty($app)){
            return Html::error('应用已存在', '返回重新输入');
        }
        $category = ModelAppCategory::findOneAndUpdate(
            ['title' => $request->data('category')],
            [
                '$set' => [
                    'update_time' => new UTCDateTime(),
                    'thumb' => $params['thumb']
                ],
                '$setOnInsert' => [
                    'title' => $request->data('category'),
                    'create_time' => new UTCDateTime(),
                    'num.app' => 0,
                    'status' => 'draft'
                ]
            ],
            [
                'returnDocument' => 2,
                'upsert' => true
            ]
        );
        $params['create_time'] = new UTCDateTime();
        $params['update_time'] = new UTCDateTime();
        $params['category'] = $category['title'];
        $params['category_id'] = new ObjectId($category['_id']);
        $params['status'] = 'draft';
        $app = ModelApp::insertOne($params);
        if (empty($app)) {
            return Html::error('添加失败', '返回重新输入');
        }
        return Html::success('添加成功', '前往应用列表', '/app/edit?app_id=' . $app);
    }

}