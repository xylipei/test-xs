<?php
namespace Application\Admin\App\Category;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\Middleware\Handler;

use MongoDB\BSON\UTCDateTime;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

use Model\Api\App\Category as ModelAppCategory;

class Add
{
    public $isRole = ['administrator'];

    public function get(Request $request)
    {
        $html = $request->html();
        return $html;
    }

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next): Response {
            $validator = Validator::keySet(
                Validator::key('title', Validator::nickname(), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request): Response
    {
        $params = $request->data(['title']);
        $params['num'] = ['app' => 0];
        $params['create_time'] = new UTCDateTime();
        $params['update_time'] = new UTCDateTime();
        $params['status'] = 'draft';
        if (ModelAppCategory::insertOne($params)) {
            return Html::success('添加成功', '前往应用分类列表', '/app/category');
        } else {
            return Html::error('添加失败', '返回重新输入');
        }
    }
}