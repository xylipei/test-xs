<?php


namespace Application\Admin\App\Category;


use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Middleware;
use Framework\Middleware\Handler;
use Framework\Http\Response\Html;
use Respect\Validation\Validator;
use Respect\Validation\Exceptions\NestedValidationException;
use MongoDB\BSON\ObjectId;

use Model\Api\App\Category as ModelAppCategory;
use Model\Admin\App as ModelApp;


class Status
{

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next): Response {
            $validator = Validator::keySet(
                Validator::key('category_id', Validator::id(), true),
                Validator::key('status', Validator::in(['trashed', 'accepted']), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新修改');
            }
        });
    }

    public function post(Request $request)
    {
        $category_id = new ObjectId($request->data('category_id'));
        $status = $request->data('status');
        $category = ModelAppCategory::findOne(['_id' => $category_id]);
        if (empty($category)) {
            return Html::error('分类不存在','返回应用分类列表', '/app/category');
        }

        $category = ModelAppCategory::findOneAndUpdate(
            ['_id' => $category_id],
            ['$set' => ['status' => $status]]
        );

        if ($category) {
            if ($status == 'trashed'){
                ModelApp::updateMany(
                    [
                        'category_id' => new ObjectId($category_id),
                        'status' => 'accepted'
                    ],
                    [
                        '$set' => [
                            'status' => 'trashed'
                        ]
                    ]
                );
                ModelAppCategory::findOneAndUpdate(
                    ['_id' => $category_id],
                    ['$set' => ['num.app' => 0]]
                );
            }
            if ($status == 'accepted') {
                $update_count = ModelApp::updateMany(
                    [
                        'category_id' => new ObjectId($category_id),
                        'status' => 'trashed'
                    ],
                    [
                        '$set' => [
                            'status' => 'accepted'
                        ]
                    ]
                );
                ModelAppCategory::findOneAndUpdate(
                    ['_id' => $category_id],
                    ['$set' => ['num.app' => (int)$update_count]]
                );
            }
            return Html::success('修改成功', '返回应用分类列表', '/app/category');
        }
        return Html::error('修改失败', '返回重新修改');
    }

}