<?php


namespace Application\Admin\App\Category;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\Middleware\Handler;


use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

use Model\Api\App\Category as ModelAppCategory;

class Edit
{
    public $isRole = ['administrator'];

    public function setGetMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next): Response {
            $validator = Validator::keySet(
                Validator::key('category_id', Validator::id(), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function get(Request $request)
    {
        $html = $request->html();
        $category_id = $request->data('category_id');
        $html->category = ModelAppCategory::findOne([
            '_id' => new ObjectId($category_id)
        ]);
        return $html;
    }
}