<?php


namespace Application\Admin\App;

use Framework\Http\Request;
use Model\Api\App\Category as ModelAppCategory;


class Category
{
    public $isRole = ['administrator'];

    public function get(Request $request)
    {
        $html = $request->html();
        $html->categorys = ModelAppCategory::find([])->toArray();
        return $html;
    }
}