<?php
namespace Application\Admin;


use Framework\Http\Request;
use Framework\Http\Response;

use Model\Api\Order\Pre as ModelPre;



class Pre
{
    public $isRole = ['administrator'];

    public function get(Request $request) : Response
    {
        $html = $request->html();
        $action  = $request->data('action') ?? 'vip';
        $html->action = $action;
        $html->pres = ModelPre::find(
            [
                'action' => $action
            ]
        )->toArray();
        return $html;
    }
}