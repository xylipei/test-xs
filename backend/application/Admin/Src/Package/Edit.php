<?php

namespace Application\Admin\Src\Package;

use Framework\Middleware\Handler;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime;

use Model\Admin\Package as ModelPackage;
use Model\Admin\Kw as ModelKw;

class Edit
{
    public $isRole = ['administrator'];

    public function setGetMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request,Handler $next) {
            $validator = Validator::create()->arrayType()
                ->key('package_id', Validator::id(), true);
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function get(Request $request): Response
    {
        $html = $request->html();
        $package_id = $request->data('package_id');
        $kw = ModelKw::findOne(['package.package_id' => new ObjectId($package_id)]);
        $html->package = ModelPackage::findOne(['_id' => new ObjectId($package_id)]);
        $html->kw_id = $kw['_id'];
        $html->kws = ModelKw::find([])->toArray();
        return $html;
    }

    public function setPostMiddleware(Middleware $middleware): void
    {
        $middleware->add(function (Request $request,Handler $next) {
            $validator = Validator::keySet(
                Validator::key('package_id', Validator::id(), true),
                Validator::key('name', Validator::stringType(), true),
                Validator::key('icon', Validator::url(), true),
                Validator::key('kw_id', Validator::id(), true),
                Validator::key('package_name', Validator::stringType(), true)
            );

            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request): Response
    {
        $params = $request->data();
        $kw = (bool)ModelKw::findOneAndUpdate(
            ['package.package_id' => new ObjectId($params['package_id'])],
            [
                '$pull' => [
                    'package' => [
                        'package_id' => new ObjectId($params['package_id'])
                    ]
                ]
            ]
        );

        if ($kw){
            //添加关键词
            ModelKw::findOneAndUpdate(
                ['_id' => new ObjectId($params['kw_id'])],
                [
                    '$addToSet' => [
                        'package' => [
                            'package_id' => new ObjectId($params['package_id']),
                            'create_time' => new UTCDateTime()
                        ]
                    ]
                ]
            );
        }

        $package = ModelPackage::findOneAndUpdate(
            ['_id' => new ObjectID($params['package_id'])],
            [
                '$set' => [
                    'name' => $params['name'],
                    'icon' => $params['icon'],
                    'package_name' => $params['package_name'],
                    'create_time' => new UTCDateTime()
                ],
            ],
            ['returnDocument' => 2]
        );

        if (!empty($package)) {
            return Html::success('修改成功', '前往分包列表', '/src/package/');
        } else {
            return Html::error('修改失败', '返回重新输入');
        }

    }

}
