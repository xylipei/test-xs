<?php

namespace Application\Admin\Src\Package;

use Framework\Middleware\Handler;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Json;
use Framework\Middleware;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime;

use Model\Admin\Package as ModelPackage;
use Model\Admin\Etc as ModelEtc;

class Task
{
    public $isPublic = true;

    public function setMiddleware(Middleware $middleware): void
    {
        $middleware->add(function (Request $request, Handler $next) {
            $token = md5(date('Ymd'));
            if (strcasecmp($request->data('token'), md5($token . date('Ymd'))) === 0) {
                return $next->handle($request);
            }
            return Json::error('没有权限');
        });
    }

    public function get(Request $request): Response
    {
        $package = ModelPackage::findOneAndUpdate(
            ['status' => 'wait'],
            [
                '$set' => [
                    'status' => 'packing',
                    'pack_start' => new UTCDateTime()
                ]
            ],
            ['sort' => ['create_time' => -1]]
        );

        if (!empty($package)) {
            ModelPackage::updateMany(
                [
                    'status' => 'wait',
                    'src_id' => new ObjectId($package['src_id']),
                    'package_name' => $package['package_name']
                ],
                ['$set' => ['status' => 'packing']]
            );
            $package['package_id'] = $package['_id'];
            unset($package['_id']);
            $package['keystore'] = $package['keystore'] ?? '';
            $url = ModelEtc::findOne(['name' => 'package_url']);
            $package['url'] = [
                'api' => $url['api'] . '/',
                'image' => $url['image'] . '/',
                'video' => $url['video'] . '/'
            ];

            return Json::success($package);
        }

        return Json::error('没有打包任务', 1000);
    }

    public function setPostMiddleware(Middleware $middleware): void
    {
        $middleware->add(function (Request $request,Handler $next) {
            $validator = Validator::keySet(
                Validator::key('package_id', Validator::id(), true),
                Validator::key('src_id', Validator::id(), true),
                Validator::key('sign', Validator::stringType(), true),
                Validator::key('keystore', Validator::stringType(), false),
                Validator::key('version', Validator::version(), true),
                Validator::key('token', Validator::stringType(), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Json::error('输入数据不合法');
            }
        });
    }

    public function post(Request $request): Response
    {
        $params = $request->data();
        if (
            empty($_FILES['apk']) ||
            $_FILES['apk']['error'] !== 0 ||
            pathinfo($_FILES['apk']['name'], PATHINFO_EXTENSION) !== 'apk' ||
            strcasecmp(md5_file($_FILES['apk']['tmp_name']), $params['sign']) !== 0
        ) {
            return Json::error('上传错误');
        }

        $dir = 'upload/apk/new/';

        if (
            !is_dir(ROOT . 'www/' . $dir) &&
            !mkdir(ROOT . 'www/' . $dir, 0777, true) &&
            !is_writable(ROOT . 'www/' . $dir)
        ) {
            return Json::error('目录不可写');
        }

        $size = filesize($_FILES['apk']['tmp_name']);
        $new = $dir . $params['package_id'] . '.apk';
        $apk_uri = 'apk/new/'.$params['package_id'] . '.apk';
        if (file_exists(ROOT . 'www/' . $new)) {
            unlink(ROOT . 'www/' . $new);
        }

        if (move_uploaded_file($_FILES['apk']['tmp_name'], ROOT . 'www/' . $new)) {
            $package = ModelPackage::findOneAndUpdate(
                ['_id' => new ObjectId($params['package_id'])],
                ['$set' => [
                    'status' => 'packed',
                    'version' => $params['version'],
                    'keystore' => $params['keystore'],
                    'sign' => $params['sign'],
                    'pack_end' => new UTCDateTime(),
                    'download' => $apk_uri,
                    'size' => $size,
                ]],
                ['returnDocument' => 2]
            );

            //刷新缓存
            $urls = ModelEtc::findOne(['name' => 'package_url']);
            $download_url = $urls['download'] ?? '';
            @file_get_contents($download_url . '/refresh/' . $new);

            //更新数据库
            ModelPackage::updateMany(
                [
                    'package_name' => $package['package_name'],
                    'src_id' => new ObjectId($package['src_id']),
                    'status' => 'packing'
                ],
                [
                    '$set' => [
                        'status' => 'packed',
                        'version' => $package['version'],
                        'keystore' => $package['keystore'],
                        'sign' => $package['sign'],
                        'pack_start' => new UTCDateTime($package['pack_start'] * 1000),
                        'pack_end' => new UTCDateTime(),
                        'download' => $package['download'],
                        'size' => $package['size']
                    ]
                ]
            );

            return Json::success($package);
        }
        return Json::error('未知错误');
    }
}

