<?php

namespace Application\Admin\Src\Package;


use Framework\Middleware\Handler;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime;
use Overtrue\Pinyin\Pinyin;

use Model\Admin\Src as ModelSrc;
use Model\Admin\Package as ModelPackage;
use Model\Admin\Kw as ModelKw;


class Add
{
    public $isRole = ['administrator'];

    public function get(Request $request) : Response
    {
        $html = $request->html();
        $html->srcs = ModelSrc::find([])->toArray();
        $html->kws = ModelKw::find([])->toArray();
        return $html;
    }

    public function setPostMiddleware(Middleware $middleware) : void
    {
        $middleware->add(function (Request $request,Handler $next) {
            $validator = Validator::keySet(
                Validator::key('src_id', Validator::id(), true),
                Validator::key('kw_id', Validator::id(), true),
                Validator::key('text', Validator::callback(function ($text) {
                    $is = true;
                    $text = explode("\n", trim($text));
                    if (empty($text)) $is = false;
                    foreach ($text as $t) {
                        $t = explode(',', $t);
                        if (count($t) !== 3) $is = false;
                    }
                    return $is;
                }), true)
            );

            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request) : Response
    {
        ignore_user_abort(true);
        set_time_limit(0);
        ini_set('memory_limit', '2048M');

        $src_id = $request->data('src_id');
        $kw_id = $request->data('kw_id');
        $text = $request->data('text');

        $pinyin = new Pinyin();
        $package_count = ModelPackage::count([]);
        $text = explode("\n", trim($text));
        $packages = [];
        if (!empty($text)) foreach ($text as $t) {
            $t = explode(',', $t);
            if (count($t) !== 3) continue;
            list($package['source'], $package['name'], $package['icon']) = $t;
            $uri = $this->strip_numbers(strtolower($pinyin->permalink($package['name'], '')));
            $package['package_name'] = 'com.'.$this->strip_numbers($uri.$package_count).'.xunshu';
            $package['src_id'] = new ObjectId($src_id);
            $package['keystore'] = '';
            $packages[] = $package;
            unset($package);
        }


        $ids = [];
        if (!empty($packages)) foreach ($packages as $package) {
            $source = $package['source'];
            unset($package['source']);
            $package = ModelPackage::findOneAndUpdate(
                [
                    'src_id' => $package['src_id'],
                    'package_name' => $package['package_name']
                ],
                [
                    '$addToSet' => ['source' => $source],
                    '$set' => ['status' => 'wait', 'create_time' => new UTCDateTime()],
                    '$setOnInsert' => $package
                ],
                [
                    'upsert' => true,
                    'returnDocument' => 2
                ]
            );

            if (!empty($package)) {
                $ids[] = $package['_id'];
            }

            $is_kw =(bool) ModelKw::findOneAndUpdate(
                [
                    'package.package_id' => new ObjectId($package['_id'])
                ],
                [
                    '$pull' => [
                        'package' =>[
                            'package_id' => new ObjectId($package['_id'])
                        ]
                    ]
                ]
            );

            if (!$is_kw){
                //添加关键词
                ModelKw::findOneAndUpdate(
                    ['_id' => new ObjectId($kw_id)],
                    [
                        '$addToSet' => [
                            'package' => [
                                'package_id' => new ObjectId($package['_id']),
                                'create_time' => new UTCDateTime()
                            ]
                        ]
                    ]
                );
            }
        }
        if (!empty($ids)) {
            return Html::success('添加成功', '前往分包列表','/src/package');
        } else {
            return Html::error('添加失败', '返回重新输入');
        }
    }

    private function strip_numbers($str)
    {
        return str_replace([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k'], $str);
    }
}
