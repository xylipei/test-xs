<?php


namespace Application\Admin\Src\Package;


use Framework\Middleware\Handler;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use MongoDB\BSON\ObjectId;

use Model\Admin\Package as ModelPackage;


class Delete
{
    public $isRole = ['administrator'];

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request,Handler $next) {
            $validator = Validator::create()->arrayType()
                ->key('package_id', Validator::id(), true);
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('数据不合法', '返回重新选择');
            }
        });
    }

    public function post(Request $request): Response
    {
        $params = $request->data();
        $deleted = ModelPackage::findOneAndUpdate(
            ['_id' => new ObjectId($params['package_id'])],
            [
                '$set' => [
                    'status' => 'deleted'
                ]
            ]
        );
        if (!empty($deleted)) {
            return Html::success('删除成功', '前往列表', '/src/package/');
        } else {
            return Html::error('删除失败', '返回重新删除');
        }
    }
}