<?php

namespace Application\Admin\Src\Package;


use Framework\Middleware\Handler;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

use Model\Admin\Etc as ModelEtc;


class Url
{
    public $isRole = ['administrator'];

    public function get(Request $request): Response
    {
        $html = $request->html();
        $url = ModelEtc::findOne(['name' => 'package_url']);
        if (!empty($url)) {
            $html->url = $url;
        }
        return $html;
    }

    public function setPostMiddleware(Middleware $middleware): void
    {
        $middleware->add(function (Request $request, Handler $next) {
            $validator = Validator::keySet(
                Validator::key('image', Validator::url(), true),
                Validator::key('video', Validator::url(), true),
                Validator::key('api', Validator::oneOf(
                    Validator::url(),
                    Validator::ip()
                ), true),
                Validator::key('download', Validator::oneOf(
                    Validator::url(),
                    Validator::ip()
                ), true)
            );

            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request): Response
    {
        $params = $request->data();

        $url = ModelEtc::findOneAndUpdate(
            ['name' => 'package_url'],
            ['$set' => $params],
            [
                'returnDocument' => 2,
                'upsert' => true
            ]
        );
        if (!empty($url)) {
            return Html::success('添加成功', '前往分包配置', '/src/package/url');
        } else {
            return Html::error('添加失败', '返回重新输入');
        }
    }
}



