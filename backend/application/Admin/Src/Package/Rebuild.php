<?php

namespace Application\Admin\Src\Package;


use Framework\Middleware\Handler;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use MongoDB\BSON\ObjectId;

use Model\Admin\Package as ModelPackage;


class Rebuild
{
    public $isRole = ['administrator'];

    public function setPostMiddleware(Middleware $middleware) : void
    {
        $middleware->add(function (Request $request,Handler $next) {
            $validator = Validator::oneOf(
                Validator::keySet(
                    Validator::key('action', Validator::in(['all', 'packing', 'quick']), true)
                ),
                Validator::keySet(
                    Validator::key('action', Validator::in(['one']), true),
                    Validator::key('id', Validator::id(), true)
                )
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request) : Response
    {
        $params = $request->data();
        if (!empty($params['action'])) switch ($params['action']) {
            case 'one':
                $id = $params['id'];
                ModelPackage::findOneAndUpdate(['_id' => new ObjectId($params['id'])], ['$set' => ['status' => 'wait']]);
                break;
            case 'packing':
                ModelPackage::updateMany(['status' => 'packing'], ['$set' => ['status' => 'wait']]);
                break;
            case 'all':
                ModelPackage::updateMany(['status' => ['$ne' => 'deleted']],['$set' => ['status' => 'wait']]);
                break;
            case 'quick':
                ModelPackage::updateMany(['status' => ['$ne' => 'deleted']],['$set' => ['status' => 'packed']]);
                ModelPackage::updateMany(['active.'.date('Y\mm') => ['$gt' => 0],],['$set' => ['status' => 'wait']]);
                break;
        }
        return Html::success('保存成功', '返回分包列表', '/src/package');
    }
}

