<?php

namespace Application\Admin\Src;

use Framework\Middleware\Handler;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use MongoDB\BSON\ObjectId;

use Model\Admin\Account as ModelAccount;
use Model\Admin\Src as ModelSrc;

class Edit
{
    public $isRole = ['administrator'];

    public function setGetMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request,Handler $next) {
            $validator = Validator::create()->arrayType()
                ->key('src_id', Validator::id(), true);
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('数据不合法', '返回重新选择');
            }
        });
    }

    public function get(Request $request) : Response
    {
        $html = $request->html();
        $src_id = $request->data('src_id');
        $html->src = ModelSrc::findOne(['_id' => new ObjectID($src_id)]);
        $html->accounts = ModelAccount::find(['role' => 'src'])->toArray();
        return $html;
    }

    public function setPostMiddleware(Middleware $middleware) : void
    {
        $middleware->add(function (Request $request,Handler $next) {
            $validator = Validator::keySet(
                Validator::key('src_id', Validator::id(), true),
                Validator::key('name', Validator::stringType(), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request) : Response
    {
        $src_id = $request->data('src_id');
        $params = $request->data(['name', 'apk']);
        $params['status'] = 'ok';
        $params = array_filter($params);
        $src = ModelSrc::findOneAndUpdate(
            ['_id' => new ObjectID($src_id),],
            ['$set' => $params,],
            ['returnDocument' => 2]
        );
        if (!empty($src)) return Html::success('修改成功', '前往渠道列表','/src');
        return Html::error('修改失败', '返回重新输入');
    }

}