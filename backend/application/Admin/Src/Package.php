<?php

namespace Application\Admin\Src;

use Framework\Middleware\Handler;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\View\Pagination;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use MongoDB\BSON\ObjectId;

use Model\Admin\Src as ModelSrc;
use Model\Admin\Package as ModelPackage;
use Model\Admin\Etc as ModelEtc;


class Package
{
    public $isRole = ['administrator'];

    public function setGetMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next) {
            $validator = Validator::keySet(
                Validator::key('src_id', Validator::oneOf(
                    Validator::id(),
                    Validator::equals('')
                ), false),
                Validator::key('version', Validator::oneOf(
                    Validator::version(),
                    Validator::equals('')
                ), false),
                Validator::key('name', Validator::stringType(), false),
                Validator::key('status', Validator::in(['wait', 'packed', 'packing', 'deleted', '']), false),
                Validator::key('action', Validator::in(['view', 'download', '']), false),
                Validator::key('page', Validator::numeric(), false)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function get(Request $request): Response
    {
        $html = $request->html();
        $params = $request->data();
        $html->params = $params = array_filter(array_unique($params));

        $filter = ['status' => ['$ne' => 'deleted']];
        if (!empty($params)) foreach ($params as $name => $value) {
            switch ($name) {
                case 'src_id':
                    $filter['src_id'] = new ObjectId($value);
                    break;
                case 'name':
                    if (!empty($value)) {
                        $filter['name'] = ['$regex' => $value];
                    }
                    break;
                case 'status':
                    $filter['status'] = $value;
                    break;
                case 'version':
                    $filter['version'] = $value;
                    break;
            }
            unset($name, $value);
        }
        if (!empty($params['src_id'])) $html->src = ModelSrc::findOne(['_id' => new ObjectId($params['src_id'])]);
        $cspip = [];
        if (!empty($filter)) $cspip[] = [
            '$match' => $filter
        ];
        $cspip[] = [
            '$group' => [
                '_id' => [
                    'status' => '$status'
                ],
                'count' => ['$sum' => 1],
            ]
        ];
        $cs = ModelPackage::aggregate($cspip)->toArray();

        $count = [];

        if (!empty($cs)) foreach ($cs as $c) {
            $count[$c['_id']['status']] = $c['count'];
        }
        unset($c);

        $html->count = $count;
        $html->pagination = new Pagination(empty($params['status']) ? array_sum($count) : $count[$params['status']] ?? 0, 50);

        $options = [];
        $options['sort'] = ['create_time' => -1];
        if (($params['action'] ?? '') != 'download') {
            $options['limit'] = $html->pagination->limit();
            $options['skip'] = $html->pagination->skip();
        }
        $packages = $html->packages = ModelPackage::find($filter, $options)->toArray();
        $html->srcs = [];
        $srcs = ModelSrc::find([])->toArray();
        if (!empty($srcs)) foreach ($srcs as $src) {
            $html->srcs[$src['_id']] = $src['name'];
        }
        $src_names = $html->srcs;
        $url = ModelEtc::findOne(['name' => 'package_url']);
        $html->url = $url['download'] ?? '';
        if (($params['action'] ?? '') == 'download') {
            $download = tmpfile();
            fputcsv($download, ['渠道', '渠道ID', '原始ID', '应用名称', '图标地址', '打包状态', '打包耗时', '版本号', '下载地址']);
            if (!empty($packages)) foreach ($packages as $package) {
                if (!empty($package['source']) && is_array($package['source'])) foreach ($package['source'] as $source) {
                    if (!empty($package['download'])) {
                        fputcsv($download, [$src_names[$package['src_id']] ?? '', $package['src_id'], $source, $package['name'], $package['icon'], '已打包', (($package['pack_end'] ?? 0) - ($package['pack_start'] ?? 0)), $package['version'], $html->url . '/' . $package['download']]);
                    } else {
                        fputcsv($download, [$src_names[$package['src_id']] ?? '', $package['src_id'], $source, $package['name'], $package['icon'], '未打包', '', '', '']);
                    }
                }
            }
            ob_clean();
            rewind($download);
            $download = stream_get_contents($download);
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment;filename=渠道包' . time() . '.csv');
            header('Pragma: public');
            echo $download;
            exit;
        }

        return $html;
    }
}