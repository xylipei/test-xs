<?php

namespace Application\Admin\Src;

use Framework\Middleware\Handler;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use MongoDB\BSON\ObjectId;

use Model\Admin\Account as ModelAccount;
use Model\Admin\Src as ModelSrc;

class Add
{
    public $isRole = ['administrator'];

    public function get(Request $request) : Response
    {
        $html = $request->html();
        $html->accounts = ModelAccount::find(['role' => 'src']);
        return $html;
    }

    public function setPostMiddleware(Middleware $middleware) : void
    {
        $middleware->add(function (Request $request,Handler $next) {
            $validator = Validator::keySet(
                Validator::key('src_id', Validator::oneOf(
                    Validator::id(),
                    Validator::equals('')
                ), true),
                Validator::key('name', Validator::stringType(), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request) : Response
    {
        $params = $request->data(['src_id', 'name', 'apk']);
        $params['status'] = 'ok';
        $params = array_filter($params);
        if (!empty($params['src_id'])) $params['_id'] = new ObjectID($params['src_id']);
        $params['username'] = (string) new ObjectID();
        $params['password'] = md5(hash_hmac('sha256', $params['username'], $params['username']));
        $src = ModelSrc::insertOne($params);
        if (!empty($src)) return Html::success('添加成功', '前往渠道列表','/src');
        return Html::error('添加失败', '返回重新输入');
    }
}