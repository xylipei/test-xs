<?php

namespace Application\Admin;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Middleware;
use Respect\Validation\Validator;
use Respect\Validation\Exceptions\NestedValidationException;
use Framework\Http\Response\Html;
use Framework\Middleware\Handler;

use MongoDB\BSON\UTCDateTime;
use MongoDB\BSON\ObjectId;

use Model\Admin\Src as ModelSrc;
use Model\Api\Order as ModelOrder;
use Model\Api\User as ModelUser;

class State
{
    public $isRole = ['administrator'];

    public function setGetMiddleware(Middleware $middleware) : void
    {
        $middleware->add(function (Request $request,Handler $next) {
            $validator = Validator::create()->arrayType()
                ->key('date', Validator::oneOf(
                    Validator::keySet(
                        Validator::key('start', Validator::equals(''), true),
                        Validator::key('end', Validator::equals(''), true)
                    ),
                    Validator::keySet(
                        Validator::key('start', Validator::date('Y-m-d'), true),
                        Validator::key('end', Validator::date('Y-m-d'), true)
                    )
                ), false)
                ->key('src', Validator::oneOf(
                    Validator::id(),
                    Validator::equals('')
                ), false)
                ->key('action', Validator::in(['', 'download']), false);
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新登录');
            }
        });
    }

    public function get(Request $request) : Response
    {
        $html = $request->html();
        $html->params = $params = $request->data(['date', 'src'], [
            'date' => [
                'start' => date('Y-m-d'),
                'end' => date('Y-m-d'),
            ]
        ]);
        $params = array_filter($params);
        $filter = [];
        if (!empty($params)) foreach ($params as $name => $value) {
            switch ($name) {
                case 'date':
                    $filter['create_time'] = [
                        '$gte' => new UTCDateTime(strtotime($value['start']) * 1000),
                        '$lte' => new UTCDateTime((strtotime($value['end']) + 86400) * 1000),
                    ];
                    break;
                case 'src':
                    $filter['src'] = new ObjectID($value);
                    break;
            }
        }


        $srcs = ModelSrc::find([])->toArray();
        $html->srcs = [];
        if (!empty($srcs)) foreach ($srcs as $src) {
            $html->srcs[$src['_id']] = $src;
        }

        $user = ModelUser::aggregate([
            [
                '$match' => $filter
            ],
            [
                '$group' => [
                    '_id' => [
                        'src' => '$src',
                        'date' => ['$dateToString' => ['format' => '%Y-%m-%d', 'date' => '$create_time', 'timezone' => 'Asia/Shanghai']],
                    ],
                    'count' => ['$sum' => 1],
                ]
            ]
        ])->toArray();

        $html->user = [];
        if (!empty($user)) foreach ($user as $u) {
            $id = $u['_id']; unset($u['_id']);
            if (!empty($id['src'])) {
                $html->user[$id['src']][$id['date']] = $u['count'];
            }
        }

        $result = ModelOrder::aggregate([
            [
                '$match' => $filter
            ],
            [
                '$project' => [
                    'src' => 1,
                    'paid_num' => ['$cond' => [['$eq' => ['$status', 'paid']], 1, 0]],
                    'paid_amount' => ['$cond' => [['$eq' => ['$status', 'paid']], '$amount', 0]],
                    'paid_wx_amount' => ['$cond' => [
                        ['$eq' => ['$status', 'paid']],
                        ['$cond' => [['$eq' => ['$pm', 'wxh5']], '$amount', 0]],
                        0
                    ]],
                    'paid_ali_amount' => ['$cond' => [
                        ['$eq' => ['$status', 'paid']],
                        ['$cond' => [['$eq' => ['$pm', 'alih5']], '$amount', 0]],
                        0
                    ]],
                    'paid_wxapp_amount' => ['$cond' => [
                        ['$eq' => ['$status', 'paid']],
                        ['$cond' => [['$eq' => ['$pm', 'wxapp']], '$amount', 0]],
                        0
                    ]],
                    'create_time' => 1,
                ]
            ],
            [
                '$group' => [
                    '_id' => [
                        'src' => '$src',
                        'date' => ['$dateToString' => ['format' => '%Y-%m-%d', 'date' => '$create_time', 'timezone' => 'Asia/Shanghai']],
                    ],
                    'order_num' => ['$sum' => 1],
                    'order_amount' => ['$sum' => '$amount'],
                    'order_paid_num' => ['$sum' => '$paid_num'],
                    'order_paid_amount' => ['$sum' => '$paid_amount'],
                    'order_paid_wx_amount' => ['$sum' => '$paid_wx_amount'],
                    'order_paid_ali_amount' => ['$sum' => '$paid_ali_amount'],
                    'order_paid_wxapp_amount' => ['$sum' => '$paid_wxapp_amount'],
                ],
            ]
        ])->toArray();

        $html->result = [];
        if (!empty($result)) foreach ($result as $r) {
            $id = $r['_id']; unset($r['_id']);
            if (!isset($id['src'])) continue;
            $r['src'] = $id['src'];
            $r['date'] = $id['date'];
            $html->result[] = $r;
            unset($r);
        }

        array_multisort(
            array_column($html->result, 'src'), SORT_DESC,
            array_column($html->result, 'date'), SORT_DESC,
            $html->result
        );

        return $html;

    }
}