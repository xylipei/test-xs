<?php

namespace Application\Admin;

use Framework\Http\Request;
use Framework\Http\Response;

use Framework\View\Pagination;
use Model\Api\Feedback as ModelFeedback;

class Feedback
{
    public $isRole = ['administrator'];

    public function get(Request $request): Response
    {
        $html = $request->html();

        $html->pagination = new Pagination(ModelFeedback::count([]), 50);
        $html->feedbacks = ModelFeedback::find()->toArray();

        return $html;
    }
}