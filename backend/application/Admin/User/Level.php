<?php

namespace Application\Admin\User;

use Framework\Http\Request;
use Framework\Http\Response;


class Level
{
    public $isRole = ['administrator'];

    public function get(Request $request): Response
    {
        $html = $request->html();
        return $html;
    }
}