<?php

namespace Application\Admin\User;

use Framework\Http\Request;
use Framework\Http\Response;

use Framework\View\Pagination;
use Model\Api\Device as ModelDevice;

class Device
{
    public function get(Request $request): Response
    {
        $html = $request->html();

        $html->count = ModelDevice::count([]);
        $html->pagination = new Pagination($html->count, 50);
        $html->users = ModelDevice::find([],['skip' => $html->pagination->skip(), 'limit' => $html->pagination->limit(), 'sort' => ['create_time' => -1]])->toArray();

        return $html;
    }
}
