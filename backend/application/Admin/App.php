<?php


namespace Application\Admin;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\View\Pagination;
use Framework\Middleware;
use Framework\Middleware\Handler;
use Framework\Http\Response\Html;
use Respect\Validation\Validator;
use Respect\Validation\Exceptions\NestedValidationException;
use MongoDB\BSON\ObjectId;

use Model\Admin\App as ModelApp;
use Model\Api\App\Category as ModelAppCategory;

class App
{
    public $isRole = ['administrator', 'editor'];

    public function setGetMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next): Response {
            $validator = Validator::keySet(
                Validator::key('category_id', Validator::oneOf(Validator::id(), Validator::equals('')), false),
                Validator::key('app_name', Validator::stringType(), false)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function get(Request $request) : Response
    {
        $html = $request->html();
        $app_name = $request->data('app_name');
        $category_id = $request->data('category_id');
        $filter = [];
        if (!empty($app_name)){
            $filter['app_name'] = ['$regex' => $app_name];
        }
        if (!empty($category_id)){
            $filter['category_id'] = new ObjectId($category_id);
        }
        $html->category_id = $category_id;
        $html->categorys = ModelAppCategory::find([])->toArray();
        $html->app_name = $app_name;
        $html->pagination = new Pagination(ModelApp::count($filter), 50);
        $html->apps = ModelApp::find($filter,[
            'skip' => $html->pagination->skip(),
            'limit' => $html->pagination->limit(),
            'sort' => [
                'view_num' => -1
            ]
        ])->toArray();
        return $html;
    }

}