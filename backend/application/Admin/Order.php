<?php

namespace Application\Admin;


use Framework\Http\Response;
use Framework\Http\Request;
use Framework\View\Pagination;

use MongoDB\BSON\ObjectId;
use Model\Api\Order as ModelOrder;
use Model\Admin\Src as ModelSrc;

class Order
{
    public $isRole = ['administrator'];

    public function get(Request $request) : Response
    {
        $html = $request->html();
        $params = $request->data();
        $filter = [];
        foreach ($params as $name => $value) {
            switch ($name) {
                case 'order_id':
                    if (empty($value)){
                        break;
                    }
                    $filter['_id'] = new ObjectId($value);
                    break;
                case 'is_vip':
                    switch ($value) {
                        case 'true':
                            $filter['extra.action'] = 'vip';
                            break;
                        case 'false':
                            $filter['extra.action'] = 'coin';
                            break;
                    }
                    break;
            }
        }
        $html->pagination = new Pagination(ModelOrder::count([]), 50);
        $html->orders = ModelOrder::find($filter,[
            'skip' => $html->pagination->skip(),
            'limit' => $html->pagination->limit(),
            'sort' => [
                'create_time' => -1
            ]
        ])->toArray();
        $html->srcs = [];
        $srcs = ModelSrc::find([])->toArray();
        if (!empty($srcs)) foreach ($srcs as $src) {
            $html->srcs[$src['_id']] = $src['name'];
        }
        return $html;
    }
}