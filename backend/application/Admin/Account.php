<?php

namespace Application\Admin;

use Framework\DB;
use Framework\Http\Request;
use Framework\Http\Response;

use Framework\Ip\Location;
use Model\Admin\Account as ModelAccount;

class Account
{
    public $isRole = ['administrator'];

    public function get(Request $request) : Response
    {
        $html = $request->html();
        $html->accounts = ModelAccount::find([])->toArray();
        return $html;
    }
}


