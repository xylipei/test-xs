<?php

namespace Application\Admin;

use Framework\Http\Request;
use Framework\Http\Response;
use MongoDB\BSON\ObjectId;

use Framework\View\Pagination;
use Model\Api\User as ModelUser;

class User
{
    public function get(Request $request): Response
    {
        $html = $request->html();
        $params = $request->data();
        $params = $html->params = array_replace($params, $request->data());
        $params = array_filter($params);
        $filter = [];
        $sort = [
            'create_time' => -1
        ];
        if (!empty($params)) foreach ($params as $name => $value) {
            switch ($name) {
                case 'user_id':
                    $filter[$name] = new ObjectId($value);
                    break;
                case 'status':
                    $filter[$name] = $value;
                    break;
                case 'search':
                    $filter['nickname'] = ['$regex' => $value];
                    break;
                case 'sort':
                    $sort = [$value => -1];
                    break;
            }
        }
        $html->count = ModelUser::count($filter);
        $html->pagination = new Pagination($html->count, 50);
        $html->users = ModelUser::find($filter,
            [
                'sort' => $sort,
                'skip' => $html->pagination->skip(),
                'limit' => $html->pagination->limit(),
            ]
        )->toArray();
        return $html;
    }
}
