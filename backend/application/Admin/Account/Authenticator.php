<?php
namespace Application\Admin\Account;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\Middleware\Handler;

use Framework\Security\Authenticator as AuthenticatorUtil;
use MongoDB\BSON\ObjectId;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

use Model\Admin\Account as ModelAccount;

class Authenticator
{
    public $isRole = ['administrator'];

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next) : Response {
            $validator = Validator::keySet(
                Validator::key('id', Validator::id(), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request) : Response
    {
        $id = $request->data('id');
        $disabled = ModelAccount::findOneAndUpdate(['_id' => new ObjectId($id)], [
            '$set' => [
                'authenticator' => [
                    'secret' => AuthenticatorUtil::secret(),
                    'bind' => false
                ]
            ]
        ]);

        if (!empty($disabled)) {
            return Html::success('重置成功', '前往账号列表', '/account');
        } else {
            return Html::error('禁用失败', '返回重新禁用');
        }
    }
}


