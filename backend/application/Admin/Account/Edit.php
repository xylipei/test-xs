<?php

namespace Application\Admin\Account;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\Middleware\Handler;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDateTime;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

use Model\Admin\Account as ModelAccount;

class Edit
{
    public $isRole = ['administrator'];

    public function setGetMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next) : Response {
            $validator = Validator::keySet(
                Validator::key('id', Validator::id(), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }


    public function get(Request $request) : Response
    {
        $params = $request->data();
        $html = $request->html();
        $html->account = ModelAccount::findOne(['_id' => new ObjectId($params['id'])]);
        return $html;
    }

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next) : Response {
            $validator = Validator::keySet(
                Validator::key('id', Validator::id(), true),
                Validator::key('nickname',Validator::stringType(),true),
                Validator::key('role', Validator::in(['editor', 'administrator']), true),
                Validator::key('password', Validator::oneOf(
                    Validator::stringType()->length(6),
                    Validator::equals('')
                ), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request) : Response
    {
        $params= $request->data();
        $id = $request->data('id');
        $is = ModelAccount::findOne([
            'nickname' => $params['nickname'],
            '_id' => [
                '$ne' => new ObjectID($id)
            ]
        ]);
        if (!empty($is)){
            return Html::error('用户已存在','返回重新输入','/account');
        }
        $password = $params['password'];
        if (!empty($params['password'])) {
            $params['password'] = [];
            $params['password']['str'] = hash_hmac('sha256', $password, $params['nickname']);
            $params['password']['must_reset'] = true;
        }
        $params['update_time'] = new UTCDateTime();
        $params = array_filter($params);
        unset($params['id']);
        $account = ModelAccount::findOneAndUpdate(['_id' => new ObjectID($id)], [
            '$set' => $params,
        ]);
        if (!empty($account))
            return Html::success('修改成功', '前往账号列表', '/account');
        return Html::error('修改失败', '返回重新输入');

    }
}
