<?php
namespace Application\Admin\Account;


use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\Middleware\Handler;

use Framework\Security;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

use Model\Admin\Account as ModelAccount;

class Reset
{
    public $isPublic = true;

    public function get(Request $request) : Response
    {
        $html = $request->html();
        $html->action = 'get';
        $html->count = ModelAccount::count([]);
        return $html;
    }

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next) : Response {
            $validator = Validator::keySet(
                Validator::key('nickname', Validator::nickname(), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request) : Response
    {
        $html = $request->html();
        $html->action = 'post';

        if (ModelAccount::count([]) === 0) {
            $nickname = $request->data('nickname');
            $password = Security::password();
            ModelAccount::new([
                'nickname' => $nickname,
                'password' => [
                    'str' => hash_hmac('sha256', $password, $nickname),
                ],
                'role' => 'administrator',
            ]);

            $html->success = true;
            $html->nickname = $nickname;
            $html->password = $password;

        } else {
            $html->success = false;
        }

        return $html;
    }
}


