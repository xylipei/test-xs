<?php
namespace Application\Admin\Account;


use Framework\DB;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\Middleware\Handler;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

use Model\Admin\Account as ModelAccount;

class Add
{

    public $isRole = ['administrator'];

    public function get(Request $request)
    {
        return $request->html();
    }

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next) : Response {
            $validator = Validator::keySet(
                Validator::key('nickname', Validator::nickname(), true),
                Validator::key('role', Validator::in(['editor', 'administrator']), true),
                Validator::key('password', Validator::stringType()->length(6), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request)
    {
        $params = $request->data(['role', 'nickname', 'password']);

        $is = DB::collection('account')->findOne([
            'nickname' => $params['nickname']
        ]);

        if (!empty($is)){
            return Html::error('用户已存在','返回重新输入','/account');
        }

        $password = hash_hmac('sha256', $params['password'], $params['nickname']);
        $account = ModelAccount::new([
            'nickname' => $params['nickname'],
            'password' => ['str' => $password]
        ]);

        if (!empty($account)) {
            return Html::success('添加成功', '前往账号列表', '/account');
        } else {
            return Html::error('添加失败', '返回重新输入');
        }
    }
}


