<?php

namespace Application\Admin;

use Framework\Http\Request;
use Framework\Http\Response;

use Model\Admin\Src as ModelSrc;

class Src
{
    public $isRole = ['administrator'];

    public function get(Request $request) : Response
    {
        $html = $request->html();
        $html->srcs = ModelSrc::find([])->toArray();
        return $html;
    }
}