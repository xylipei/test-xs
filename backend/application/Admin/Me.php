<?php

namespace Application\Admin;

use Framework\Http\Request;
use Framework\Http\Response;

class Me
{
    public function get(Request $request) : Response
    {
        $html = $request->html();
        $html->user = $request->user();
        return $html;
    }
}

