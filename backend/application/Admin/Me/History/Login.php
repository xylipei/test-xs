<?php

namespace Application\Admin\Me\History;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\View\Pagination;

use Model\Admin\History\Login as ModelLogin;
use MongoDB\BSON\ObjectId;

class Login
{
    public function get(Request $request) : Response
    {
        $user = $request->user();
        $html = $request->html();

        $filter = ['account_id' => new ObjectId($user['_id'])];

        $html->count = ModelLogin::count($filter);
        $html->pagination = new Pagination($html->count, 50);
        $html->logins = ModelLogin::find($filter, [
            'sort' => ['create_time' => -1],
            'skip' => $html->pagination->skip(),
            'limit' => $html->pagination->limit(),
        ])->toArray();
        return $html;
    }
}
