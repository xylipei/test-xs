<?php
namespace Application\Admin\Me;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Ip\Location;
use Framework\Middleware;
use Framework\Middleware\Handler;

use Framework\Security\Filter;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

use Model\Admin\Account as ModelAccount;
use Model\Admin\History\Login as ModelLogin;

class Login
{
    public $isPublic = true;

    public function get(Request $request)
    {
        return $request->html();
    }

    public function setPostMiddleware(Middleware $middleware) : void
    {
        $middleware->add(function (Request $request, Handler $next) : Response {
            $validator = Validator::keySet(
                Validator::key('nickname', Validator::nickname(), true),
                Validator::key('password', Validator::stringType()->length(6), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入', '/me/login');
            }
        });
    }

    public function post(Request $request) : Response
    {
        $params = $request->data(['nickname', 'password'], ['', '']);
        $password = hash_hmac('sha256', $params['password'], $params['nickname']);

        $ip = $request->ip();
        $location = Location::find($ip);
        $location = !empty($location) ? implode(':', $location) : '';
        $user_agent = Filter::str($request->user_agent());
        $session = new ObjectId();

        $history_login = [];
        $history_login['nickname'] = Filter::str($params['nickname']);
        $history_login['password'] = Filter::str($params['password']);
        $history_login['ip'] = $ip;
        $history_login['location'] = $location;
        $history_login['user_agent'] = $user_agent;
        $history_login['session'] = $session;

        $user = ModelAccount::findOneAndUpdate([
            'nickname' => $params['nickname'],
            'password.str' => $password,
        ], [
            '$set' => [
                'session' => $session,
                'authenticator.must_valid' => true,
                'login.last_time' => new UTCDateTime(),
                'login.ip' => $ip,
                'login.location' => $location,
                'login.user_agent' => Filter::str($user_agent),
            ],
            '$inc' => [
                'login.num' => 1
            ]
        ], [
            'returnDocument' => 2
        ]);

        if (!empty($user)) {
            $history_login['account_id'] = new ObjectId($user['_id']);
            if (($user['status'] ?? 'disabled') !== 'ok') {
                $history_login['status'] = 'disabled';
                $html = Html::error('登录失败', '账号已被禁用', '/me/login');
            } else {

                $request->cookie()->set('session', $user['session'], 86400);
                $html = Html::success('登录成功', '前往首页', '/');

                $history_login['status'] = 'ok';
                $history_login['password'] = '******';
            }

        } else {
            $html = Html::error('登录失败', '重新登录', '/me/login');
        }

        ModelLogin::new($history_login);

        return $html;
    }
}

