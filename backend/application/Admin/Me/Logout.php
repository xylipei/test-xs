<?php
namespace Application\Admin\Me;


use Framework\DB;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use MongoDB\BSON\ObjectID;


class Logout
{
    public function get(Request $request) : Response
    {
        $request->cookie()->remove('session');
        DB::collection('account')->findOneAndUpdate([
            '_id' => new ObjectID($request->user('_id'))
        ], [
            '$set' => ['session' => new ObjectID()]
        ]);
        return Html::success('退出成功', '重新登录', '/me/login');
    }
}

