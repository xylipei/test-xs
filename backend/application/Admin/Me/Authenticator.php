<?php

namespace Application\Admin\Me;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\Middleware\Handler;

use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDateTime;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

use Model\Admin\Account as ModelAccount;
use Framework\Security\Authenticator as AuthenticatorUtil;

class Authenticator
{
    public function get(Request $request) : Response
    {
        if (($user['authenticator']['bind'] ?? false) !== false) {
            return Html::error('已经绑定', '返回首页', '/');
        }

        $html = $request->html();
        $user = $request->user();
        $host = strtoupper(urlencode($request->host()));

        $html->url = base64_encode('otpauth://totp/'.ENVIRONMENT.':'.$user['nickname'].'?secret='.$user['authenticator']['secret'].'&issuer='.$host);
        return $html;
    }

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next) : Response {
            $validator = Validator::keySet(
                Validator::key('password', Validator::stringType()->length(6), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request) : Response
    {
        $user = $request->user();
        $password = $request->data('password');

        if (AuthenticatorUtil::valid($user['authenticator']['secret'] ?? '', $password)) {
            ModelAccount::findOneAndUpdate([
                '_id' => new ObjectId($user['_id']),
                'authenticator.bind' => false
            ], [
                '$set' => [
                    'authenticator.bind' => true,
                    'authenticator.bind_time' => new UTCDateTime()
                ]
            ]);
            return Html::success('绑定成功', '前往个人中心', '/me');
        }

        return Html::error('绑定失败', '返回重新输入');
    }
}
