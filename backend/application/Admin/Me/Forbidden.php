<?php
namespace Application\Admin\Me;

use Framework\Http\Request;

class Forbidden
{
    public $isPublic = true;

    public function get(Request $request)
    {
        return $request->html();
    }
}

