<?php

namespace Application\Admin\Me\Authenticator;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\Middleware\Handler;

use Framework\Security\Authenticator;
use Framework\Security\Filter;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDateTime;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

use Model\Admin\Account as ModelAccount;

class Valid
{
    public function get(Request $request) : Response
    {
        $html = $request->html();
        return $html;
    }

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next) : Response {
            $validator = Validator::keySet(
                Validator::key('password', Validator::stringType()->length(6), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request) : Response
    {
        $user = $request->user();
        $password = $request->data('password');

        if (Authenticator::valid($user['authenticator']['secret'] ?? '', $password)) {

            $account = ModelAccount::findOneAndUpdate([
                '_id' => new ObjectId($user['_id']),
                'authenticator.bind' => true
            ], [
                '$set' => [
                    'authenticator.last_valid_time' => new UTCDateTime(),
                    'authenticator.must_valid' => false
                ]
            ]);

            return Html::success('二步验证成功', '前往首页', '/');
        }

        return Html::error('二步验证失败', '返回重新输入');
    }
}
