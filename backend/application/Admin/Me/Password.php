<?php

namespace Application\Admin\Me;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\Middleware\Handler;

use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDateTime;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

use Model\Admin\Account as ModelAccount;

class Password
{
    public function get(Request $request) : Response
    {
        $html = $request->html();
        return $html;
    }

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next) : Response {
            $validator = Validator::keySet(
                Validator::key('password', Validator::stringType()->length(6), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request) : Response
    {
        $user = $request->user();
        $password = $request->data('password');
        $password = hash_hmac('sha256', $password, $user['nickname']);

        $account = ModelAccount::findOneAndUpdate([
            '_id' => new ObjectId($user['_id'])
        ], [
            '$set' => [
                'password.str' => $password,
                'password.last_reset_time' => new UTCDateTime(),
                'password.must_reset' => false
            ]
        ]);

        if (!empty($account)) {
            return Html::success('修改成功', '前往个人中心', '/me');
        } else {
            return Html::error('修改失败', '返回重新输入');
        }
    }
}
