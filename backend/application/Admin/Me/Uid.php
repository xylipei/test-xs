<?php

namespace Application\Admin\Me;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Html;
use Framework\Middleware;
use Framework\Middleware\Handler;

use MongoDB\BSON\ObjectID;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

use Model\Admin\Account as ModelAccount;
use Model\Api\User as ModelUser;

class Uid
{
    public function get(Request $request) : Response
    {
        $user = $request->user();
        $html = $request->html();
        $html->me = $user;

        if (!empty($user['uid'])) {
            $html->user = ModelUser::findOne(['_id' => new ObjectId($user['uid'])]);
        }

        return $html;
    }

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next) : Response {
            $validator = Validator::oneOf(Validator::keySet(
                Validator::key('action', Validator::equals('bind'), true),
                Validator::key('uid', Validator::id(), true)
            ), Validator::keySet(
                Validator::key('action', Validator::equals('unbind'), true)
            ));
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Html::error('输入数据不合法', '返回重新输入');
            }
        });
    }

    public function post(Request $request) : Response
    {
        $user = $request->user();
        $action = $request->data('action');

        if ($action == 'bind') {
            $uid = new ObjectId($request->data('uid'));
            $app_user = ModelUser::findOneAndUpdate(['_id' => new ObjectId($uid), 'isUser' => true, 'status' => 'accepted'], ['$set' => ['account_id' => $user['_id']]]);
            if (empty($app_user)) {
                return Html::error('前端用户不存在', '返回重新输入');
            }

            $update = ['$set' => ['uid' => $uid]];
        }

        if ($action == 'unbind') {
            $update = ['$unset' => ['uid' => '']];
        }

        $account = ModelAccount::findOneAndUpdate([
            '_id' => new ObjectId($user['_id'])
        ], $update);

        if (!empty($account)) {
            return Html::success('修改成功', '前往绑定前端用户页', '/me/uid');
        } else {
            return Html::error('修改失败', '返回重新输入');
        }
    }
}
