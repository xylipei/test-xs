<?php
namespace Application\Api;

use Framework\Middleware\Handler;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Json;
use Framework\Middleware;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDateTime;
use Framework\Payment;

use Model\Api\Order\Pre as ModelPre;
use Model\Api\Order as ModelOrder;
use Model\Api\User as ModelUser;

class Order
{
    public function setGetMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request,Handler $next) {
            $validator = Validator::keySet(
                Validator::key('order_id', Validator::id(), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Json::error('业务参数错误');
            }
        });
    }

    public function get(Request $request) : Response
    {
        $params = $request->data(['order_id']);
        $order = ModelOrder::findOne([
            '_id' => new ObjectID($params['order_id']),
            'user_id' => new ObjectID($request->user('_id'))
        ]);
        if (empty($order)) {
            return Json::error('订单不存在');
        }
        return Json::success([
            'order_id' => $order['_id'],
            'status' => $order['status']
        ]);
    }

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request,Handler $next) {
            $validator = Validator::keySet(
                Validator::key('action', Validator::in(['vip', 'balance']), true),
                Validator::key('pm', Validator::in(['wxh5', 'alih5']), true),
                Validator::key('id', Validator::id(), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Json::error('业务参数错误');
            }
        });
    }

    public function post(Request $request) : Response
    {
        $params = $request->data();
        $user = $request->user();

        $user_id = new ObjectID($user['_id']);

        $order = [];
        $order['pm'] = $params['pm'];
        $order['user_id'] = $user_id;
        $order['src'] = new ObjectID($request->x('src'));
        $order['package_name'] = $request->x('name');

        $order['create_time'] = new UTCDateTime();
        $order['status'] = 'create';

        switch ($params['action']) {
            case 'vip':
                $vip = ModelPre::vip($params['id']);
                $order['title'] = time();
                $order['memo'] = $vip['name'];
                $order['amount'] = $vip['amount'];
                $order['extra'] = [
                    'action' => 'vip',
                    'id' => $params['id']
                ];
                break;
            case 'balance':
                $balance = ModelPre::balance($params['id']);
                $order['title'] = time();
                $order['memo'] = $balance['name'];
                $order['amount'] = $balance['amount'];
                $order['extra'] = [
                    'action' => 'balance',
                    'id' => $params['id']
                ];
                break;
        }

        if ($id = ModelOrder::insertOne($order)) {
            ModelUser::updateOne(
                ['_id' => $user_id],
                [
                    '$inc' => ['order.num' => 1, 'order.amount' => $order['amount']],
                    '$set' => ['order.time' => new UTCDateTime()]
                ]
            );
            $order['_id'] = $id;
            $result = [];
            $result['order_id'] = $id;
            $result['url'] = Payment::make($order);
            $result['pm'] = $order['pm'];
            return Json::success($result);
        }
        return Json::error('下单失败');
    }
}
