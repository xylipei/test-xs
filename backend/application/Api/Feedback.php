<?php

namespace Application\Api;

use Framework\Middleware\Handler;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Json;
use Framework\Middleware;
use Framework\Str;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDateTime;

use Model\Api\Feedback as ModelFeedback;


class Feedback
{
    public function setGetMiddleware(Middleware $middleware): void
    {
        $middleware->add(function (Request $request,Handler $next) {
            try {
                $validator = Validator::keySet(
                    Validator::key('skip', Validator::numeric()->min(0), false)
                );
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $e) {
                return Json::error('业务参数错误');
            }
        });
    }

    public function get(Request $request): Response
    {
        $user = $request->user();
        $skip = $request->data('skip', ['skip' => 0]);
        $feedbacks = ModelFeedback::find(
            [
                'user_id' => new ObjectId($user['_id']),
                'action' => 'feedback'
            ],
            [
                'limit' => 20,
                'sort' => [
                    'create_time' => -1
                ],
                'skip' => (int)$skip
            ]
        )->toArray();
        $result = [];
        if (!empty($feedbacks)) foreach ($feedbacks as $feedback) {
            $feedback['feedback_id'] = $feedback['_id'];
            unset($feedback['_id']);
            $result[] = $feedback;
        }
        return Json::success($result);
    }

    public function setPostMiddleware(Middleware $middleware): void
    {
        $middleware->add(function (Request $request,Handler $next) {
            try {
                $validator = Validator::keySet(
                    Validator::key('content', Validator::stringType()->length(1, 500), true),
                    Validator::key('contact', Validator::oneOf(
                        Validator::stringType()->length(1, 100),
                        Validator::equals('')
                    ), false),
                    Validator::key('order_id', Validator::oneOf(
                        Validator::equals(''),
                        Validator::id()
                    ), false)
                );
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $e) {
                return Json::error('业务参数错误');
            }
        });
    }

    public function post(Request $request): Response
    {
        $user = $request->user();
        $params = $request->data();
        $params['user_id'] = new ObjectId($user['_id']);
        $params['content'] = Str::htmlentities($params['content']);
        $params['contact'] = Str::htmlentities($params['contact'] ?? '');
        $params['content'] = preg_replace("/(\n)|(\s)|(\t)|( )/", '', $params['content']);
        $params['create_time'] = new UTCDateTime();
        $params['action'] = 'feedback';
        $params['reply'] = [];
        $params['status'] = 'created';
        if (ModelFeedback::insertOne($params)) return Json::success();
        return Json::error('反馈失败');
    }
}
