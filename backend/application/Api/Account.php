<?php

namespace Application\Api;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Json;
use Framework\Middleware;
use Framework\Middleware\Handler;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use Framework\Str;

use Model\Api\User as ModelUser;

class Account
{
    public $accessRole = ['user'];

    public function get(Request $request) : Response
    {
        $user = $request->user();
        $vip = ($user['vip'] ?? 0) - time();
        $vip = $vip > 0 ? $vip : 0;
        return Json::success([
            'invite_num' => $user['invite']['num'] ?? 0,
            'user_id' => $user['_id'],
            'nickname' => $user['nickname'] ?? '',
            'session' => $user['session'],
            'balance' => $user['balance'] ?? 0,
            'vip' => $vip
        ]);
    }

    public function setPostMiddleware(Middleware $middleware): void
    {
        $middleware->add(function (Request $request,Handler $next) {
            try {
                $validator = Validator::keySet(
                    Validator::key('nickname', Validator::stringType(), true)
                );
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $e) {
                return Json::error('业务参数错误');
            }
        });
    }

    /**
     * 修改用户信息
     * @param Request $request
     * @return Response
     */

    public function post(Request $request): Response
    {
        $user = $request->user();
        $params = $request->data(['nickname']);
        $params['nickname'] = Str::htmlentities($params['nickname']);
        $params['update_time'] = new UTCDateTime();
        $update = (bool)ModelUser::findOneAndUpdate(
            ['_id' => new ObjectId($user['_id'])],
            ['$set' => $params]
        );
        return !empty($update) ? Json::success() : Json::error('修改失败');
    }
}
