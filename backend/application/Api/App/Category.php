<?php


namespace Application\Api\App;

use Framework\Http\Request;
use Framework\Http\Response\Json;
use Model\Api\App\Category as ModelAppCategory;

class Category
{
    public function get(Request $request)
    {
        $category = ModelAppCategory::findWithJoin(
            ['status' => 'accepted'],
            [
                'sort' => [
                    'sort' => 1
                ]
            ],
            ['title','_id','num']
        );
        return Json::success($category);
    }
}