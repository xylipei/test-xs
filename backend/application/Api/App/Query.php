<?php


namespace Application\Api\App;

use Framework\Middleware\Handler;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Json;
use Framework\Middleware;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use MongoDB\BSON\ObjectID;

use Model\Admin\App as ModelApp;


class Query
{
    public function setGetMiddleware(Middleware $middleware): void
    {
        $middleware->add(function (Request $request, Handler $next): Response {
            try {
                $validator = Validator::keySet(
                    Validator::key('skip', Validator::numeric(), false),
                    Validator::key('category_id', Validator::id(), false)
                );
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $e) {
                return Json::error('业务参数错误');
            }
        });
    }

    public function get(Request $request): Response
    {
        $params = $request->data(['skip', 'limit', 'category_id'], ['skip' => 0, 'limit' => 10]);
        if (!empty($params['category_id'])){
            $filter['category_id'] = new ObjectId($params['category_id']);
        }
        $filter['status'] = 'accepted';
        $options['limit'] = (int) $params['limit'];
        $options['skip'] = (int) $params['skip'];
        $app_list = ModelApp::findList($filter, $options);
        return Json::success($app_list);
    }
}