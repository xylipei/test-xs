<?php

namespace Application\Api\Order;

use Framework\Middleware\Handler;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Json;
use Framework\Middleware;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

use Model\Api\Order\Pre as ModelPre;

class Pre
{
    public function setGetMiddleware(Middleware $middleware): void
    {
        $middleware->add(function (Request $request,Handler $next) {
            try {
                $validator = Validator::keySet(
                    Validator::key('action', Validator::in(['vip', 'balance']), true)
                );
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $e) {
                return Json::error('业务参数错误');
            }
        });
    }

    public function get(Request $request): Response
    {
        $params = $request->data();
        $result = [];
        $result['tip'] = '近期支付系统不稳定，如果充值失败请于晚间重新尝试。';
        $result['list'] = ModelPre::{$params['action']}();
        $result['pm'] = ['wxh5', 'alih5'];
        return Json::success($result);
    }
}

