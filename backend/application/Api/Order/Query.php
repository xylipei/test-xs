<?php

namespace Application\Api\Order;


use Framework\Middleware\Handler;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Json;
use Framework\Middleware;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use MongoDB\BSON\ObjectID;

use Model\Api\Order as ModelOrder;

class Query
{
    public function setGetMiddleware(Middleware $middleware): void
    {
        $middleware->add(function (Request $request,Handler $next) {
            $validator = Validator::keySet(
                Validator::key('skip', Validator::numeric()->min(0), false)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Json::error('业务参数错误');
            }
        });
    }

    public function get(Request $request): Response
    {
        $user = $request->user();
        $params = $request->data(['skip'], ['skip' => 0]);
        $orders = ModelOrder::find(
            [
                'user_id' => new ObjectID($user['_id']),
                'status' => 'paid'
            ],
            [
                'sort' => ['create_time' => -1],
                'skip' => (int)$params['skip'] * 20,
                'limit' => 20
            ]
        )->toArray();
        
        $result = [];
        if (!empty($orders)) foreach ($orders as $order) {
            $order['order_id'] = $order['_id'];
            unset($order['_id']);
            $result[] = $order;
        }
        return Json::success($result);
    }
}