<?php

namespace Application\Api\Account;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Json;
use Framework\Middleware;
use Framework\Middleware\Handler;
use Framework\Ip\Location;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

use Model\Api\Device as ModelDevice;
use Model\Api\User as ModelUser;

class Login
{
    public $accessRole = ['none'];

    public function setPostMiddleware(Middleware $middleware) : void
    {
        $middleware->add(function (Request $request, Handler $next) : Response {
            try {
                $validator = Validator::keySet(
                    Validator::key('id', Validator::regex('/[0-9a-z]{32}/'), true)
                );
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $e) {
                return Json::error('业务参数错误');
            }
        });
    }

    public function post(Request $request) : Response
    {
        $id = md5($request->data('id').$request->x('name'));
        $ip = $request->ip();
        $location = Location::find($ip);
        $location = !empty($location) ? implode(':', $location) : '';
        $user = ModelUser::findOneAndUpdate(
            [
                'account.id' => $id,
            ],
            [
                '$set' => [
                    'session' => new ObjectId()
                ],
                '$inc' => [
                    'login.num' => 1
                ],
                '$setOnInsert' => [
                    'nickname' => '游客',
                    'account' => [
                        'id' => $id
                    ],
                    'invite' => [
                        'num' => 0,
                    ],
                    'login.time' => new UTCDateTime(),
                    'login.ip' => $ip,
                    'login.location' => $location,
                    'vip' => 0,
                    'balance' => 200,
                    'src' => new ObjectId($request->x('src')),
                    'device_id' => $request->x('device_id'),
                    'os' => $request->x('os'),
                    'create_time' => new UTCDateTime(),
                    'status' => 'accepted'
                ]
            ],
            [
                'returnDocument' => 2,
                'upsert' => true
            ]
        );
        $today = strtotime('today');
        if (($today >  $user['login']['time']) && ($user['balance'] < 2000) ){
            $user = ModelUser::findOneAndUpdate(
                [
                    'account.id' => $id,
                ],
                [
                    '$set' => [
                        'session' => new ObjectId(),
                        'login.time' => new UTCDateTime()
                    ],
                    '$inc' => [
                        'balance' => EVERY_DAY_BALANCE
                    ]
                ],
                [
                    'returnDocument' => 2,
                    'upsert' => true
                ]
            );
        }
        if (!empty($user)) {
            ModelUser::findOneAndUpdate(
                [
                    'account.id' => $id,
                ],
                [
                    '$set' => [
                        'login.time' => new UTCDateTime()
                    ]
                ]
            );
            $vip = ($user['vip'] ?? 0) - time();
            $vip = $vip > 0 ? $vip : 0;
            return Json::success([
                'user_id' => $user['_id'],
                'nickname' => $user['nickname'] ?? '',
                'session' => $user['session'],
                'balance' => $user['balance'] ?? 0,
                'vip' => $vip
            ]);
        }
        return Json::error('登录失败');
    }
}
