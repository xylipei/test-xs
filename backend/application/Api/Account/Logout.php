<?php

namespace Application\Api\Account;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Json;
use MongoDB\BSON\ObjectId;
use Model\Api\User as ModelUser;

class Logout
{
    public $accessRole = ['user'];

    public function post(Request $request) : Response
    {
        ModelUser::findOneAndUpdate(['_id' => new ObjectId($request->user('_id'))], [
            '$set' => ['session' => new ObjectId()]
        ], [
            'returnDocument' => 2
        ]);
        return Json::success();
    }
}
