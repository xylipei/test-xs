<?php

namespace Application\Api;

use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Json;
use MongoDB\BSON\ObjectID;

use Model\Admin\Kw as ModelKw;
use Model\Api\Book as ModelBook;

class Kw
{
    public function get(Request $request): Response
    {
        $package_id = $request->x('package_id');
        $kw = ModelKw::findOneAndUpdate(
            [
                'package.package_id' => new ObjectId($package_id),
                'status' => 'accepted'
            ],
            ['$inc' => ['view_num' => 1]]
        );
        $book = ModelBook::findOneInfo([
            '_id' => new ObjectID($kw['book_id'])
        ]);
        if (empty($book['title'])){
            $book = [];
        }
        if (empty($book)) return Json::success(new \stdClass);
        return Json::success($book);
    }
}