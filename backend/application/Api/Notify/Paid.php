<?php
namespace Application\Api\Notify;

use Framework\Middleware\Handler;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Json;
use Framework\Middleware;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDateTime;
use Framework\Payment;
use Framework\Http\Response\Raw;

use Model\Api\Order\Pre as ModelPre;
use Model\Api\Order as ModelOrder;
use Model\Api\User as ModelUser;


class Paid
{
    public $isApi = false;
    public $isPrivate = false;

    public function setPostMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request,Handler $next) {
            parse_str(html_entity_decode(file_get_contents('php://input')), $data);
            $request->setData($data);
            $validator = Validator::keySet(
                Validator::key('mch_id', Validator::stringType(), true),
                Validator::key('payment_time', Validator::notEmpty(), true),
                Validator::key('total_fee', Validator::notEmpty(), true),
                Validator::key('trade_no', Validator::notEmpty(), true),
                Validator::key('out_trade_no', Validator::id(), true),
                Validator::key('sign', Validator::notEmpty(), true)
            );

            try {
                $validator->assert($request->data());
                $params = $request->data();
                $sign = $params['sign'] ?? '';
                unset($params['sign']);

                if (strcasecmp(Payment::sign($params), $sign) !== 0) {
                    return Json::error('请求签名错误');
                }
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Json::error('业务参数错误');
            }
        });
    }


    public function post(Request $request) : Response
    {
        ignore_user_abort(true);
        $params = $request->data();
        $order = ModelOrder::findOneAndUpdate(
            ['_id' => new ObjectID($params['out_trade_no']), 'status' => 'create'],
            ['$set' => [
                'trade_no' => $params['trade_no'],
                'trade_time' => new UTCDateTime(\DateTime::createFromFormat('YmdHis', $params['payment_time'])->getTimestamp() * 1000),
                'status' => 'paid',
            ]],
            ['returnDocument' => 2]
        );

        if (empty($order)) {
            return Raw::error('fail');
        }

        $update = [];
        $update['$inc']['order.paid.num'] = 1;
        $update['$inc']['order.paid.amount'] = (int) $order['amount'];
        $update['$set']['order.paid.time'] = new UTCDateTime();

        if (!empty($order)) {
            switch ($order['extra']['action']) {
            case 'vip':
                $vip = ModelPre::vip($order['extra']['id']);
                $user = ModelUser::findOne(['_id' => new ObjectId($order['user_id'])]);
                if (($user['vip'] ?? 0) > time()) {
                    $update['$inc']['vip'] = $vip['num'];
                } else {
                    $update['$set']['vip'] = time() + $vip['num'];
                }
                break;
            case 'balance':
                $balance = ModelPre::balance($order['extra']['id']);
                $update['$inc']['balance'] = $balance['num'];
                break;
        }
        }

        $result = ModelUser::findOneAndUpdate(
            ['_id' => new ObjectID($order['user_id'])],
            $update
        );

        if (isset($result) && $result) {
            return Raw::success('success');
        } else {
            return Raw::error('fail');
        }
    }
}

