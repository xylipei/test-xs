<?php


namespace Application\Api\Book;

use Framework\Middleware\Handler;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Json;
use Framework\Middleware;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use MongoDB\BSON\ObjectID;

use Model\Api\Book\Bookshelf as ModelBookshelf;
use Model\Api\Book as ModelBook;
use Model\Api\Book\Chapter as ModelBookChapter;

class History
{
    public function setPostMiddleware(Middleware $middleware): void
    {
        $middleware->add(function (Request $request, Handler $next) {
            $validator = Validator::keySet(
                Validator::key('book_id', Validator::id(), true),
                Validator::key('chapter_id', Validator::id(), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $e) {
                return Json::error('业务参数错误');
            }
        });
    }

    public function post(Request $request): Response
    {
        $user = $request->user();
        $book_id = new ObjectID($request->data('book_id'));
        $chapter_id = new ObjectID($request->data('chapter_id'));
        $book = ModelBook::findOne([
            '_id' => $book_id,
            'status' => 'accepted'
        ]);
        if (empty($book)) {
            return Json::error('非法小说');
        }
        $chapter = ModelBookChapter::findOne([
            '_id' => $chapter_id,
            'book_id' => $book_id,
            'status' => 'accepted'
        ]);
        if (empty($chapter)) {
            return Json::error('非法章节');
        }
        $usleep = rand(20000, 10000);
        usleep($usleep);
        $history = ModelBookshelf::updateHistory($chapter_id, $book_id, $user);
        if (empty($history)) return Json::error('上报失败');
        return Json::success();
    }
}