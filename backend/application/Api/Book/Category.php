<?php


namespace Application\Api\Book;

use Framework\Http\Request;
use Framework\Http\Response\Json;

use Model\Api\Book\Category as ModelBookCategory;

class Category
{
    public function get(Request $request)
    {
        $category = ModelBookCategory::findWithJoin(
            ['status' => 'accepted'],
            [
                'sort' => [
                    'sort' => 1
                ]
            ],
            ['title','_id','num','thumb']
        );
        return Json::success($category);
    }
}