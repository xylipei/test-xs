<?php


namespace Application\Api\Book;

use Framework\Middleware\Handler;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Json;
use Framework\Middleware;

use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDateTime;

use Model\Api\Book\Chapter\Content as ModelBookChapterContent;
use Model\Api\Book\Chapter as ModelBookChapter;
use Model\Api\Bill as ModelBill;
use Model\Api\Book as ModelBook;
use Model\Api\User as ModelUser;

class Chapter
{
    public function setGetMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next) {
            $validator = Validator::keySet(
                Validator::key('book_id', Validator::id(), true),
                Validator::key('chapter_id', Validator::oneOf(
                    Validator::id(),
                    Validator::equals('')
                ), false)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Json::error('输入数据不合法');
            }
        });
    }

    public function get(Request $request): Response
    {
        $user = $request->user();
        $book_id = new ObjectID($request->data('book_id'));
        $book = ModelBook::findOne([
            '_id' => $book_id,
            'status' => 'accepted'
        ]);
        if (empty($book)){
            return Json::error('非法ID');
        }
        if (!empty($request->data('chapter_id'))){
            $chapter_id = new ObjectId($request->data('chapter_id'));
            $chapter = ModelBookChapter::findOne(
                [
                    '_id' => $chapter_id,
                    'status' => 'accepted'
                ]
            );
            if (empty($chapter)){
                return Json::error('非法ID');
            }
        }else{
            $chapter = ModelBookChapter::findOne(
                [
                    'book_id' => $book_id,
                    'status' => 'accepted'
                ],
                [
                    'sort' => [
                        'sort' => 1
                    ]
                ]
            );
            $chapter_id = new ObjectID($chapter['_id']);
        }

        $content = ModelBookChapterContent::findOne(
            ['chapter_id' => $chapter_id]
        );

        if (empty($content)){
            return Json::error('非法ID');
        }

        $result = [];
        $result['paid_balance'] = 0;
        $book['free_chapter_num'] = isset($book['free_chapter_num']) ? $book['free_chapter_num'] : 200;
        $book['free'] = isset($book['free']) ? $book['free'] : 1;
        if (!isset($chapter['free'])){
            $chapter['free'] = 0;
            if ($book['free_chapter_num'] > $chapter['sort']){
                $chapter['free'] = 1;
            }
        }
        $book['vip'] = isset($book['vip']) ? $book['vip'] : 0;
        if ($book['vip'] && ($user['vip'] < time())){
            return Json::error('会员专享', USER_NOT_VIP);
        }
        if (!$book['free'] && !ModelBill::paid($user['_id'], $content['_id']) && !$chapter['free']){
            $balance_price = (int) ($book['chapter_balance_price'] ?? CHAPTER_PRICE);
            if (($user['balance'] ?? 0) < $balance_price) {
                return Json::error('余额不足', USER_NOT_BALANCE);
            }
            $update = ModelUser::findOneAndUpdate(
                [
                    '_id' => new ObjectId($user['_id']),
                    'balance' => ['$gte' => $balance_price]
                ],
                [
                    '$inc' => ['balance' => - $balance_price]
                ]
            );
            if (empty($update)) {
                return Json::error('购买失败', USER_NOT_BALANCE);
            }
            $bill = ModelBill::insertOne(
                [
                    'user_id' => new ObjectId($user['_id']),
                    'book' => [
                        'book_id' => new ObjectId($book['_id']),
                        'title' => $book['title'] ?? null,
                        'thumb' => $book['thumb'] ?? null
                    ],
                    'chapter' => [
                        'chapter_id' => new ObjectId($chapter['_id']),
                        'title' => $chapter['title'] ?? null,
                    ],
                    'status' => 'paid',
                    'create_time' => new UTCDateTime()
                ]
            );
            if (!$bill) {
                return Json::error('购买失败');
            }
            $result['paid_balance'] = $balance_price;
        }

        $result = [
            'content_id' => $content['_id'],
            'content' => $content['content']
        ];
        return Json::success($result);
    }
}