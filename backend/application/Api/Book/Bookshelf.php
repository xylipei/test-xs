<?php


namespace Application\Api\Book;

use Framework\Middleware\Handler;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Json;
use Framework\Middleware;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use MongoDB\BSON\ObjectID;

use Model\Api\Book\Bookshelf as ModelBookshelf;
use Model\Api\Book as ModelBook;

class Bookshelf
{
    public function setGetMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next) {
            $validator = Validator::keySet(
                Validator::key('action', Validator::in(['shelf', 'history']), true),
                Validator::key('skip', Validator::numeric()->min(0), false)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Json::error('输入数据不合法');
            }
        });
    }

    public function get(Request $request): Response
    {
        $user = $request->user();
        $action = $request->data('action');
        $filter['user_id'] = new ObjectID($user['_id']);
        $options['sort'] = [
            'update_time' => -1
        ];
        switch ($action) {
            case 'shelf':
                $filter['status'] = 'accepted';
                $filter['is_collect'] = 1;
                $result = ModelBookshelf::findHistory($filter, $options);
                break;
            default:
                $filter['status'] = 'accepted';
                $options['skip'] = (int) ($request->data('skip') ?? 0);
                $options['limit'] = 20;
                $filter['chapter_id'] = [
                    '$exists' => true
                ];
                $result = ModelBookshelf::findHistory($filter, $options);
                break;
        }
        if (empty($result)){
            $result = [];
        }
        return Json::success($result);
    }

    public function setPostMiddleware(Middleware $middleware): void
    {
        $middleware->add(function (Request $request, Handler $next) {
            $validator = Validator::keySet(
                Validator::key('books_id', Validator::arrayType()->each(
                    Validator::id()
                ), true),
                Validator::key('action', Validator::in(['set', 'unset']), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $e) {
                return Json::error('业务参数错误');
            }
        });
    }

    public function post(Request $request): Response
    {
        $user = $request->user();
        $params = $request->data();
        $book_id_array = [];
        foreach ($params['books_id'] as $book_id) {
            $book = ModelBook::findOne([
                '_id' => new ObjectId($book_id),
                'status' => 'accepted'
            ]);
            if (empty($book)){
                return Json::error('非法ID');
            }
            $book_id_array[] = new ObjectId($book_id);
        }
        $like = ModelBookshelf::change($book_id_array, $user, $params['action']);
        if ($like['error']) return Json::error($like['message']);
        return Json::success($like['message']);
    }
}