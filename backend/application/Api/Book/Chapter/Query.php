<?php
namespace Application\Api\Book\Chapter;

use Framework\Middleware\Handler;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Json;
use Framework\Middleware;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use MongoDB\BSON\ObjectID;

use Model\Api\Book\Chapter as ModelBookChapter;
use Model\Api\Book as ModelBook;


class Query
{
    public function setGetMiddleware(Middleware $middleware)
    {
        $middleware->add(function (Request $request, Handler $next) {
            $validator = Validator::keySet(
                Validator::key('book_id', Validator::id(), true)
            );
            try {
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $exception) {
                return Json::error('输入数据不合法');
            }
        });
    }


    public function get(Request $request): Response
    {
        $book_id = $request->data('book_id');
        $book = ModelBook::findOne([
            '_id' => new ObjectId($book_id),
            'status' => 'accepted'
        ]);
        if (empty($book)){
            return Json::error('非法ID');
        }
        $chapter_list = ModelBookChapter::findWithJoin(
            [
                'book_id' => new ObjectId($book_id),
                'status' => 'accepted'
            ],
            [
                'sort' => [
                    'sort' => 1
                ]
            ],
            ['_id', 'title', 'content_id', 'book_id', 'free']
        );
        return Json::success($chapter_list);
    }
}