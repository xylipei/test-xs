<?php


namespace Application\Api\Book;

use Framework\Middleware\Handler;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Json;
use Framework\Middleware;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use MongoDB\BSON\ObjectID;

use Model\Api\Book as ModelBook;


class Query
{
    public function setGetMiddleware(Middleware $middleware): void
    {
        $middleware->add(function (Request $request, Handler $next): Response {
            try {
                $validator = Validator::keySet(
                    Validator::key('category_id', Validator::id(), false),
                    Validator::key('author_id', Validator::id(), false),
                    Validator::key('action', Validator::in(['ranking', 'search']), false),
                    Validator::key('search', Validator::stringType(), false),
                    Validator::key('skip', Validator::numeric()->min(0), false),
                    Validator::key('limit', Validator::numeric(), false)
                );
                $validator->assert($request->data());
                return $next->handle($request);
            } catch (NestedValidationException $e) {
                return Json::error('业务参数错误');
            }
        });
    }

    public function get(Request $request): Response
    {
        $user = $request->user();
        $params = $request->data(['category_id', 'skip', 'limit', 'author_id','action','search'], ['skip' => 0, 'limit' => 10]);
        $filter = ['status' => 'accepted'];
        $options['limit'] = (int) $params['limit'];
        $options['skip'] = (int) $params['skip'];
        foreach ($params as $name => $value) {
            if (empty($params[$name])){
                continue;
            }
            switch ($name) {
                case 'category_id':
                case 'author_id':
                    $filter[$name] = new ObjectId($value);
                    break;
                case 'action':
                    switch($value){
                        case 'ranking':
                            $filter['ranking'] = ['$exists' => true];
                            $options['limit'] = 20;
                            $options['sort'] = [
                                'ranking.sort' => 1
                            ];
                            break;
                        case 'search':
                            $filter['ranking'] = ['$exists' => true];
                            $options['limit'] = 10;
                            $options['sort'] = [
                                'ranking.sort' => 1
                            ];
                            $options['skip'] = 0;
                            break;
                    }
                    break;
                case 'search':
                    $filter['$or'] =  [
                        [
                            'title' => [
                                '$regex' => trim($value)
                            ]
                        ],
                        [
                            'author' => [
                                '$regex' => trim($value)
                            ]
                        ]
                    ];
                    unset($options['limit']);
                    break;
            }
        }
        $book_list = ModelBook::findList($filter, $options,$user);
        return Json::success($book_list);
    }
}