<?php
namespace Application\Api\Book\Author;

use Framework\Middleware\Handler;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Response\Json;
use Framework\Middleware;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;
use MongoDB\BSON\ObjectID;

use Model\Api\Book as ModelBook;
use Model\Api\Book\Author as ModelBookAuthor;

class Query
{
//    public function setGetMiddleware(Middleware $middleware)
//    {
//        $middleware->add(function (Request $request, Handler $next) {
//            $validator = Validator::keySet(
//                Validator::key('initials', Validator::regex('/[A-Z]/')->max(1), true)
//            );
//            try {
//                $validator->assert($request->data());
//                return $next->handle($request);
//            } catch (NestedValidationException $exception) {
//                return Json::error('输入数据不合法');
//            }
//        });
//    }

    public function get(Request $request): Response
    {
        $skip = $request->data('skip');
        $author = ModelBookAuthor::findList(
            [
                'num.book' => [
                    '$gt' => 0
                ]
            ],
            [
                'sort' => [
                    'initials' => 1
                ],
                'limit' => 30,
                'skip' => (int) $skip
            ],
            ['_id', 'is_hot', 'name', 'num']
        );

        return Json::success($author);
    }

}