<?php
use Framework\View;
echo (new View('admin/header'));
?>

<div class="col-lg-12">

<?php if (!empty($db)) foreach ($db as $dbName => $dbInfo):?>

    <div class="panel panel-info">
        <div class="panel-heading"><?php echo $dbName;?> : <?php echo $dbInfo['env']['database'];?></div>
        <?php if (!empty($dbInfo['collections'])):?>
        <table class="table table-bordered">
            <thead>
                <th class="col-lg-1">表名</th>
                <th class="col-lg-1">空间</th>
                <th class="col-lg-1">内存</th>
                <th class="col-lg-1">对象数</th>
                <th class="col-lg-1">平均大小</th>
                <th class="col-lg-1">索引数</th>
                <th class="col-lg-1">索引大小</th>
            </thead>
            <tbody>
            <?php foreach ($dbInfo['collections'] as $collectionName => $collectionInfo):?>
            <tr>
                <td><?php echo $collectionName;?></td>
                <td><?php echo $collectionInfo['storageSize'] ?? 0;?></td>
                <td><?php echo $collectionInfo['size'] ?? 0;?></td>
                <td><?php echo $collectionInfo['count'] ?? 0;?></td>
                <td><?php echo $collectionInfo['avgObjSize'] ?? 0;?></td>
                <td><?php echo $collectionInfo['nindexes'] ?? 0;?></td>
                <td><?php echo $collectionInfo['totalIndexSize'] ?? 0;?></td>
            </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <?php else:?>
        <div class="panel-body">
        <div class="alert alert-danger">
            没有数据表
        </div>
        </div>
        <?php endif;?>
    </div>
<?php endforeach;?>
</div>

<?php echo (new View('admin/footer'));?>
