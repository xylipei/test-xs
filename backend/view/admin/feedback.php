<?php

use Framework\View;

echo(new View('admin/header'));
?>

<style type="text/css">
    .popover {
        max-width: 100%;
    }
</style>

<script type="text/javascript">
    $(function () {
        $('[data-toggle="popover"]').popover({
            container: 'body'
        })
    })
</script>


<div class="col-lg-12">
    <div class="panel panel-info">
        <div class="panel-heading clearfix">
            <span class="pull-left">用户反馈</span>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr class="bg-warning">
                <th>用户ID</th>
                <th>用户权限</th>
                <th>渠道</th>
                <th>支付</th>
                <th class="col-lg-1">订单</th>
                <th class="">反馈内容</th>
                <th class="col-lg-1">联系方式</th>
                <th class="">时间</th>
                <td>状态</td>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <?php if (!empty($feedbacks)) foreach ($feedbacks as $feedback): ?>
                <tr>
                    <td><?php echo $feedback['user_id'] ?? ''; ?></td>

                    <td>
                        <?php if (!empty($feedback['user'])):?>
                            <?php $user = current($feedback['user']);?>
                            <?php if (($user['vip'] ?? 0 - time()) > 0):?>
                                <span style="margin-left: 5px;" class="btn btn-xs btn-danger">会员</span>
                            <?php endif;?>
                            <?php if (($user['balance'] ?? 0) > 0):?>
                                <span style="margin-left: 5px;" class="btn btn-xs btn-danger"><?php echo $user['balance'];?></span>
                            <?php endif;?>
                        <?php endif;?>
                    </td>
                    <td>
                        <?php echo $srcs[$user['src']??''] ?? '';?>
                    </td>
                    <td>
                        <?php echo (($user['order']['amount'] ?? 0)/100).' / '.(($user['order']['paid']['amount'] ?? 0)/100);?>
                    </td>
                    <td>
                        <?php if (!empty($feedback['order'])):?>
                            <span
                                    class="btn btn-xs btn-success"
                                    data-toggle="popover"
                                    data-html="true"
                                    data-trigger="hover"
                                    data-content='<table class="table table-bordered">
                        <thead>
                            <tr class="bg-info">
                                <th>项目</th>
                                <th>金额</th>
                                <th>下单时间</th>
                                <th>付款时间</th>
                                <th>状态</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($feedback['order'] as $order):?>
                        <tr>
                            <td><?php echo $order['extra']['action'] ??'';?></td>
                            <td><?php echo (($order['amount'] ??0)/100);?></td>
                            <td><?php echo date('Y-m-d H:i:s', $order['create_time']);?></td>
                            <td><?php if(!empty($order['trade_time'])) echo date('Y-m-d H:i:s', $order['trade_time']);?></td>
                            <td><?php echo $order['status'] ?? '';?></td>
                        </tr>
                        <?php endforeach;?>
                        <tbody>
                        </table>
                        '>
                            <?php echo count($feedback['order']);?> 个订单
                        </span>
                        <?php endif;?>
                    </td>
                    <td><?php echo mb_substr($feedback['content'] ?? '', 0, 50); ?></td>
                    <td><?php echo $feedback['contact'] ?? ''; ?></td>
                    <td><?php echo date('Y-m-d H:i:s', $feedback['create_time']); ?></td>
                    <td>
                        <?php
                        switch ($feedback['status'] ?? '') {
                            case 'created':
                                echo '<span class="label label-default">未处理</span>';
                                break;
                            case 'ok':
                                echo '<span class="label label-success">已处理</span>';
                                break;
                        }
                        ?>
                    </td>
                    <td>
                        <div>
                            <a class="btn btn-xs btn-primary"
                               href="/feedback/reply?feedback_id=<?php echo $feedback['_id']; ?>">处理</a>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="panel-footer">
            <?php if (!empty($pagination)): ?>
                <?php echo $pagination; ?>
            <?php endif; ?>
        </div>
    </div>

</div>

<?php echo(new View('admin/footer')); ?>
