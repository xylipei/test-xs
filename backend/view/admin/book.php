<?php
use Framework\View;
echo (new View('admin/header'));
?>

<style>
    span{
        vertical-align: text-top;
    }
</style>

<?php echo (new View('admin/book/menu'))->set('on', 'book');?>

<div class="col-lg-12">
    <div class="panel panel-info">
        <div class="panel-body">
            <form method="get" class="form-horizontal" autocomplete="off">
                <div class="form-group">
                    <div class="col-lg-12">
                        <div class="pull-left" style="margin-right: 10px;">
                            <p><label class="label label-default">筛选分类</label></p>
                            <select class="select" name="category_id">
                                <option value="">全部</option>
                                <?php if (!empty($categorys)) foreach ($categorys as $category):?>
                                    <option <?php if ($params['category_id'] == $category['_id']):?>selected<?php endif;?> value="<?php echo $category['_id']??'';?>"><?php echo $category['title']??'';?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="pull-left" style="margin-right: 10px;">
                            <p><label class="label label-default">VIP</label></p>
                            <select class="select" name="vip">
                                <option value="">全部</option>
                                <option <?php if ($params['vip'] == '1'):?> selected<?php endif;?> value="1">是</option>
                                <option <?php if ($params['vip'] == '0'):?> selected<?php endif;?> value="0">否</option>
                            </select>
                        </div>
                        <div class="pull-left" style="margin-right: 10px;">
                            <p><label class="label label-default">是否免费</label></p>
                            <select class="select" name="free">
                                <option value="">全部</option>
                                <option <?php if ($params['free'] == '1'):?> selected<?php endif;?> value="1">是</option>
                                <option <?php if ($params['free'] == '0'):?> selected<?php endif;?> value="0">否</option>
                            </select>
                        </div>
                        <div class="pull-left" style="margin-right: 10px;">
                            <p><label class="label label-default">筛选状态</label></p>
                            <select class="select" name="status">
                                <option value="">全部</option>
                                <option <?php if (!empty($params['status']) && $params['status'] == 'draft'):?> selected<?php endif;?> value="draft">草稿</option>
                                <option <?php if (!empty($params['status']) && $params['status'] == 'accepted'):?> selected<?php endif;?> value="accepted">发布</option>
                                <option <?php if (!empty($params['status']) && $params['status'] == 'trashed'):?> selected<?php endif;?> value="trashed">隐藏</option>
                                <option <?php if (!empty($params['status']) && $params['status'] == 'deleted'):?> selected<?php endif;?> value="deleted">删除</option>
                            </select>
                        </div>
                        <div class="pull-left" style="margin-right: 10px;">
                            <p><label class="label label-default">筛选作者</label></p>
                            <input name="author" class="form-control" placeholder="作者名字" <?php if (!empty($params['author'])):?> value="<?php echo $params['author'];?>"<?php endif;?>>
                        </div>
                        <div class="pull-left" style="margin-right: 10px;">
                            <p><label class="label label-default">书名搜索</label></p>
                            <input name="search" class="form-control" placeholder="书名" <?php if (!empty($params['search'])):?> value="<?php echo $params['search'];?>"<?php endif;?>>
                        </div>
                        <div class="pull-left" style="margin-right: 10px;">
                            <p><label class="label label-default">排序</label></p>
                            <select class="select" name="sort">
                                <option <?php if (!empty($params['sort']) && $params['sort'] == 'create_time'):?> selected<?php endif;?> value="create_time">时间</option>
                                <option <?php if (!empty($params['sort']) && $params['sort'] == 'num.view'):?> selected<?php endif;?> value="num.view">浏览</option>
                            </select>
                        </div>
                        <div class="pull-left" style="margin-right: 10px;">
                            <p>&nbsp;</p>
                            <input type="submit" class="form-control btn-primary" value="筛选">
                        </div>
                    </div>
                </div>

            </form>
        </div>
        <div class="panel-heading clearfix">
            <span class="pull-left">全部</span>
            <a class="pull-right" href="/book/add"><span class="glyphicon glyphicon-plus-sign"></span></a>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr class="bg-warning">
                <td class="col-lg-1">ID</td>
                <td>标题</td>
                <td>作者</td>
                <td>类型</td>
                <td>封面</td>
                <td>连载状态</td>
                <td>章节数</td>
                <td>是否vip</td>
                <td>是否免费</td>
                <td>状态</td>
                <td>操作
                    <a class="pull-right" style="margin-right: 5px;" href="/book/upset">
                        <button class="btn btn-danger btn-xs" type="button">
                            乱序 <span class="glyphicon glyphicon-refresh"></span>
                        </button>
                    </a>
                </td>
            </tr>
            </thead>
            <tbody>
            <?php if (!empty($books)) foreach ($books as $book):?>
                <tr>
                    <td class="bg-warning">
                        <a href="/book/chapter?book_id=<?php echo $book['book_id'];?>"><?php echo $book['book_id'];?></a>
                    </td>
                    <td>
                       <span  class="" data-toggle="popover" data-content="<?php echo $book['title']??''?>">
                            <?php if (mb_strlen($book['title']??'','utf-8') > 5):?>
                                <?php echo mb_substr($book['title']??'', 0, 5,'utf-8')?>……
                            <?php else:?>
                                <?php echo $book['title']??'' ?>
                            <?php endif;?>
                        </span>
                    </td>
                    <td><?php echo $book['author'] ?? '';?></td>
                    <td><?php echo $book['category']??'';?></td>
                    <td>
                        <?php if (!empty($book['thumb'])):?>
                            <img src="<?php echo $book['thumb'];?>" width="100px" height="100px"/>
                        <?php endif;?>
                    </td>
                    <td><?php if ($book['finish']):;?><span class="label label-primary">完结</span><?php else:?><span class="label label-info">连载</span><?php endif;?></td>
                    <td><?php echo $book['num']['chapter'] ?? 0;?></td>
                    <td>
                        <?php
                        switch ($book['vip'] ?? '') {
                            case '1':
                                echo '<span class="label label-success">是</span>';
                                break;
                            default:
                                echo '<span class="label label-default">否</span>';
                                break;
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        switch ($book['free'] ?? '') {
                            case '1':
                                echo '<span class="label label-success">是</span>';
                                break;
                            default:
                                echo '<span class="label label-default">否</span>';
                                break;
                        }
                        ?>
                    </td>

                    <td>
                        <?php
                        switch ($book['status'] ?? '') {
                            case 'accepted':
                                echo '<span class="label label-success">已发布</span>';
                                break;
                            case 'draft':
                                echo '<span class="label label-default">草稿</span>';
                                break;
                            case 'trashed':
                                echo '<span class="label label-warning">隐藏</span>';
                                break;
                            case 'deleted':
                                echo '<span class="label label-danger">已删除</span>';
                                break;
                        }
                        ?>
                    </td>
                    <td>
                        <a class="btn btn-xs btn-info" href="/book/chapter?book_id=<?php echo $book['book_id'];?>"><span class="glyphicon glyphicon-align-left"></span> 章节管理</a>
                        <a class="btn btn-xs btn-primary" href="/book/edit?book_id=<?php echo $book['book_id'];?>"><span class="glyphicon glyphicon-edit"></span> 修改</a>
                        <?php if (!isset($book['ranking'])):?>
                        <a class="btn btn-xs btn-warning" href="/book/ranking/edit?book_id=<?php echo $book['book_id'];?>"><span class="glyphicon glyphicon-plus-sign"></span>加入风云榜</a>
                        <?php endif;?>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>

        <div class="panel-footer">
            <?php if (isset($pagination)) echo $pagination;?>
        </div>

    </div>
</div>

<?php echo (new View('admin/footer'));?>
