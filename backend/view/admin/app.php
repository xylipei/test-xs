<?php
use Framework\View;
echo (new View('admin/header'));
?>
<?php echo (new View('admin/app/menu'))->set('on', 'app');?>
<div class="col-lg-12">
    <div class="panel panel-info">
        <div class="panel-heading clearfix">
            <span class="pull-left">应用列表</span>
            <a class="pull-right" href="/app/add"><span class="glyphicon glyphicon-plus-sign"></span></a>
        </div>
        <div class="panel-body">
            <form method="get" class="form-horizontal" autocomplete="off">
                <div class="form-group">
                    <div class="col-lg-12">
                        <div class="pull-left" style="margin-right: 10px;">
                            <p><label class="label label-default">筛选分类</label></p>
                            <select class="select" name="category_id">
                                <option value="">全部</option>
                                <?php if (!empty($categorys)) foreach ($categorys as $category):?>
                                    <option <?php if ($category_id == $category['_id']):?>selected<?php endif;?> value="<?php echo $category['_id']??'';?>"><?php echo $category['title']??'';?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="pull-left" style="margin-right: 10px;">
                            <p><label class="label label-default">应用名称</label></p>
                            <input name="app_name" class="form-control" placeholder="应用名称" <?php if (!empty($app_name)):?> value="<?php echo $app_name;?>"<?php endif;?>>
                        </div>
                        <div class="pull-left" style="margin-right: 10px;">
                            <p>&nbsp;</p>
                            <input type="submit" class="form-control btn-primary" value="筛选">
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr class="bg-warning">
                <td>ID</td>
                <td>应用名称</td>
                <td>应用分类</td>
                <td class="col-lg-2">资源</td>
                <td>状态</td>
                <td>操作</td>
            </tr>
            </thead>
            <tbody>
            <?php if (!empty($apps)) foreach ($apps as $app):?>
                <tr>
                    <td class="bg-warning col-lg-1">
                        <?php echo $app['_id'];?>
                    </td>
                    <td><?php echo $app['app_name']??'';?></td>
                    <td><?php echo $app['category']??'';?></td>
                    <td>
                        <a class="btn btn-xs btn-info" href="<?php echo $app['apk_url'];?>" target="_blank">安装包</a>
                        <a class="btn btn-xs btn-info" href="<?php echo $app['icon'];?>" target="_blank">图标</a>
                    </td>
                    <td>
                        <?php
                        switch ($app['status'] ?? '') {
                            case 'accepted':
                                echo '<span class="label label-success">已发布</span>';
                                break;
                            case 'draft':
                                echo '<span class="label label-default">草稿</span>';
                                break;
                            case 'trashed':
                                echo '<span class="label label-warning">隐藏</span>';
                                break;
                            case 'deleted':
                                echo '<span class="label label-danger">已删除</span>';
                                break;
                        }
                        ?>
                    </td>
                    <td>
                        <a class="btn btn-xs btn-primary" href="/app/edit?app_id=<?php echo $app['_id'];?>">修改</a>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <div class="panel-footer">
            <?php if (!empty($pagination)):?>
                <?php echo (new View('pagination'))->set('pagination', $pagination);?>
            <?php endif;?>
        </div>
    </div>


</div>

<?php echo (new View('admin/footer'));?>
