<?php
use Framework\View;
use Framework\Ip\Location;

echo (new View('admin/header'));
?>


<div class="col-lg-12">
    <div class="panel panel-info">
        <div class="panel-body">
            <form method="get" class="form-horizontal" autocomplete="off">
                <div class="form-group">
                    <div class="col-lg-12">
                        <div class="pull-left" style="margin-right: 10px;">
                            <p><label class="label label-default">用户ID</label></p>
                            <input name="user_id" class="form-control" placeholder="用户ID" <?php if (!empty($params['user_id'])):?> value="<?php echo $params['user_id'];?>"<?php endif;?>>
                        </div>
                        <div class="pull-left" style="margin-right: 10px;">
                            <p><label class="label label-default">用户搜索</label></p>
                            <input name="search" class="form-control" placeholder="用户昵称" <?php if (!empty($params['search'])):?> value="<?php echo $params['search'];?>"<?php endif;?>>
                        </div>
                        <div class="pull-left" style="margin-right: 10px;">
                            <p><label class="label label-default">筛选状态</label></p>
                            <select class="select" name="status">
                                <option value="">全部</option>
                                <option <?php if (!empty($params['status']) && $params['status'] == 'draft'):?> selected<?php endif;?> value="draft">草稿</option>
                                <option <?php if (!empty($params['status']) && $params['status'] == 'accepted'):?> selected<?php endif;?> value="accepted">发布</option>
                                <option <?php if (!empty($params['status']) && $params['status'] == 'trashed'):?> selected<?php endif;?> value="trashed">隐藏</option>
                                <option <?php if (!empty($params['status']) && $params['status'] == 'deleted'):?> selected<?php endif;?> value="deleted">删除</option>
                            </select>
                        </div>
                        <div class="pull-left" style="margin-right: 10px;">
                            <p><label class="label label-default">排序</label></p>
                            <select class="select" name="sort">
                                <option <?php if (!empty($params['sort']) && $params['sort'] == 'login.time'):?> selected<?php endif;?> value="login.time">时间</option>
                                <option <?php if (!empty($params['sort']) && $params['sort'] == 'invite.num'):?> selected<?php endif;?> value="invite.num">活跃</option>
                            </select>
                        </div>
                        <div class="pull-left" style="margin-right: 10px;">
                            <p>&nbsp;</p>
                            <input type="submit" class="form-control btn-primary" value="筛选">
                        </div>
                    </div>
                </div>

            </form>
        </div>
        <div class="panel-heading clearfix">
            <span class="pull-left">用户列表</span>
            <a class="pull-right" href="/board/add"><span class="glyphicon glyphicon-plus-sign"></span></a>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr class="bg-warning">
                <td>ID</td>
                <td>设备ID</td>
                <td>昵称</td>
                <td>登录时间</td>
                <td>登录地址</td>
                <td>状态</td>
                <td>操作</td>
            </tr>
            </thead>
            <tbody>
            <?php if (!empty($users)) foreach ($users as $user):?>
                <tr>
                    <td class="bg-warning"><?php echo $user['_id'];?></td>
                    <td><?php echo $user['device_id'] ?? '';?></td>
                    <td><?php echo $user['nickname'] ?? '';?></td>
                    <td>
                        <?php if (!empty($user['login']['time'])):?>
                            <?php echo date('Y-m-d H:i:s', $user['login']['time']);?>
                        <?php endif;?>
                    </td>
                    <td><?php echo $user['login']['location'] ?? '';?></td>
                    <td>
                        <?php
                        $status = [
                            'accepted' => '<span class="label label-success">正常</span>',
                            'deleted' => '<span class="label label-danger">屏蔽</span>',
                        ];
                        echo $status[$user['status'] ?? ''] ?? '';
                        ?>
                    </td>
                    <td>
                        <?php if (!empty($user['account_id'])):?>
                            <span class="btn btn-xs btn-danger disabled">已绑</span>
                        <?php else:?>
                            <a class="btn btn-xs btn-primary" href="/user/edit?id=<?php echo $user['_id'];?>">修改</a>
                        <?php endif;?>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <div class="panel-footer">
            <?php if (!empty($pagination)):?>
                <?php echo $pagination;?>
            <?php endif;?>
        </div>
    </div>

</div>

<?php echo (new View('admin/footer'));?>
