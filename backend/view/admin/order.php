<?php
use Framework\View;
echo (new View('admin/header'));
?>

<div class="col-lg-12">
    <div class="panel panel-info">
        <div class="panel-heading clearfix">
            <form method="get" class="form-inline" autocomplete="off">
                <input name="order_id" class="form-control" placeholder="ID">
                <select name="is_vip" class="form-control">
                    <option value="" <?php echo empty($filter['is_vip']) ? 'selected' : '' ?>>不限</option>
                    <option value="true" <?php if(!empty($filter['is_vip'])) : ?><?php if($filter['is_vip'] == 'true') : ?>selected<?php endif;?><?php endif;?>>会员</option>
                    <option value="false" <?php if(!empty($filter['is_vip'])) : ?><?php if($filter['is_vip'] == 'false') : ?>selected<?php endif;?><?php endif;?>>普通</option>
                </select>
                <button class="btn btn-sm btn-primary" type="submit">查询</button>
            </form>
        </div>

        <div class="panel-heading clearfix">
            <span class="pull-left">订单列表</span>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr class="bg-warning">
                <th class="col-lg-1">ID</th>
                <th class="col-lg-1">渠道</th>
                <th class="col-lg-1">用户ID</th>
                <th class="" style="width: 50px;">支付</th>
                <th class="col-lg-1">支付订单</th>
                <th class="" style="width: 50px;">项目</th>
                <th class="" style="width: 80px;">金额/元</th>
                <th class="">状态</th>
                <th class="">时间</th>
                <th class="">付款时间</th>
            </tr>
            </thead>
            <tbody>
            <?php if (!empty($orders)) foreach ($orders as $order):?>
                <tr <?php if ($order['status'] == 'paid'):?> class="bg-success"<?php endif;?>>
                    <td><?php echo $order['_id'];?></td>
                    <td><?php echo $srcs[$order['src']] ?? '';?></td>
                    <td><?php echo $order['user_id'];?></td>
                    <td><span class="label label-info"><?php echo $order['pm'] ?? '';?></span></td>
                    <td><?php echo $order['trade_no'] ?? '';?></td>
                    <td><span class="label label-info"><?php echo $order['extra']['action'] ?? '';?></span></td>
                    <td><?php echo $order['amount']/100;?></td>
                    <td><?php echo $order['status'];?></td>
                    <td><?php echo date('Y-m-d H:i:s', $order['create_time']);?></td>
                    <td><?php if (!empty($order['trade_time'])) echo date('Y-m-d H:i:s', $order['trade_time']);?></td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <div class="panel-footer">
            <?php if (isset($pagination)) echo $pagination;?>
        </div>
    </div>

</div>


<?php echo (new View('admin/footer'));?>
