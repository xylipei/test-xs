<?php
use Framework\View;
echo (new View('admin/header'));?>

<div class="col-lg-6">
    <div class="panel panel-info">
        <div class="panel-heading panel-info">
            修改账号
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post">
                <input name="id" type="hidden" value="<?php echo $account['_id']??'';?>">
                <div class="form-group">
                    <div class="col-lg-6">
                        <input class="form-control" name="nickname" type="text" placeholder="用户名" value="<?php echo $account['nickname']??'';?>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-6">
                        <input class="form-control" name="password" type="password" placeholder="密码">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-3">
                        <select class="select form-control" name="role">
                            <option<?php if (($account['role'] ?? null) == 'editor'):?> selected="selected"<?php endif;?> value="editor">编辑</option>
                            <option<?php if (($account['role'] ?? null) == 'administrator'):?> selected="selected"<?php endif;?> value="administrator">管理员</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-primary">提交保存</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="panel-heading panel-danger">
            禁用账号
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post" action="/account/disable">
                <input name="id" type="hidden" value="<?php echo $account['_id']??'';?>">
                <div class="form-group">
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-danger">禁用账号</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="panel-heading panel-danger">
            重置二步验证
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post" action="/account/authenticator">
                <input name="id" type="hidden" value="<?php echo $account['_id']??'';?>">
                <div class="form-group">
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-danger">重置二步验证</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php echo (new View('admin/footer'));?>
