<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>管理后台 - w(´･ω･`)w </title>
    <link href="/dist/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
        #login {
            background: #fff;
            position:fixed;
            top:50%;
            left: 50%;
            margin-top:-300px;
            margin-left: -190px;
            width: 330px;
            border:1px solid #ddd;
            padding: 20px 50px;

        }

        .box {
            position:fixed;
            top:50%;
            left: 50%;
            margin-top:-300px;
            margin-left: -190px;
            width: 330px;
            padding: 20px 50px;
        }

    </style>
</head>
<body style="background:#f1f1f1; ">
    <?php if ($action == 'get'):?>
        <?php if ($count === 0):?>
            <div id="login">
                <form class="form-horizontal" method="post">
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="text" autocomplete="off" name="nickname" class="form-control" placeholder="用户名">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <button type="submit" class="col-md-12 btn btn-danger">初始化</button>
                        </div>
                    </div>
                </form>
            </div>
        <?php else:?>
            <div class="box">
                <div class="alert alert-danger">
                    MAKE MORE MONEY
                </div>
            </div>
        <?php endif;?>
    <?php endif;?>

    <?php if ($action == 'post'):?>
        <?php if ($success == true):?>
            <div class="box">
                <div class="alert alert-success">
                    <p><?php echo $nickname;?></p>
                    <p><?php echo $password;?></p>
                </div>
            </div>
        <?php else:?>
            <div class="box">
                <div class="alert alert-danger">
                    MAKE MORE MONEY
                </div>
            </div>
        <?php endif;?>
    <?php endif;?>

</body>
</html>

