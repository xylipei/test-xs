<?php
use Framework\View;
use Framework\Security;
echo (new View('admin/header'));?>

<div class="col-lg-6">
    <div class="panel panel-info">
        <div class="panel-heading panel-info">
            添加管理员
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post">
                <div class="form-group">
                    <div class="col-lg-6">
                        <input autocomplete="off" class="form-control" name="nickname" type="text" placeholder="用户名">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-6">
                        <input autocomplete="off" class="form-control" name="password" type="text" placeholder="密码" value="<?php echo Security::password();?>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-3">
                        <select class="select form-control" name="role">
                            <option value="editor">编辑</option>
                            <option value="administrator">管理员</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-primary">提交保存</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php echo (new View('admin/footer'));?>
