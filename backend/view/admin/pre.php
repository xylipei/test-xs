<?php
use Framework\View;
echo (new View('admin/header'));
?>

<script type="text/javascript" src="/dist/js/echarts.js"></script>
<script type="text/javascript" src="/dist/js/jquery-3.3.1.min.js"></script>

<style type="text/css">
    .popover{max-width: 100%;}
</style>

<script type="text/javascript">
    $(function () {
        $('.actionbox').click(function(){
            $('.actionlist').addClass('hide');
            $(this).find('.actionlist').removeClass('hide');
        });
    })
</script>
<div class="col-lg-12">
    <ul class="nav nav-tabs" style="margin-bottom: 20px;">
        <li class="<?php if ($action == 'vip') echo 'active'; ?>"><a href="/pre">VIP</a></li>
        <li class="<?php if ($action == 'balance') echo 'active'; ?>"<?php ?> ><a href="/pre?action=balance">金币</a></li>
    </ul>
</div>
<div class="col-lg-12" style="margin-bottom: 150px;">
    <div class="panel panel-info">
        <div class="panel-heading clearfix">
            <span class="pull-left">套餐列表</span>
            <a class="pull-right" href="/pre/add"><span class="glyphicon glyphicon-plus-sign"></span></a>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr class="bg-warning">
                <td class="col-lg-1">ID</td>
                <td>标题</td>
                <td>副标题</td>
                <td>简介</td>
                <td>付费(分)</td>
                <td>
                    <?php if ($action == 'vip'):?>
                        <?php echo '时间(天)';?>
                    <?php else:?>
                        <?php echo '金币数(个)';?>
                    <?php endif?>
                </td>
                <th>状态</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <?php if (!empty($pres)) foreach ($pres as $k => $pre):?>
                <tr>
                    <td class="bg-warning"><?php echo $pre['_id'];?></td>
                    <td><?php echo $pre['name']??'';?></td>
                    <td><?php echo $pre['tip']??'';?></td>
                    <td><?php echo $pre['memo']??'';?></td>
                    <td><span class="label label-success"><?php echo $pre['amount']??'';?></span></td>
                    <td>
                        <span class="label label-warning">
                        <?php if ($pre['action'] == 'vip'):?>
                            <?php echo $pre['num'] / 86400;?>
                        <?php else:?>
                            <?php echo $pre['num']??0;?>
                        <?php endif?>
                        </span>
                    </td>
                    <td>
                        <?php
                        switch ($pre['status'] ?? '') {
                            case 'accepted':
                                echo '<span class="label label-success">已发布</span>';
                                break;
                            case 'draft':
                                echo '<span class="label label-default">草稿</span>';
                                break;
                            case 'trashed':
                                echo '<span class="label label-warning">隐藏</span>';
                                break;
                        }
                        ?>
                    </td>
                    <td>
                        <a class="btn btn-xs btn-primary" href="/pre/edit?pre_id=<?php echo $pre['_id']??'';?>">修改</a>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>
<?php echo (new View('admin/footer'));?>
