<?php
use Framework\View;
use Framework\Security;
echo (new View('admin/header'));?>


<script type="text/javascript" src="/dist/uploader/fine-uploader.js"></script>
<link type="text/css" href="/dist/uploader/fine-uploader.css">

<script type="text/template" id="qq-template">
    <div class="qq-uploader-selector qq-uploader clearfix" qq-drop-area-text="Drop files here">
        <div class="pull-left qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals" style="margin-left: 15px;">
            <span class="qq-upload-size-selector qq-upload-size"></span>
            <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
            <div class="progress qq-progress-bar-container-selector" style="padding-left: 0; padding-right: 0;">
                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-info qq-progress-bar-selector qq-progress-bar"></div>
            </div>
        </div>
    </div>
</script>

<script type="text/javascript">
    $(document).ready(function(){
        var ops = {
            autoUpload: true,
            debug: true,
            multiple: false,
            maxConnections: 5,
            request: {
                endpoint: '/upload',
                method: 'POST',
                inputName: 'file'
            },
            retry: {
                enableAuto: false,
                maxAutoAttempts: 3,
                autoAttemptDelay: 5,
            },
            validation: {
                allowedExtensions: ['png', 'jpeg', 'jpg', 'mp4'],
                stopOnFirstInvalidFile: false,
            },
            cors: {
                expected: false,
            }
        };

        new qq.FineUploader($.extend(ops, {
            element: document.getElementById('icon'),
            button: document.getElementById('icon-btn'),
            callbacks: {
                onComplete: function(id, name, responseJSON, maybeXhr) {
                    if (responseJSON.error === 0) {
                        $('#icon').val(responseJSON.url);
                        $('#icon-show').html('<img style="margin: 10px 0;" width="200" src="'+responseJSON.url+'"/>');
                    }
                    return true;
                }
            }
        }));
    });
</script>

<div class="col-lg-6">
    <div class="panel panel-info">
        <div class="panel-heading panel-info">
            添加应用
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post">
                <div class="form-group">
                    <label for="app_name" class="col-lg-2 control-label">应用名称</label>
                    <div class="col-lg-6">
                        <input id="app_name" autocomplete="off" class="form-control" name="app_name" type="text" placeholder="应用名称">
                    </div>
                </div>
                <div class="form-group">
                    <label for="package_name" class="col-lg-2 control-label">应用包名</label>
                    <div class="col-lg-6">
                        <input id="package_name" autocomplete="off" class="form-control" name="package_name" type="text" placeholder="应用包名">
                    </div>
                </div>
                <div class="form-group">
                    <label for="tags" class="col-lg-2 control-label">应用分类</label>
                    <div class="col-lg-5 clearfix" style="line-height: 34px">
                        <select class="selectpicker pull-left" name="category">
                            <option>请选择分类</option>
                            <?php if (!empty($categorys)) foreach ($categorys as $category):?>
                                <option value="<?php echo $category['title']??'';?>"><?php echo $category['title']??'';?></option>
                            <?php endforeach;?>
                        </select>
                        <div class="pull-right">
                            <a class="btn btn-xs btn-primary " href="/app/category/add"><span class="glyphicon glyphicon-plus-sign"></span>添加应用分类</a>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label  class="col-lg-2 control-label">简介</label>
                    <div class="col-lg-6">
                        <textarea  class="form-control" name="memo" rows="8" placeholder="请填写简介"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="url" class="col-lg-2 control-label">链接地址</label>
                    <div class="col-lg-6">
                        <input id="url" autocomplete="off" class="form-control" name="apk_url" type="text" placeholder="链接地址">
                    </div>
                </div>
                <input id="icon" name="icon" type="hidden" value="">
                <div class="form-group">
                    <label for="title" class="col-lg-2 control-label">图标</label>
                    <div class="col-lg-10 clearfix">
                        <div class="clearfix">
                            <div class="btn btn-default pull-left" id="icon-btn">选择图标</div>
                            <div id="icon"></div>
                        </div>
                        <div id="icon-show"></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-12 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary">提交保存</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php echo (new View('admin/footer'));?>
