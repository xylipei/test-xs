<?php
use Framework\View;
echo(new View('admin/header'));
?>

<?php echo (new View('admin/app/menu'))->set('on', 'category');?>
<style>
    span{
        vertical-align: text-top;
    }
</style>
<form method="post">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading clearfix">
                <span class="pull-left">应用分类列表</span>
                <a class="pull-right" href="/app/category/add"><span class="glyphicon glyphicon-plus-sign"></span></a>
            </div>
            <table class="table table-bordered">
                <thead>
                <tr class="bg-warning">
                    <td class="" style="width: 50px;">排序</td>
                    <td class="col-lg-1">ID</td>
                    <td>标题</td>
                    <td>应用总数</td>
                    <td>添加时间</td>
                    <td>更新时间</td>
                    <td>状态</td>
                    <td>操作</td>
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($categorys)) foreach ($categorys as $category):?>
                    <tr>
                        <td class="">
                            <input name="id[]" type="hidden" value="<?php echo $category['_id'];?>">
                            <input name="sort[]" type="text" style="width: 30px; text-align: center;" value="<?php echo isset($category['sort']) ? (int) $category['sort'] : 100;?>">
                        </td>
                        <td class="bg-warning">
                            <?php echo $category['_id'];?>
                        </td>
                        <td><?php echo $category['title'] ?? '';?></td>
                        <td><?php echo $category['num']['app'] ?? 0;?></td>
                        <td><?php echo empty($category['create_time']) ? '' : date('Y-m-d H:i', $category['create_time']);?></td>
                        <td><?php echo empty($category['update_time']) ? '' : date('Y-m-d H:i', $category['update_time']);?></td>
                        <td>
                            <?php
                            switch ($category['status'] ?? '') {
                                case 'accepted':
                                    echo '<span class="label label-success">已发布</span>';
                                    break;
                                case 'draft':
                                    echo '<span class="label label-default">草稿</span>';
                                    break;
                                case 'trashed':
                                    echo '<span class="label label-warning">隐藏</span>';
                                    break;
                            }
                            ?>
                        </td>
                        <td>
                            <a class="btn btn-xs btn-primary" href="/app/category/edit?category_id=<?php echo $category['_id'];?>"><span class="glyphicon glyphicon-edit"></span> 修改</a>
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>

            <div class="panel-footer clearfix">
                <div class="pagination pull-left" style="margin-right: 20px;">
                    <button type="submit" class="btn btn-md btn-primary">保存排序</button>
                </div>
                <?php if (isset($pagination)) echo $pagination;?>
            </div>

        </div>
    </div>
</form>
<?php echo (new View('admin/footer'));?>
