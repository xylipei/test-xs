<?php
use Framework\View;
echo (new View('admin/header'));
?>

<div class="col-lg-12">
    <div class="panel panel-info">
        <div class="panel-heading clearfix">
            <span class="pull-left">管理员列表</span>
            <a class="pull-right" href="/account/add"><span class="glyphicon glyphicon-plus-sign"></span></a>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr class="bg-warning">
                <td class="col-lg-1">ID</td>
                <td class="col-lg-1">角色</td>
                <td class="col-lg-1">名称</td>
                <td class="col-lg-2" colspan="2">二步验证</td>
                <td class="col-lg-1">状态</td>
                <td class="col-lg-1">添加时间</td>
                <td class="col-lg-1">登录时间</td>
                <td class="col-lg-1">登录地址</td>
                <td class="col-lg-1">操作</td>
            </tr>
            </thead>
            <tbody>
            <?php if (!empty($accounts)) foreach ($accounts as $account):?>
                <tr>
                    <td class="bg-warning">
                        <?php echo $account['_id'];?>
                    </td>
                    <td>
                    <?php
                        switch (($account['role']??'')) {
                            case 'administrator':
                                echo '<span class="text-danger">管理员</span>';
                                break;
                            case 'editor':
                                echo '<span class="text-info">编辑</span>';
                                break;
                            default:
                                echo '<span class="text-warning">未知</span>';
                                break;
                        }
                    ?>
                    </td>
                    <td><?php echo $account['nickname'];?></td>
                    <td><?php if ($account['authenticator']['bind'] ?? false) echo '<span class="label label-success">已绑</span>';?></td>
                    <td><?php if (!empty($account['authenticator']['last_time'])) echo date('Y-m-d H:i:s', $account['authenticator']['last_time']);?></td>

                    <td><?php echo $account['status'];?></td>
                    <td><?php echo date('Y-m-d H:i:s', $account['create_time']);?></td>
                    <td><?php if (!empty($account['login']['last_time'])) echo date('Y-m-d H:i:s', $account['login']['last_time']);?></td>
                    <td><?php echo $account['login']['location'] ?? '';?></td>
                    <td><a class="btn btn-xs btn-primary" href="/account/edit?id=<?php echo $account['_id'];?>">修改</a></td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <div class="panel-footer">
            <?php if (!empty($pagination)):?>
            <?php echo $pagination;?>
            <?php endif;?>
        </div>
    </div>

</div>

<?php echo (new View('admin/footer'));?>
