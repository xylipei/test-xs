<?php
use Framework\View;
echo (new View('admin/header'));
?>

<script type="text/javascript" src="/dist/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/dist/locales/bootstrap-datepicker.zh-CN.min.js"></script>
<link type="text/css" rel="stylesheet" href="/dist/css/bootstrap-datepicker3.css">
<style type="text/css">
    .bg1 {background: #ffffff;}
    .bg2 {background: #f9f9f9;}
</style>
<script type="text/javascript">
    $(document).ready(function(){
        $('.choose').on('choose', function(){

            var value = $(this).find('input').val();
            $(this).find('span.btn').each(function() {
                if ($(this).data('value') == value) {
                    $(this).addClass('btn-danger').removeClass('btn-default');
                } else {
                    $(this).addClass('btn-default').removeClass('btn-danger');
                }
            });
        });


        $('.choose').trigger('choose');

        $('.choose span.btn').click(function(){
            var value = $(this).data('value');
            $(this).parent().parent().find('input').val(value);
            $(this).parent().parent().trigger('choose');
        });

        $('.input-daterange').datepicker({
            format: "yyyy-mm-dd",
            weekStart: 0,
            endDate: "<?php echo date('Y-m-d');?>",
            maxViewMode: 1,
            language: "zh-CN",
            autoclose: true
        });
    });
</script>

<div class="col-lg-10">
    <div class="panel panel-info">
        <div class="panel-heading clearfix">
            筛选
        </div>
        <div class="panel-body">
            <form class="form-horizontal" autocomplete="off">
                <div class="form-group">
                    <label class="col-sm-1 control-label">选择日期</label>
                    <div class="col-sm-11">
                        <div class="input-daterange clearfix">
                            <div class="pull-left" style="margin-right: 5px;">
                                <input type="text" class="input-sm form-control" name="date[start]" value="<?php echo ($params['date']['start'] ?? date('Y-m-d'));?>" placeholder="开始日期" />
                            </div>
                            <div class="pull-left">
                                <input type="text" class="input-sm form-control" name="date[end]" value="<?php echo ($params['date']['end'] ?? date('Y-m-d'));?>" placeholder="结束日期" />
                            </div>
                        </div>
                    </div>
                </div>
                <?php if (!empty($srcs)):?>
                    <div class="form-group choose" data-group="appsrc">
                        <input name="src" value="<?php echo ($params['src'] ?? '');?>" type="hidden">
                        <label class="col-sm-1 control-label">选择渠道</label>
                        <div class="col-sm-11">
                            <span data-appnames="" data-value="" class="btn btn-sm btn-danger" style="margin: 5px 5px 5px 0;">全部</span>
                            <?php foreach ($srcs as $src):?>
                                <span data-value="<?php echo $src['_id'];?>" class="btn btn-sm btn-default" style="margin: 5px 5px 5px 0;"><?php echo ($src['name']);?></span>
                            <?php endforeach;?>
                        </div>
                    </div>
                <?php endif;?>
                <div class="form-group">
                    <div class="col-sm-11 col-sm-offset-1">
                        <button type="submit" class="btn btn-md btn-primary">筛选查询</button>
                        <a style="margin-left: 20px;" href="/state" class=""><span class="glyphicon glyphicon-remove-circle"></span></a>
                    </div>
                </div>
            </form>
        </div>
        <div class="panel-heading clearfix">
            <span class="pull-left">数据统计</span>
        </div>
        <table class="table table-bordered table-hover">
            <thead>
            <tr class="bg-warning">
                <th>渠道</th>
                <th>日期</th>
                <th>订单数</th>
                <th>订单总金额</th>
                <th>支付订单数</th>
                <th>支付订单总金额</th>
                <th>微信</th>
                <th>支付宝</th>
                <th>微信APP</th>
                <th>手续费</th>
                <th>毛利</th>
                <th>分成金额</th>
                <th>实际安装数</th>
                <th>返回安装数</th>
                <th>盈利</th>
            </tr>
            </thead>
            <tbody>
            <?php if (empty($result)):?>
                <tr>
                    <td colspan="16">
                        <div class="alert alert-danger" style="margin: 15px;">
                            没有数据
                        </div>
                    </td>
                </tr>
            <?php else:?>
                <?php $total = [
                    'order_num' => 0,
                    'order_amount' => 0,
                    'order_paid_num' => 0,
                    'order_paid_amount' => 0,
                    'order_paid_wx_amount' => 0,
                    'order_paid_wxapp_amount' => 0,
                    'order_paid_ali_amount' => 0,
                    'money_tmp' => 0,
                    'fee' => 0,
                    'amount' => 0,
                    'real_active' => 0,
                    'active' => 0,
                    'money' => 0
                ]; foreach ($result as $r):?>
                    <tr>
                        <td><?php echo ($srcs[$r['src']]['name'] ?? $r['src']);?></td>
                        <td><?php echo $r['date'];?></td>
                        <td><?php $total['order_num']+= $r['order_num']; echo $r['order_num'];?></td>
                        <td><?php $total['order_amount']+= $r['order_amount']; echo $r['order_amount']/100;?></td>
                        <td><?php $total['order_paid_num']+= $r['order_paid_num']; echo $r['order_paid_num'];?></td>
                        <td class="bg-success"><?php $total['order_paid_amount']+= $r['order_paid_amount']; echo $r['order_paid_amount']/100;?></td>
                        <td><?php $total['order_paid_wx_amount']+= $r['order_paid_wx_amount']; echo $r['order_paid_wx_amount']/100;?></td>
                        <td><?php $total['order_paid_ali_amount']+= $r['order_paid_ali_amount']; echo $r['order_paid_ali_amount']/100;?></td>
                        <td><?php $total['order_paid_wxapp_amount']+= $r['order_paid_wxapp_amount']; echo $r['order_paid_wxapp_amount']/100;?></td>
                        <td><?php
                            if (time() > strtotime('2019-01-31')) {
                                $fee = ceil($r['order_paid_ali_amount'] * 0.4 + $r['order_paid_wx_amount'] * 0.4 + $r['order_paid_wxapp_amount'] * 0.4);
                            } else {
                                $fee = ceil($r['order_paid_ali_amount'] * 0.18 + $r['order_paid_wx_amount'] * 0.13 + $r['order_paid_wxapp_amount'] * 0.32);
                            }
                            $total['fee'] += $fee; echo $fee/100;
                            ?></td>
                        <td class="bg-danger"><?php
                            $money_tmp = $r['order_paid_amount'] - $fee;
                            $total['money_tmp'] += $money_tmp;
                            echo $money_tmp/100;
                            ?>
                        </td>
                        <td class="bg-warning"><?php
                                $rate = 0.2;
                            $amount = ceil(($r['order_paid_amount'] - $fee) * $rate);
                            $amount = floor($amount/300) * 300;
                            $total['amount']+= $amount;
                            echo $amount/100;
                            ?></td>
                        <td><?php $real_active = ($user[$r['src']][$r['date']] ?? 0); $total['real_active'] += $real_active; echo $real_active;?></td>
                        <td><?php $active = floor($amount/300); $total['active']+= $active; echo $active;?></td>
                        <td><?php $money = ($r['order_paid_amount'] - $amount - $fee); $total['money']+= $money; echo $money/100;?></td>
                    </tr>
                <?php endforeach;?>
                <tr class="bg-info">
                    <th colspan="2" style="text-align: right;">总计</th>
                    <th><?php echo $total['order_num'];?></th>
                    <th><?php echo $total['order_amount']/100;?></th>
                    <th><?php echo $total['order_paid_num'];?></th>
                    <th><?php echo $total['order_paid_amount']/100;?></th>
                    <th><?php echo $total['order_paid_wx_amount']/100;?></th>
                    <th><?php echo $total['order_paid_ali_amount']/100;?></th>
                    <th><?php echo $total['order_paid_wxapp_amount']/100;?></th>
                    <th><?php echo $total['fee']/100;?></th>
                    <th><?php echo $total['money_tmp']/100;?></th>
                    <th><?php echo $total['amount']/100;?></th>
                    <th><?php echo $total['real_active'];?></th>
                    <th><?php echo $total['active'];?></th>
                    <th><?php echo $total['money'] /100;?></th>
                </tr>
            <?php endif;?>
            </tbody>
        </table>
    </div>

</div>


<?php echo (new View('admin/footer'));?>
