<?php
use Framework\View;
echo(new View('admin/header'));
?>



<script type="text/javascript" src="/dist/js/jquery.min.js"></script>
<script type="text/javascript" src="/dist/uploader/fine-uploader.js"></script>
<link type="text/css" href="/dist/uploader/fine-uploader.css">

<script type="text/javascript">

    $(document).ready(function(){
        $(document).on('click', '.reply_list a', function(){
            var test =  $(this).text();
            $('.reply_list a').removeClass('active');
            $('.reply_content').text(test);
            $(this).addClass('active');
        });
    });
</script>
<div class="col-lg-6">
    <div class="panel panel-info">
        <div class="panel-heading panel-info">
            反馈
        </div>
        <div class="panel-body">
            <div class="bs-example" data-example-id="body-copy">
                <blockquote class="bg-warning">
                    <p><?php echo $feedback['content']??''?></p>
                    <footer><?php echo !empty($feedback['create_time']) ? date('Y-m-d H:i', $feedback['create_time']) : ''; ?> <cite title="Source Title">反馈</cite></footer>
                </blockquote>
                <?php if (!empty($feedback['reply'])):?>
                    <?php foreach ($feedback['reply'] as $r):?>
                        <blockquote class="blockquote-reverse bg-info">
                            <p><?php echo $r['content']??''?></p>
                            <footer><?php echo !empty($r['create_time']) ? date('Y-m-d H:i', $r['create_time']) : ''; ?> <cite title="Source Title">回复</cite></footer>
                        </blockquote>
                    <?php endforeach;?>
                <?php endif;?>

            </div>
        </div>

        <div class="panel-heading panel-danger">
            回复
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post">
                <input type="hidden" name="feedback_id" value="<?php echo $feedback['_id'] ?? ''?>">
                <div class="form-group">
                    <div class="col-lg-6">
                        <textarea  class="form-control reply_content" name="content" rows="8" ></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-12">
                        <div class="left">
                            <button type="submit" class="btn btn-primary">回复</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="col-lg-6">
    <div class="panel panel-info">
        <div class="panel-heading panel-info">
            最近回复
        </div>
        <div class="panel-body">
            <div class="list-group reply_list">
                <?php if (!empty($replys)): ?>
                    <?php foreach ($replys as $reply): ?>
                        <a href="#" class="list-group-item"><?php echo $reply['content'] ?></a>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php echo (new View('admin/footer'));?>
