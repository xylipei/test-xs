<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>(..•˘_˘•..)</title>
    <link href="/dist/css/bootstrap.css" rel="stylesheet">
    <script type="text/javascript" src="/dist/js/jquery.min.js"></script>
    <script type="text/javascript" src="/dist/js/bootstrap.js"></script>
    <script type="text/javascript" src="/dist/js/bootstrap-select.js"></script>
    <script type="text/javascript" src="/dist/uploader/fine-uploader.js"></script>
    <link type="text/css" href="/dist/uploader/fine-uploader.css">
    <script type="text/javascript">
        $(document).ready(function(){
            $('select.select').selectpicker({});
        });
    </script>
</head>
<body>

<nav class="navbar navbar-default col-lg-12">
    <ul class="nav navbar-nav">
        <li class="<?php if ($nav == ''):?>active<?php endif;?>"><a href="/">管理首页</a></li>
        <li class="<?php if ($nav == 'user'):?>active<?php endif;?>"><a href="/user">用户管理</a></li>
        <li class="<?php if ($nav == 'book'):?>active<?php endif;?>"><a href="/book">小说管理</a></li>
        <li class="<?php if ($nav == 'app'):?>active<?php endif;?>"><a href="/app">应用管理</a></li>
        <li class="<?php if ($nav == 'kw'):?>active<?php endif;?>"><a href="/kw">关键词管理</a></li>
        <li class="<?php if ($nav == 'feedback'):?>active<?php endif;?>"><a href="/feedback">反馈管理</a></li>
        <li class="<?php if ($nav == 'pre'):?>active<?php endif;?>"><a href="/pre">付费方案管理</a></li>
        <li class="<?php if ($nav == 'order'):?>active<?php endif;?>"><a href="/order">订单管理</a></li>
        <?php if (($_user['role'] ?? null) == 'administrator'):?>
        <li class="<?php if ($nav == 'state'):?>active<?php endif;?>"><a href="/state">支付统计</a></li>
        <?php endif;?>
        <li class="<?php if ($nav == 'src'):?>active<?php endif;?>"><a href="/src">渠道管理</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <?php if (($_user['role'] ?? null) == 'administrator'):?>
            <li class="<?php if ($nav == 'account'):?>active<?php endif;?>"><a href="/account">账号管理</a></li>
        <?php endif;?>
        <li class="<?php if ($nav == 'me'):?>active<?php endif;?>"><a href="/me"><span class="glyphicon glyphicon-user"></span> 欢迎你，<?php echo ($_user['nickname'] ?? '匿名用户');?></a></li>
        <li><a href="/me/logout"><span class="glyphicon glyphicon-off"></span></a></li>
    </ul>
</nav>
