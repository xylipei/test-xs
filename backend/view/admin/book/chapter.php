<?php
use Framework\View;
echo (new View('admin/header'));
?>
<?php echo (new View('admin/book/menu'))->set('on', 'book');?>
<?php echo (new View('admin/book/breadcrumb'))->set('on', 'chapter');?>
<style>
    #show-search {position:fixed; top:170px; right:200px;width: 500px;height:600px;overflow-x: hidden;overflow-y: scroll;}
    #show-content {position:fixed; top:255px; right:200px;width: 500px;height:600px;overflow-x: hidden;overflow-y: scroll;}
    #scroll {position:fixed; top:350px; right:100px;font-size: 13px}
    .scrollItem {width:50px; height:50px; cursor: pointer; text-align: center;}
    span{
        vertical-align: text-top;
    }
</style>
<script type="text/template" id="qq-template">
    <div class="qq-uploader-selector qq-uploader clearfix" qq-drop-area-text="Drop files here">
        <div class="pull-left qq-upload-list-selector qq-upload-list" aria-live="polite"
             aria-relevant="additions removals" style="margin-left: 15px;">
            <span class="qq-upload-size-selector qq-upload-size"></span>
            <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
            <div class="progress qq-progress-bar-container-selector" style="padding-left: 0; padding-right: 0;">
                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                     class="progress-bar progress-bar-info qq-progress-bar-selector qq-progress-bar"></div>
            </div>
        </div>
    </div>
</script>

<script type="text/javascript">
    $(document).ready(function () {
        var ops = {
            autoUpload: true,
            debug: true,
            multiple: true,
            maxConnections: 5,
            request: {
                endpoint: '/book/chapter/add',
                method: 'POST',
                inputName: 'file',
                params: {
                    'book_id':'<?php echo $book['_id']??'';?>',
                    'free_chapter_num':'<?php echo $book['free_chapter_num']??'';?>'
                }
            },
            retry: {
                enableAuto: false,
                maxAutoAttempts: 3,
                autoAttemptDelay: 5,
            },
            validation: {
                allowedExtensions: ['txt'],
                stopOnFirstInvalidFile: false,
            },
            cors: {
                expected: false,
            }
        };

        new qq.FineUploader($.extend(ops, {
            element: document.getElementById('content'),
            button: document.getElementById('content-btn'),
            callbacks: {
                onComplete: function (id, name, responseJSON, maybeXhr) {
                    if (responseJSON.error === 0) {
                        var html = '<tr><td  class="bg-warning">' + responseJSON.chapter._id + '</td>';
                        html += '<td  class="bg-warning"><a style="cursor:pointer;" class="show-content">' + responseJSON.chapter.content_id + '</a></td>';
                        html += '<td>' + responseJSON.chapter.title + '</td>';
                        if (responseJSON.chapter.status === 'accepted'){
                           html += '<td><span class="label label-success">已发布</span></td>';
                        } else if(responseJSON.chapter.status === 'trashed'){
                           html += '<td><span class="label label-warning">隐藏</span></td>';
                        } else {
                           html += '<td><span class="label label-default">草稿</span></td>';
                        }
                        html += '<td><a class="btn btn-xs btn-primary" href="/book/chapter/edit?chapter_id='+responseJSON.chapter._id+'"><span class="glyphicon glyphicon-edit">修改</span></td>';
                        $('#content-box').append(html);
                    }
                }
            }
        }));


        $('.show-content').click(function () {
            var content_id = $(this).text();
            $.ajax({
                url : "/book/chapter/content",
                async : true,
                type : "POST",
                data : {
                    "content_id" : content_id
                },
                // 成功后开启模态框
                success : showContent,
                error : function() {
                    alert("请求失败");
                },
                dataType : "json"
            });
        });

        function showContent(data) {
            // $('#myModalLabel').text(data.content.title);
            // $('#myModal').modal('show');
            $('#show-content p').text(data.content.content);
        }

        var $root = $('html, body');
        $('#scroll a').click(function() {
            var href = $.attr(this, 'href');
            console.log(href);
            $root.animate({
                scrollTop: $(href).offset().top
            }, 500, function () {
                window.location.hash = href;
            });
            return false;
        });

        $('#search').click(function() {
            var val = $('input[name="search"]').val();
            var id = '#' + (val - 1);
            $root.animate({
                scrollTop: $(id).offset().top
            }, 500, function () {
                window.location.hash = id ;
            });
            return false;
        });

    });
</script>
<div id="top"></div>
<div class="col-lg-6">
    <div class="col-lg-3">
        <img src="<?php echo $book['thumb']??'';?>" alt="Responsive image" class="img-thumbnail">
    </div>
    <div class="col-lg-9">
        <table class="table table-bordered table-striped">
            <colgroup>
                <col class="col-xs-2">
                <col class="col-xs-7">
            </colgroup>
            <thead>
            <tr>
                <th colspan="2" class="clearfix">
                    <span style="margin: 0 2px;" class="pull-left">小说详情</span>
                    <a style="margin: 0 2px;" class="btn btn-xs btn-primary pull-right" href="/book/edit?book_id=<?php echo $book['_id'];?>"><span class="glyphicon glyphicon-edit"></span> 修改</a>
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">
                    标题
                </th>
                <td><?php echo $book['title']??'';?></td>
            </tr>
            <tr>
                <th scope="row">
                    状态
                </th>
                <td>
                    <?php
                    switch ($book['status'] ?? '') {
                        case 'accepted':
                            echo '<span class="label label-success">已发布</span>';
                            break;
                        case 'draft':
                            echo '<span class="label label-default">草稿</span>';
                            break;
                        case 'trashed':
                            echo '<span class="label label-warning">隐藏</span>';
                            break;
                    }
                    ?>

                    <a style="margin: 0 2px;line-height: 18px;" class="btn btn-xs btn-primary pull-right" href="/book/status?book_id=<?php echo $book['_id'];?>&status=draft"><span class="glyphicon glyphicon-floppy-disk"></span> 保存草稿</a>
                    <?php if (($book['status'] ?? '') == 'accepted'):?>
                        <a style="margin: 0 2px;" class="btn btn-xs btn-warning pull-right" href="/book/status?book_id=<?php echo $book['_id'];?>&status=trashed"><span class="glyphicon glyphicon glyphicon-eye-close"></span> 隐藏</a>
                    <?php endif;?>
                    <?php if (in_array($book['status'] ?? '', ['trashed', 'draft'])):?>
                        <a style="margin: 0 2px;" class="btn btn-xs btn-success pull-right" href="/book/status?book_id=<?php echo $book['_id'];?>&status=accepted"><span class="glyphicon glyphicon-send"></span> 发布</a>
                    <?php endif;?>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    作者
                </th>
                <td><?php echo $book['author']??'';?></td>
            </tr>
            <tr>
                <th scope="row">
                    简介
                </th>
                <td><?php echo $book['memo']??'';?></td>
            </tr>
            <tr>
                <th scope="row">
                    分类
                </th>
                <td>
                    <span class="label label-success"><?php echo $book['category']??'';?></span>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    是否完结
                </th>
                <td>
                    <?php if ($book['finish']??1):;?><span class="label label-primary">完结</span><?php else:?><span class="label label-info">连载</span><?php endif;?>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    是否VIP
                </th>
                <td>
                    <?php if ($book['vip']??0):;?><span class="label label-success">是</span><?php else:?><span class="label label-warning">否</span><?php endif;?>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    是否免费
                </th>
                <td>
                    <?php if ($book['free']??1):;?><span class="label label-success">是</span><?php else:?><span class="label label-warning">否</span><?php endif;?>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    章节数(已发布)
                </th>
                <td><?php echo $book['num']['chapter']??0;?></td>
            </tr>
            <tr>
                <th scope="row">
                    免费章节数
                </th>
                <td><?php echo $book['free_chapter_num']??0;?></td>
            </tr>
            <tr>
                <th scope="row">
                    每章所需金币
                </th>
                <td><?php echo $book['chapter_balance_price']??0;?></td>
            </tr>
            <tr>
                <th scope="row">
                    创建时间
                </th>
                <td><?php echo date('Y-m-d H:i', $book['create_time']);?></td>
            </tr>
            <tr>
                <th scope="row">
                    修改时间
                </th>
                <td><?php echo date('Y-m-d H:i', $book['update_time']);?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading clearfix">
                <span class="pull-left">章节列表</span>
                <button class="btn btn-primary btn-xs pull-right" id="content-btn">
                    上传章节 <span class="glyphicon glyphicon-open"></span>
                </button>
                <a onclick="return confirm('确定全部公开吗?')" class="pull-right" style="margin-right: 5px;" href="/book/chapter/open?book_id=<?php echo $book['_id'];?>">
                    <button class="btn btn-danger btn-xs" type="button">
                        全部公开 <span class="glyphicon glyphicon-refresh"></span>
                    </button>
                </a>
                <div class="hide" id="content"></div>
            </div>
            <table class="table table-bordered">
                <thead>
                <tr class="bg-warning">
                    <td>章节ID</td>
                    <td>内容ID<code>(点击查看内容详情)</code></td>
                    <td>名称</td>
                    <td class="col-lg-1">免费</td>
                    <td>状态</td>
                    <td>操作</td>
                </tr>
                </thead>
                <tbody id="content-box">
                <?php if (!empty($chapters)) foreach ($chapters as $k => $chapter):?>
                    <tr id="<?php echo $chapter['sort']??''?>">
                        <td  class="bg-warning"><?php echo $chapter['_id']??'';?></td>
                        <td  class="bg-warning"><a style="cursor:pointer;" class="show-content"><?php echo $chapter['content_id']??'';?></a></td>
                        <td><?php echo $chapter['title']??'';?></td>
                        <td>
                            <?php  if ($chapter['free']??1):;?><span class="label label-success">是</span><?php else:?><span class="label label-warning">否</span><?php endif;?>
                        </td>
                        <td>
                            <?php
                            switch ($chapter['status']??'') {
                                case 'accepted':
                                    echo '<span class="label label-success">已发布</span>';
                                    break;
                                case 'draft':
                                    echo '<span class="label label-default">草稿</span>';
                                    break;
                                case 'trashed':
                                    echo '<span class="label label-warning">隐藏</span>';
                                    break;
                                case 'deleted':
                                    echo '<span class="label label-danger">已删除</span>';
                                    break;
                            }
                            ?>
                        </td>
                        <td>
                            <a class="btn btn-xs btn-primary" href="/book/chapter/edit?chapter_id=<?php echo $chapter['_id'];?>"><span class="glyphicon glyphicon-edit"></span> 修改</a>
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
            <div id="bottom"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
                <p id="content-content"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

<div id="show-search" class="panel-body">
    <div class="form-group">
        <div class="col-lg-12">
            <div class="pull-left" style="margin-right: 10px;">
                <p><label class="label label-default">章节搜索</label></p>
                <input name="search" class="form-control" placeholder="章节搜索（例：1000）"  value="">
            </div>
            <div class="pull-left" style="margin-right: 10px;">
                <p>&nbsp;</p>
                <input id="search" type="button" class="form-control btn-primary" value="筛选">
            </div>
        </div>
    </div>
</div>
<div id="show-content" class="well">
    <p></p>
</div>

<div id="scroll">
    <div id="toTop" class="scrollItem">
        <a href="#top" class="btn-fixed">
            <button type="button" class="btn btn-primary btn-lg">
                <span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
            </button>
        </a>
    </div>

    <div id="toBottom" class="scrollItem">
        <a href="#bottom" class="btn-fixed">
            <button type="button" class="btn btn-primary btn-lg">
                <span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span>
            </button>
        </a>
    </div>
</div>

<?php echo (new View('admin/footer'));?>
