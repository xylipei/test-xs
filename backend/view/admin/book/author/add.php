<?php
use Framework\View;
echo (new View('admin/header'));
?>

<?php echo (new View('admin/book/menu'))->set('on', 'author');?>
<?php echo (new View('admin/book/author/breadcrumb'))->set('on', 'Content');?>


<div class="col-lg-6">
    <div class="panel panel-info">
        <div class="panel-heading panel-info clearfix">
            <span class="pull-left">添加作者</span>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post">
                <div class="form-group">
                    <label for="name" class="col-lg-2 control-label">作者名称</label>
                    <div class="col-lg-6">
                        <input id="name" autocomplete="off" class="form-control" name="name" type="text" placeholder="作者名称">
                    </div>
                </div>
                <div class="form-group">
                    <label for="tags" class="col-lg-2 control-label">是否热门</label>
                    <div class="col-lg-4">
                        <select class="selectpicker" name="is_hot">
                            <option>请选择</option>
                            <option value="0">否</option>
                            <option value="1">是</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-12 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary">提交保存</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php echo (new View('admin/footer'));?>
