<?php
use Framework\View;
echo (new View('admin/header'));
?>

<style>
    span{
        vertical-align: text-top;
    }
</style>
<?php echo (new View('admin/book/menu'))->set('on', 'ranking');?>

<div class="col-lg-12">
    <div class="panel panel-info">
        <div class="panel-heading clearfix">
            <span class="pull-left">风云榜</span>
        </div>
        <div class="panel-body">
            <form method="get" class="form-horizontal" autocomplete="off">
                <div class="form-group">
                    <div class="col-lg-12">
                        <div class="pull-left" style="margin-right: 10px;">
                            <input name="title" class="form-control" placeholder="小说名称" <?php if (!empty($title)):?> value="<?php echo $title;?>"<?php endif;?>>
                        </div>
                        <div class="pull-left" style="margin-right: 10px;">
                            <button type="submit" class="btn btn-primary">过滤</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr class="bg-warning">
                <td class="col-lg-1">ID</td>
                <td>排序</td>
                <td>标题</td>
                <td>作者</td>
                <td>类型</td>
                <td>封面</td>
                <td>连载状态</td>
                <td>章节数</td>
                <td>添加时间</td>
                <td>更新时间</td>
                <td>状态</td>
                <td>操作</td>
            </tr>
            </thead>
            <tbody>
            <?php if (!empty($books)) foreach ($books as $book):?>
                <tr>
                    <td class="bg-warning">
                        <a href="/book/chapter?book_id=<?php echo $book['_id'];?>"><?php echo $book['_id'];?></a>
                    </td>
                    <td>
                        <span class="label label-danger"><?php echo $book['ranking']['sort']?></span>
                    </td>
                    <td>
                       <span  class="" data-toggle="popover" data-content="<?php echo $book['title']??''?>">
                            <?php if (mb_strlen($book['title']??'','utf-8') > 5):?>
                                <?php echo mb_substr($book['title']??'', 0, 5,'utf-8')?>……
                            <?php else:?>
                                <?php echo $book['title']??'' ?>
                            <?php endif;?>
                        </span>
                    </td>
                    <td><?php echo $book['author'] ?? '';?></td>
                    <td><?php echo $book['category']??'';?></td>
                    <td>
                        <?php if (!empty($book['thumb'])):?>
                            <img src="<?php echo $book['thumb'];?>" width="100px" height="100px"/>
                        <?php endif;?>
                    </td>
                    <td><?php if ($book['finish']):;?><span class="label label-primary">完结</span><?php else:?><span class="label label-info">连载</span><?php endif;?></td>
                    <td><?php echo $book['num']['chapter'] ?? 0;?></td>
                    <td><?php echo date('Y-m-d H:i', $book['create_time']);?></td>
                    <td><?php echo date('Y-m-d H:i', $book['update_time']);?></td>
                    <td>
                        <?php
                        switch ($book['status'] ?? '') {
                            case 'accepted':
                                echo '<span class="label label-success">已发布</span>';
                                break;
                            case 'draft':
                                echo '<span class="label label-default">草稿</span>';
                                break;
                            case 'trashed':
                                echo '<span class="label label-warning">隐藏</span>';
                                break;
                            case 'deleted':
                                echo '<span class="label label-danger">已删除</span>';
                                break;
                        }
                        ?>
                    </td>
                    <td>
                        <a class="btn btn-xs btn-primary" href="/book/ranking/edit?book_id=<?php echo $book['_id'];?>"><span class="glyphicon glyphicon-edit"></span>修改</a>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>

        <div class="panel-footer">
            <?php if (isset($pagination)) echo $pagination;?>
        </div>

    </div>
</div>

<?php echo (new View('admin/footer'));?>
