<div class="col-lg-12" style="margin-bottom: 30px;">
    <ul class="nav nav-tabs">
        <li<?php if ($on == 'book'):?> class="active"<?php endif;?>><a href="/book">小说管理</a></li>
        <li<?php if ($on == 'author'):?> class="active"<?php endif;?>><a href="/book/author">作者管理</a></li>
        <li<?php if ($on == 'category'):?> class="active"<?php endif;?>><a href="/book/category">分类管理</a></li>
        <li<?php if ($on == 'ranking'):?> class="active"<?php endif;?>><a href="/book/ranking">风云榜管理</a></li>
    </ul>
</div>
