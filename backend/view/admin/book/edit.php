<?php
use Framework\View;
echo (new View('admin/header'));
?>

<?php echo (new View('admin/book/menu'))->set('on', 'book');?>
<?php echo (new View('admin/book/breadcrumb'))->set('on', 'edit');?>

<script type="text/template" id="qq-template">
    <div class="qq-uploader-selector qq-uploader clearfix" qq-drop-area-text="Drop files here">
        <div class="pull-left qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals" style="margin-left: 15px;">
            <span class="qq-upload-size-selector qq-upload-size"></span>
            <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
            <div class="progress qq-progress-bar-container-selector" style="padding-left: 0; padding-right: 0;">
                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-info qq-progress-bar-selector qq-progress-bar"></div>
            </div>
        </div>
    </div>
</script>

<script type="text/javascript">
    $(document).ready(function(){
        var ops = {
            autoUpload: true,
            debug: true,
            multiple: false,
            maxConnections: 5,
            request: {
                endpoint: '/upload',
                method: 'POST',
                inputName: 'file'
            },
            retry: {
                enableAuto: false,
                maxAutoAttempts: 3,
                autoAttemptDelay: 5,
            },
            validation: {
                allowedExtensions: ['png', 'jpeg', 'jpg', 'mp4'],
                stopOnFirstInvalidFile: false,
            },
            cors: {
                expected: false,
            }
        };

        new qq.FineUploader($.extend(ops, {
            element: document.getElementById('thumb'),
            button: document.getElementById('thumb-btn'),
            callbacks: {
                onComplete: function(id, name, responseJSON, maybeXhr) {
                    if (responseJSON.error === 0) {
                        $('#thumb').val(responseJSON.url);
                        $('#thumb-show').html('<img style="margin: 10px 0;" width="200" src="'+responseJSON.url+'"/>');
                    }
                    return true;
                }
            }
        }));
    });
</script>


<div class="col-lg-6">
    <div class="panel panel-info">
        <div class="panel-heading panel-info clearfix">
            <span class="pull-left">修改小说</span>
            <a class="pull-right" href="javascript:history.back(-1)"><span class="glyphicon glyphicon-log-in"></span></a>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post" autocomplete="off">
                <input type="hidden" value="<?php echo $book['_id']??'';?>" name="book_id">
                <div class="form-group">
                    <label for="title" class="col-lg-2 control-label">标题名字</label>
                    <div class="col-lg-6">
                        <input id="title" value="<?php echo $book['title']??'';?>" autocomplete="off" class="form-control" name="title" type="text" placeholder="标题">
                    </div>
                </div>
                <div class="form-group">
                    <label  class="col-lg-2 control-label">小说简介</label>
                    <div class="col-lg-6">
                        <textarea  class="form-control" name="memo" rows="8" placeholder="请填写小说简介"><?php echo $book['memo']??'';?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tags" class="col-lg-2 control-label">小说作者</label>
                    <div class="col-lg-5 clearfix" style="line-height: 34px">
                        <input value="<?php echo $book['author']??'';?>" type="text" autocomplete="off" class="form-control" name="author" placeholder="小说作者">
                    </div>
                </div>
                <div class="form-group">
                    <label for="tags" class="col-lg-2 control-label">小说分类</label>
                    <div class="col-lg-5 clearfix" style="line-height: 34px">
                        <select class="selectpicker pull-left" name="category">
                            <option>请选择分类</option>
                            <?php if (!empty($categorys)) foreach ($categorys as $category):?>
                                <option <?php if($category['title'] == ($book['category']?? '')):?> selected<?php endif;?> value="<?php echo $category['title']??'';?>"><?php echo $category['title']??'';?></option>
                            <?php endforeach;?>
                        </select>
                        <div class="pull-right">
                            <a class="btn btn-xs btn-primary " href="/book/category/add"><span class="glyphicon glyphicon-plus-sign"></span>添加分类</a>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tags" class="col-lg-2 control-label">完结连载</label>
                    <div class="col-lg-4">
                        <select class="selectpicker" name="finish">
                            <option>请选择连载完结</option>
                            <option <?php if(isset($book['finish'])) if($book['finish'] ==  '0'):?> selected<?php endif;?>  value="0">连载</option>
                            <option<?php if(isset($book['finish'])) if($book['finish'] ==  '1'):?> selected<?php endif;?>  value="1">完结</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tags" class="col-lg-2 control-label">VIP</label>
                    <div class="col-lg-4">
                        <select class="selectpicker" name="vip">
                            <option>请选择</option>
                            <option <?php if(isset($book['vip'])) if($book['vip'] ==  '0'):?> selected<?php endif;?> value="0">否</option>
                            <option <?php if(isset($book['vip'])) if($book['vip'] ==  '1'):?> selected<?php endif;?> value="1">是</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tags" class="col-lg-2 control-label">是否免费</label>
                    <div class="col-lg-4">
                        <select class="selectpicker" name="free">
                            <option>请选择</option>
                            <option <?php if(isset($book['free'])) if($book['free'] ==  '0'):?> selected<?php endif;?> value="0">否</option>
                            <option <?php if(isset($book['free'])) if($book['free']??0 ==  '1'):?> selected<?php endif;?> value="1">是</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-lg-2 control-label">免费章节数</label>
                    <div class="col-lg-2">
                        <input id="free_chapters_num" autocomplete="off" class="form-control" name="free_chapter_num" type="text" placeholder="免费章节数" value="<?php echo $book['free_chapter_num']??''; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">每章所需金币数</label>
                    <div class="col-lg-2">
                        <input autocomplete="off" class="form-control" name="chapter_balance_price" type="text" placeholder="金币数" value="<?php echo $book['chapter_balance_price']??'';?>">
                    </div>
                </div>
                <input id="thumb" name="thumb" type="hidden" value="<?php echo $book['thumb']??'';?>">
                <div class="form-group">
                    <label for="title" class="col-lg-2 control-label">选择封面</label>
                    <div class="col-lg-10 clearfix">
                        <div class="clearfix">
                            <div class="btn btn-default pull-left" id="thumb-btn">选择封面</div>
                            <div id="thumb"></div>
                        </div>
                        <div id="thumb-show">
                            <img src="<?php echo $book['thumb']??'';?>" style="margin: 10px 0;" width="200">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-12 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary">提交保存草稿</button>
                    </div>
                </div>
            </form>
        </div>
        <?php if (($book['status'] ?? '') == 'draft'):?>
        <div class="panel-heading panel-danger">
            删除小说
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post" action="/book/delete">
                <input name="book_id" type="hidden" value="<?php echo $book['_id']??'';?>">
                <div class="form-group">
                    <div class="col-lg-12">
                        <button type="submit" id="delete" class="btn btn-danger" onclick="return confirm('删除后将无法恢复,确定要删除?')">删除小说</button>
                    </div>
                </div>
            </form>
        </div>
        <?php endif;?>
    </div>
</div>

<?php echo (new View('admin/footer'));?>
