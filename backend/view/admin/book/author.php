<?php
use Framework\View;
echo(new View('admin/header'));
?>

<?php echo (new View('admin/book/menu'))->set('on', 'author');?>
<style>
    span{
        vertical-align: text-top;
    }
</style>

    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading clearfix">
                <span class="pull-left">作者管理</span>
                <a class="pull-right" href="/book/author/add"><span class="glyphicon glyphicon-plus-sign"></span></a>
            </div>
            <div class="panel-body">
                <form method="get" class="form-horizontal" autocomplete="off">
                    <div class="form-group">
                        <div class="col-lg-12">
                            <div class="pull-left" style="margin-right: 10px;">
                                <input name="author" class="form-control" placeholder="作者名字" <?php if (!empty($name)):?> value="<?php echo $name;?>"<?php endif;?>>
                            </div>
                            <div class="pull-left" style="margin-right: 10px;">
                                <button type="submit" class="btn btn-primary">过滤</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <table class="table table-bordered">
                <thead>
                <tr class="bg-warning">
                    <td class="col-lg-1">ID</td>
                    <td>作者名字</td>
                    <td>小说数量</td>
                    <td>是否热门</td>
                    <td>操作</td>
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($authors)) foreach ($authors as $author):?>
                    <tr>
                        <td class="bg-warning">
                            <?php echo $author['_id'];?>
                        </td>
                        <td><?php echo $author['name'] ?? '';?></td>
                        <td><?php echo $author['num']['book'] ?? 0;?></td>
                        <td>
                            <?php
                                $author['is_hot'] = isset($author['is_hot']) ? $author['is_hot'] : 0;
                                if ($author['is_hot']){
                                    echo '<span class="label label-danger">热</span>';
                                }else{
                                    echo '<span class="label label-default">否</span>';
                                }
                            ?>
                        </td>
                        <td>
                            <a class="btn btn-xs btn-primary" href="/book/author/edit?author_id=<?php echo $author['_id'];?>"><span class="glyphicon glyphicon-edit"></span> 修改</a>
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
            <div class="panel-footer">
                <?php if (isset($pagination)) echo $pagination;?>
            </div>
        </div>
    </div>

<?php echo (new View('admin/footer'));?>
