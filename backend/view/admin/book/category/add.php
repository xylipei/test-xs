<?php
use Framework\View;
echo (new View('admin/header'));
?>

<?php echo (new View('admin/book/menu'))->set('on', 'category');?>
<?php echo (new View('admin/book/category/breadcrumb'))->set('on', 'Content');?>

<div class="col-lg-6">
    <div class="panel panel-info">
        <div class="panel-heading panel-info clearfix">
            <span class="pull-left">添加分类</span>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post">
                <div class="form-group">
                    <label for="title" class="col-lg-2 control-label">分类名称</label>
                    <div class="col-lg-6">
                        <input id="title" autocomplete="off" class="form-control" name="title" type="text" placeholder="标题名称">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-12 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary">提交保存</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php echo (new View('admin/footer'));?>
