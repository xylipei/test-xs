<?php
use Framework\View;
echo (new View('admin/header'));
?>

<?php echo (new View('admin/book/menu'))->set('on', 'category');?>
<?php echo (new View('admin/book/category/breadcrumb'))->set('on', 'edit');?>

<div class="col-lg-6">
    <div class="panel panel-info">
        <div class="panel-heading panel-info clearfix">
            <span class="pull-left">修改分类</span>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post">
                <input type="hidden" name="category_id" value="<?php echo $category['_id']??'';?>">
                <div class="form-group">
                    <label for="title" class="col-lg-2 control-label">分类名称</label>
                    <div class="col-lg-6">
                        <input id="title" value="<?php echo $category['title']??'';?>" autocomplete="off" class="form-control" name="title" type="text" placeholder="标题名称">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-12 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary">提交保存草稿</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="panel-heading panel-danger">
            修改分类状态
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post" action="/book/category/status">
                <input type="hidden" name="category_id" value="<?php echo $category['_id'];?>">

                <?php if (($category['status'] ?? '') == 'accepted'):?>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <p class="alert alert-danger" style="margin-bottom: 0;">隐藏分类，会同时隐藏此分类下的所有内容</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" name="status" value="trashed" class="btn btn-sm btn-danger" onclick="return confirm('确定隐藏?')">隐藏此分类</button>
                        </div>
                    </div>
                <?php endif;?>

                <?php if (in_array($category['status'] ?? '', ['trashed', 'draft'])):?>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <p class="alert alert-warning" style="margin-bottom: 0;">如果有修改，先保存后发布</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" name="status" value="accepted" class="btn btn-sm btn-success" onclick="return confirm('确定发布?')">发布此分类</button>
                        </div>
                    </div>
                <?php endif;?>
            </form>
        </div>
    </div>
</div>

<?php echo (new View('admin/footer'));?>
