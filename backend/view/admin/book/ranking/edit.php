<?php
use Framework\View;
echo (new View('admin/header'));
?>

<?php echo (new View('admin/book/menu'))->set('on', 'ranking');?>
<?php echo (new View('admin/book/ranking/breadcrumb'))->set('on', 'edit');?>

<div class="col-lg-6">
    <div class="panel panel-info">
        <div class="panel-heading panel-info clearfix">
            <span class="pull-left">添加风云榜</span>
            <a class="pull-right" href="javascript:history.back(-1)"><span class="glyphicon glyphicon-log-in"></span></a>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post" autocomplete="off">
                <input type="hidden" value="<?php echo $book['_id']??'';?>" name="book_id">
                <div class="form-group">
                    <label for="title" class="col-lg-2 control-label">排名</label>
                    <div class="col-lg-6">
                        <input id="title" value="<?php echo $book['ranking']['sort']??''?>" autocomplete="off" class="form-control" name="sort" type="text" placeholder="排名">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-12 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary">提交</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="panel-heading panel-danger">
            删除
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post" action="/book/ranking/delete">
                <input name="book_id" type="hidden" value="<?php echo $book['_id']??'';?>">
                <div class="form-group">
                    <div class="col-lg-12">
                        <button type="submit" id="delete" class="btn btn-danger" onclick="return confirm('删除后将无法恢复,确定要删除?')">删除小说</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php echo (new View('admin/footer'));?>
