<?php
use Framework\View;
echo (new View('admin/header'));
?>

<?php echo (new View('admin/book/menu'))->set('on', 'book');?>
<?php echo (new View('admin/book/chapter/breadcrumb'))->set(['on' => 'edit', 'book_id' => $chapter['book_id']]);?>


<div class="col-lg-6">
    <div class="panel panel-info">
        <div class="panel-heading panel-info clearfix">
            <span class="pull-left">章节添加</span>
            <a class="pull-right" href="javascript:history.back(-1)"><span class="glyphicon glyphicon-log-in"></span></a>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post">
                <input type="hidden" name="chapter_id" value="<?php echo $chapter['_id']??'';?>">
                <input type="hidden" name="content_id" value="<?php echo $content['_id']??'';?>">
                <div class="form-group">
                    <label for="title" class="col-lg-2 control-label">章节名字</label>
                    <div class="col-lg-6">
                        <input id="title" value="<?php echo $chapter['title']??'';?>" autocomplete="off" class="form-control" name="title" type="text" placeholder="标题">
                    </div>
                </div>
                <div class="form-group">
                    <label for="title" class="col-lg-2 control-label">章节内容</label>
                    <div class="col-lg-10">
                        <textarea  class="form-control reply_content" name="content" rows="30"><?php echo $content['content']??'';?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-12 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary">提交保存草稿</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="panel-heading panel-danger">
            修改章节状态
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post" action="/book/chapter/status">
                <input type="hidden" name="chapter_id" value="<?php echo $chapter['_id'];?>">
                <?php if (($chapter['status'] ?? '') == 'accepted'):?>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <p class="alert alert-danger" style="margin-bottom: 0;">隐藏章节</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" name="status" value="trashed" class="btn btn-sm btn-danger" onclick="return confirm('确定隐藏?')">隐藏此章节</button>
                        </div>
                    </div>
                <?php endif;?>

                <?php if (in_array($chapter['status'] ?? '', ['trashed', 'draft'])):?>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <p class="alert alert-warning" style="margin-bottom: 0;">如果有修改，先保存后发布</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" name="status" value="accepted" class="btn btn-sm btn-success" onclick="return confirm('确定发布?')">发布此章节</button>
                        </div>
                    </div>
                <?php endif;?>
            </form>
        </div>
        <?php if (($chapter['status'] ?? '') == 'draft'):?>
        <div class="panel-heading panel-danger">
            删除章节
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post" action="/book/chapter/delete">
                <input name="chapter_id" type="hidden" value="<?php echo $chapter['_id']??'';?>">
                <div class="form-group">
                    <div class="col-lg-12">
                        <button type="submit" id="delete" class="btn btn-danger" onclick="return confirm('删除后将无法恢复,确定要删除?')">删除章节</button>
                    </div>
                </div>
            </form>
        </div>
        <?php endif;?>
    </div>
</div>

<?php echo (new View('admin/footer'));?>
