<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>管理后台 - w(´･ω･`)w </title>
    <link href="/dist/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
        #login {
            background: #fff;
            position:fixed;
            top:50%;
            left: 50%;
            margin-top:-300px;
            margin-left: -190px;
            width: 330px;
            border:1px solid #ddd;
            padding: 20px 50px;

        }

        #login .logo {
            display: block;
            height: 240px;
            background: url("/dist/images/logo.png") center center no-repeat;
        }

    </style>
</head>
<body style="background:#f1f1f1; ">

<div id="login">
    <div class="logo"></div>
    <form class="form-horizontal" method="post">
        <?php if(!empty($error)):?>
            <div class="form-group">
                <div class="col-md-12">
                    <div class="alert alert-danger"><?php echo $error;?></div>
                </div>
            </div>
        <?php endif;?>
        <div class="form-group">
            <div class="col-md-12">
                <input type="text" autocomplete="off" name="nickname" class="form-control" autofocus="autofocus" placeholder="用户名">
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-12">
                <input type="password" autocomplete="off" name="password" class="form-control" placeholder="密码">
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-12">
                <button type="submit" class="col-md-12 btn btn-danger">登录</button>
            </div>
        </div>
    </form>
</div>

</body>
</html>
