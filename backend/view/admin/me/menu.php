<div class="col-lg-12" style="margin-bottom: 30px;">
    <h1 class="" style="margin-bottom: 35px;">你好，<?php echo $user['nickname']??'';?></h1>
    <ul class="nav nav-tabs">
        <li<?php if ($on == 'me'):?> class="active"<?php endif;?>><a href="/me">个人中心</a></li>
        <li<?php if ($on == 'action'):?> class="active"<?php endif;?>><a href="/me/history/action">操作记录</a></li>
        <li<?php if ($on == 'login'):?> class="active"<?php endif;?>><a href="/me/history/login">登录历史</a></li>
        <li<?php if ($on == 'password'):?> class="active"<?php endif;?>><a href="/me/password">修改密码</a></li>
        <li<?php if ($on == 'uid'):?> class="active"<?php endif;?>><a href="/me/uid">绑定前端用户</a></li>
    </ul>
</div>
