<?php
use Framework\View;
echo (new View('admin/header'));?>

<?php echo (new View('admin/me/menu'))->set('on', 'uid');?>

<div class="col-lg-6">
    <div class="panel panel-info">
        <div class="panel-heading">
            绑定前端用户ID
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post">

                <?php if (!empty($user)):?>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <input class="form-control" type="text" disabled value="<?php echo $user['nickname'] ?? '';?>">
                        </div>
                    </div>
                <?php endif;?>

                <div class="form-group">
                    <div class="col-lg-6">
                        <input class="form-control" name="uid" type="text" placeholder="UID" value="<?php echo $user['_id'] ?? '';?>">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-12">
                        <button type="submit" name="action" value="bind" class="btn btn-primary">提交保存</button>
                    </div>
                </div>
            </form>
        </div>

        <?php if (!empty($user)):?>
        <div class="panel-heading">
            解绑前端UID
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post">
                <div class="form-group">
                    <div class="col-lg-12">
                        <button type="submit" name="action" value="unbind" class="btn btn-danger" onclick="return confirm('确认解绑?')">确认解绑</button>
                    </div>
                </div>
            </form>
        </div>
        <?php endif;?>
    </div>
</div>

<?php echo (new View('admin/footer'));?>
