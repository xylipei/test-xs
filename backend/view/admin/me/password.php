<?php
use Framework\View;
echo (new View('admin/header'));?>

<?php echo (new View('admin/me/menu'))->set('on', 'password');?>

<div class="col-lg-6">
    <div class="panel panel-info">
        <div class="panel-heading">
            修改密码
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post">

                <div class="form-group">
                    <div class="col-lg-6">
                        <input class="form-control" name="password" type="password" placeholder="密码">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-primary">修改密码</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php echo (new View('admin/footer'));?>
