<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>管理后台 - w(´･ω･`)w </title>
    <link href="/dist/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">

        .box {
            position:fixed;
            top:50%;
            left: 50%;
            margin-top:-300px;
            margin-left: -190px;
            width: 330px;
            padding: 20px 50px;
        }

    </style>

</head>
<body style="background:#f1f1f1; ">
    <div class="box">
        <form class="form-horizontal" method="post">

            <div class="form-group">
                <div id="qr" style="text-align: center"></div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <input type="text" autocomplete="off" name="password" class="form-control" autofocus="autofocus" placeholder="验证码">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <button type="submit" class="col-md-12 btn btn-danger">二步验证</button>
                </div>
            </div>
        </form>
    </div>
</body>
</html>

