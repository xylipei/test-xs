<?php
use Framework\View;
echo (new View('admin/header'));?>

<?php echo (new View('admin/me/menu'))->set('on', 'action');?>

<div class="col-lg-12">
    <div class="panel panel-info">
        <div class="panel-heading">
            操作记录
        </div>
        <table class="table table-bordered table-hover">
            <thead>
            <tr class="bg-warning">
                <th>ID</th>
                <th>时间</th>
                <th>行为</th>
                <th>会话</th>
            </tr>
            </thead>
            <tbody>
            <?php if (!empty($actions)) foreach ($actions as $action):?>
                <tr>
                    <td><?php echo $action['_id'];?></td>
                    <td><?php echo date('Y-m-d H:i:s', $action['create_time']);?></td>
                    <td><?php echo $action['action'] ?? '';?></td>
                    <td><?php echo $action['session'] ?? '';?></td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <div class="panel-footer">
            <?php if (!empty($pagination)) echo $pagination;?>
        </div>
    </div>

</div>



<?php echo (new View('admin/footer'));?>
