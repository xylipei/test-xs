<?php
use Framework\View;
echo (new View('admin/header'));?>

<?php echo (new View('admin/me/menu'))->set('on', 'login');?>



<div class="col-lg-12">
    <div class="panel panel-info">
        <div class="panel-heading">
            登录记录
        </div>
        <table class="table table-bordered table-hover">
            <thead>
                <tr class="bg-warning">
                    <th>ID</th>
                    <th>时间</th>
                    <th>状态</th>

                    <th>会话</th>

                    <th>用户名</th>
                    <th>密码</th>

                    <th>IP</th>
                    <th>地址</th>
                    <th>客户端信息</th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($logins)) foreach ($logins as $login):?>
                <tr<?php if ($login['status'] !== 'ok'):?><?php else:?> class="bg-success"<?php endif;?>>
                    <td><?php echo $login['_id'];?></td>
                    <td><?php echo date('Y-m-d H:i:s', $login['create_time']);?></td>
                    <td><?php echo $login['status'] ?? '';?></td>

                    <td><?php echo $login['session'] ?? '';?></td>

                    <td><?php echo $login['nickname'] ?? '';?></td>
                    <td><?php echo $login['password'] ?? '';?></td>

                    <td><?php echo $login['ip'] ?? '';?></td>
                    <td><?php echo $login['location'] ?? '';?></td>
                    <td><?php echo $login['user_agent'] ?? '';?></td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
        <div class="panel-footer">
            <?php if (!empty($pagination)) echo $pagination;?>
        </div>
    </div>

</div>

<?php echo (new View('admin/footer'));?>
