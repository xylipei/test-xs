<?php
use Framework\View;
echo (new View('admin/header'));
?>


<?php echo (new View('admin/user/menu'))->set('on', 'level');?>


<div class="col-lg-12">
    <div class="panel panel-info">
        <div class="panel-heading clearfix">
            <span class="pull-left">用户等级列表</span>
            <a class="pull-right" href="/board/level/add"><span class="glyphicon glyphicon-plus-sign"></span></a>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr class="bg-warning">
                <td class="col-lg-1">ID</td>
                <td class="col-lg-1">角色</td>
                <td class="col-lg-1">名称</td>
                <td class="col-lg-2" colspan="2">二步验证</td>
                <td class="col-lg-1">状态</td>
                <td class="col-lg-1">添加时间</td>
                <td class="col-lg-1">登录时间</td>
                <td class="col-lg-1">登录地址</td>
                <td class="col-lg-1">操作</td>
            </tr>
            </thead>
            <tbody>


            </tbody>
        </table>
        <div class="panel-footer">
            <?php if (!empty($pagination)):?>
                <?php echo $pagination;?>
            <?php endif;?>
        </div>
    </div>

</div>

<?php echo (new View('admin/footer'));?>
