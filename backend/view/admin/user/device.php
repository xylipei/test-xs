<?php
use Framework\View;
echo (new View('admin/header'));
?>


<?php echo (new View('admin/user/menu'))->set('on', 'device');?>


<div class="col-lg-12">
    <div class="panel panel-info">
        <div class="panel-heading clearfix">
            <span class="pull-left">设备列表</span>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr class="bg-warning">
                <td class="col-lg-1">ID</td>

            </tr>
            </thead>
            <tbody>


            </tbody>
        </table>
        <div class="panel-footer">
            <?php if (!empty($pagination)):?>
                <?php echo $pagination;?>
            <?php endif;?>
        </div>
    </div>

</div>

<?php echo (new View('admin/footer'));?>
