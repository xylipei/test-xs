<?php
use Framework\View;
echo (new View('admin/header'));
?>

    <div class="col-lg-6">
        <div class="panel panel-info">
            <div class="panel-heading panel-info">
                添加渠道
            </div>
            <div class="panel-body">
                <form class="form-horizontal" method="post">
                    <div class="form-group">
                        <div class="col-lg-6">
                            <input class="form-control" name="src_id" type="text" placeholder="渠道ID">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-6">
                            <input class="form-control" name="name" type="text" placeholder="渠道名">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary">提交保存</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php echo (new View('admin/footer'));?>