<?php
use Framework\View;
echo (new View('admin/header'));
?>

    <div class="col-lg-6">
        <div class="panel panel-info">
            <div class="panel-heading panel-info">
                修改渠道
            </div>
            <div class="panel-body">
                <form class="form-horizontal" method="post">
                    <input name="src_id" type="hidden" value="<?php echo $src['_id']??'';?>">
                    <div class="form-group">
                        <div class="col-lg-6">
                            <input class="form-control" disabled type="text" value="<?php echo $src['_id']??'';?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-6">
                            <input class="form-control" name="name" type="text" placeholder="渠道名" value="<?php echo $src['name']??'';?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary">提交保存</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php echo (new View('admin/footer'));?>