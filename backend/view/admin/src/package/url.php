<?php
use Framework\View;
echo (new View('admin/header'));
?>

<div class="col-lg-12" style="margin-bottom: 15px;">
    <ul class="nav nav-tabs" role="tablist">
        <li><a href="/src">渠道管理</a></li>
        <li><a href="/src/package">打包管理</a></li>
        <li class="active"><a href="/src/package/url">域名配置</a></li>
    </ul>
</div>


<div class="col-lg-6">
    <div class="panel panel-info">
        <div class="panel-heading panel-info">
            域名配置
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post">
                <div class="form-group">
                    <label class="col-lg-1 control-label">接口</label>
                    <div class="col-lg-10">
                        <input class="form-control" type="text" name="api" value="<?php echo $url['api'] ?? '';?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-1 control-label">图片</label>
                    <div class="col-lg-10">
                        <input class="form-control" type="text" name="image" value="<?php echo $url['image'] ?? '';?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-1 control-label">视频</label>
                    <div class="col-lg-10">
                        <input class="form-control" type="text" name="video" value="<?php echo $url['video'] ?? '';?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-1 control-label">下载</label>
                    <div class="col-lg-10">
                        <input class="form-control" type="text" name="download" value="<?php echo $url['download'] ?? '';?>">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-offset-1 col-lg-10">
                        <button type="submit" class="btn btn-primary">提交保存</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php echo (new View('admin/footer'));?>
