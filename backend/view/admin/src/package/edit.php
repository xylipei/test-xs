<?php
use Framework\View;
echo (new View('admin/header'));
?>
    <script type="text/javascript">

        $(document).ready(function () {
            $(document).on('click', '.reply_list a', function () {
                var test = $(this).find('p').text();
                $('.reply_list a').removeClass('active');
                $('#kw').val(test);
                $(this).addClass('active');
            });

            $(document).on('click', '#edit', function () {
                $('#package_name').removeAttr('disabled');
            });
        });

    </script>

    <div class="col-lg-12" style="margin-bottom: 15px;">
        <ul class="nav nav-tabs" role="tablist">
            <li><a href="/src">渠道管理</a></li>
            <li class="active"><a href="/src/package">打包管理</a></li>
            <li><a href="/src/package/url">域名配置</a></li>
        </ul>
    </div>

    <div class="col-lg-8">
        <div class="panel panel-info">
            <div class="panel-heading panel-info">
                修改分包
            </div>
            <div class="panel-body">
                <form class="form-horizontal" method="post">
                    <input name="package_id" type="hidden" value="<?php echo $package['_id']??'';?>">
                    <div class="form-group">
                        <div class="col-lg-6">
                            <input name="package_name" id="package_name" class="form-control" disabled type="text" value="<?php echo $package['package_name']??'';?>">
                        </div>
                        <div class="col-lg-6" style="line-height: 30px">
                            <a id="edit" onclick="return confirm('修改需谨慎！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！')" class="btn btn-xs btn-danger" ><span class="glyphicon glyphicon-edit"></span>强制修改包名</a>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-6">
                            <input class="form-control" type="text" name="name" value="<?php echo $package['name']??'';?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-11">
                            <input class="form-control" name="icon" type="text" value="<?php echo $package['icon']??'';?>">
                        </div>
                        <div class="col-lg-1">
                            <img src="<?php echo $package['icon'];?>" width="34" height="34">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <input id="kw" type="text" name="kw_id" class="form-control" value="<?php echo $kw_id??'';?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary">提交保存</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="panel-heading panel-danger">
                删除
            </div>
            <div class="panel-body">
                <form class="form-horizontal" method="post" action="/src/package/delete">
                    <input name="package_id" type="hidden" value="<?php echo $package['_id']??'';?>">
                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" id="delete" class="btn btn-danger" onclick="return confirm('删除后将无法恢复,确定要删除?')">删除</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="panel panel-info">
            <div class="panel-heading panel-info">
                可选关键
            </div>
            <div class="panel-body  pre-scrollable">
                <div class="list-group reply_list ">
                    <?php if (!empty($kws)): ?>
                        <?php foreach ($kws as $kw): ?>
                            <a href="#" class="list-group-item <?php if ($kw['_id'] == $kw_id):?>active<?php endif;?>">
                                <strong><?php echo $kw['title'] ?></strong>
                                <p class="hidden"><?php echo $kw['_id'] ?></p>
                            </a>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php echo (new View('admin/footer'));?>