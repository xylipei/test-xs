<?php
use Framework\View;
echo (new View('admin/header'));
?>

<script type="text/javascript">

    $(document).ready(function () {
        $(document).on('click', '.reply_list a', function () {
            var test = $(this).find('p').text();
            $('.reply_list a').removeClass('active');
            $('#kw').val(test);
            $(this).addClass('active');
        });
    });
</script>

<div class="col-lg-12" style="margin-bottom: 15px;">
    <ul class="nav nav-tabs" role="tablist">
        <li><a href="/src">渠道管理</a></li>
        <li class="active"><a href="/src/package">打包管理</a></li>
        <li><a href="/src/package/url">域名配置</a></li>
    </ul>
</div>

<div class="col-lg-8">
    <div class="panel panel-info">
        <div class="panel-heading panel-info">
            添加分包
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post">
                <div class="form-group">
                    <label class="col-lg-1 control-label">选择渠道</label>
                    <div class="col-lg-6">
                        <select class="form-control" name="src_id">
                            <option value="">请所属选择渠道</option>
                            <?php if (!empty($srcs)) foreach ($srcs as $src):?>
                            <option value="<?php echo $src['_id'];?>"><?php echo $src['name'];?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-1">
                    <div class="alert alert-danger">
                        <p>一行一条数据，格式（英文逗号分隔）：</p>
                        <p>原站ID（如没有，可以自己编造）,应用名称,图标地址</p>
                        <p>1024,测试应用名称,http://www.baidu.com/icon.png</p>
                    </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-1 control-label">分包信息</label>
                    <div class="col-lg-10">
                        <textarea class="form-control" name="text" rows="20"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-1 control-label">关键词</label>
                    <div class="col-lg-10">
                        <input id="kw" type="text" name="kw_id" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-offset-1 col-lg-10">
                        <button type="submit" class="btn btn-primary">提交保存</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="col-lg-4">
    <div class="panel panel-info">
        <div class="panel-heading panel-info">
            可选关键
        </div>
        <div class="panel-body  pre-scrollable">
            <div class="list-group reply_list ">
                <?php if (!empty($kws)): ?>
                    <?php foreach ($kws as $kw): ?>
                        <a href="#" class="list-group-item">
                            <strong><?php echo $kw['title'] ?></strong>
                            <p class="hidden"><?php echo $kw['_id'] ?></p>
                        </a>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php echo (new View('admin/footer'));?>
