<?php
use Framework\View;
echo (new View('admin/header'));
?>

<link rel="stylesheet" href="/dist/css/bootstrap-select.min.css">
<script type="text/javascript" src="/dist/js/bootstrap-select.js"></script>
<script type="text/javascript" src="/dist/js/jquery.qrcode.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.selectpicker').selectpicker();
        $('span.qrcode').hover(function(){
            var html ='<div class="qr" style="z-index:9999; width:222px; height:222px; position: absolute; top: 0px; left: -200px; padding: 10px; board:1px slid #FFD700; background: #FFEFDB;"></div>';
            $(this).parent().append(html);
            var url = $(this).data('url');
            $(this).parent().find('.qr').qrcode({text: url, width: 200, height:200});
        }, function(){
            $(this).parent().find('.qr').remove();
        });
    });
</script>
<div class="col-lg-12" style="margin-bottom: 15px;">
    <ul class="nav nav-tabs" role="tablist">
        <li><a href="/src">渠道管理</a></li>
        <li class="active"><a href="/src/package">打包管理</a></li>
        <li><a href="/src/package/url">域名配置</a></li>
    </ul>
</div>

<div class="col-lg-12">
    <div class="panel panel-info">
        <div class="panel-body">
            <form class="form-horizontal" method="get">
                <div class="pull-left" style="margin-right: 5px;">
                    <input value="<?php echo $params['name'] ?? '';?>" name="name" type="text" class="input-md form-control" placeholder="名称">
                </div>

                <div class="pull-left" style="margin-right: 5px;">
                    <input value="<?php echo $params['version'] ?? '';?>" name="version" type="text" class="input-md form-control" placeholder="版本">
                </div>

                <div class="pull-left" style="margin-right: 5px;">
                    <select class="selectpicker" name="status">
                        <option value="">选择状态</option>
                        <option <?php if('packed' == ($params['status'] ?? '')):?> selected<?php endif;?> value="packed">packed</option>
                        <option <?php if('wait' == ($params['status'] ?? '')):?> selected<?php endif;?> value="wait">wait</option>
                        <option <?php if('packing' == ($params['status'] ?? '')):?> selected<?php endif;?> value="packing">packing</option>
                        <option <?php if('deleted' == ($params['status'] ?? '')):?> selected<?php endif;?> value="deleted">deleted</option>
                    </select>
                </div>

                <?php if (!empty($srcs)):?>
                <div class="pull-left" style="margin-right: 5px;">
                    <select class="selectpicker" name="src_id">
                        <option value="">选择渠道</option>
                        <?php foreach ($srcs as $id => $name):?>
                            <option <?php if($id == ($params['src_id'] ?? '')):?> selected<?php endif;?> value="<?php echo $id;?>"><?php echo $name;?></option>
                        <?php endforeach;?>
                    </select>
                </div>
                <?php endif;?>

                <div class="pull-left">
                    <button type="submit" class="btn btn-md btn-primary">筛选查询</button>
                    <a style="margin-left: 20px;" href="/src/package" class=""><span class="glyphicon glyphicon-remove-circle"></span></a>
                </div>

            </form>
        </div>

        <?php if (!empty($src)):?>
            <div class="panel-heading clearfix">
                <?php echo $src['name'];?>
            </div>
        <?php endif;?>

        <div class="panel-heading clearfix">
            <span class="pull-left">
                添加分包
                <span class="text-danger">[ 全部: <?php echo array_sum($count);?> ]</span>
                <span class="text-danger">[ 已打包: <?php echo $count['packed'] ?? 0;?> ]</span>
                <span class="text-danger">[ 未打包: <?php echo $count['wait'] ?? 0;?> ]</span>
            </span>

            <a class="btn btn-primary btn-xs pull-right" href="/src/package/add"><span class="glyphicon glyphicon-plus-sign"></span> 添加分包</a>

            <form class="pull-right" style="margin-right: 15px;" method="post" action="/src/package/rebuild">
                <input type="hidden" value="all" name="action">
                <button onclick="javascript:return confirm('确定全部重新打包？');" type="submit" class="btn btn-danger btn-xs">全部重新打包</button>
            </form>

            <form class="pull-right" style="margin-right: 15px;" method="post" action="/src/package/rebuild">
                <input type="hidden" value="packing" name="action">
                <button type="submit" class="btn btn-warning btn-xs">未成功的重新打包</button>
            </form>

            <form class="pull-right" style="margin-right: 15px;" method="post" action="/src/package/rebuild">
                <input type="hidden" value="quick" name="action">
                <button type="submit" class="btn btn-info btn-xs">重新打本月有新增的包</button>
            </form>

            <a style="margin-right: 15px;" class="pull-right btn btn-success btn-xs" href="?<?php $params['action'] = 'download'; echo http_build_query($params);?>" target="_blank">下载数据</a>

        </div>

        <table class="table table-bordered">
            <thead>
            <tr class="bg-warning">
                <td class="col-lg-1">任务ID</td>
                <td class="">新增 月/周/日</td>
                <td class="col-lg-1">所属渠道</td>
                <td class="">原始ID</td>
                <td class="">APP名称</td>
                <td class="col-lg-1">任务状态</td>
                <td class="">耗时</td>
                <td class="col-lg-1">版本</td>
                <td class="col-lg-1">下载地址</td>
                <td class="col-lg-1">文件大小</td>
                <td class="col-lg-1">操作</td>
            </tr>
            </thead>
            <tbody>
            <?php if (!empty($packages)) foreach ($packages as $package):?>
                <tr>
                    <td class="bg-warning"><?php echo $package['_id'];?></td>
                    <td>
                        <span class="btn btn-xs btn-info">
                            <?php echo $package['active'][date('Y\mm')] ?? 0;?>
                        </span>
                        <span class="btn btn-xs btn-info">
                            <?php echo $package['active'][date('Y\wW')] ?? 0;?>
                        </span>
                        <span class="btn btn-xs btn-info">
                            <?php echo $package['active'][date('Y\mm\dd')] ?? 0;?>
                        </span>
                    </td>
                    <td><?php echo $srcs[$package['src_id']] ?? $package['src_id'];?></td>

                    <td><?php echo count($package['source']);?></td>
                    <td>
                        <img src="<?php echo $package['icon'];?>" width="16" height="16">
                        <?php echo $package['name'];?>
                    </td>

                    <td><?php echo $package['status'];?></td>
                    <td>
                        <?php if (!empty($package['pack_end'])):?>
                        <?php echo (($package['pack_end'] ?? 0) - ($package['pack_start'] ?? 0));?>
                        <?php endif;?>
                    </td>
                    <td><?php echo $package['version'] ?? '';?></td>
                    <td style="position: relative;">
                        <?php if (!empty($package['download'])):?>
                        <?php $download = $url. '/'.$package['download'] ?? '';?>
                        <a href="<?php echo $download;?>" target="_blank">下载安装包</a>
                            <span style="cursor: pointer;" data-url="<?php echo $download;?>" class="qrcode glyphicon glyphicon-qrcode"></span>
                        <?php endif;?>
                    </td>
                    <td><?php echo $package['size'] ?? '';?></td>
                    <td>
                        <form class="pull-left form-inline" style="margin-right: 5px;" method="post" action="/src/package/rebuild">
                            <input type="hidden" value="one" name="action">
                            <input type="hidden" value="<?php echo $package['_id'];?>" name="id">
                            <button type="submit" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-cloud-upload"></span>打包</button>
                        </form>
                        <a href="/src/package/edit?package_id=<?php echo $package['_id'];?>" class="btn btn-xs btn-primary pull-left"><span class="glyphicon glyphicon-edit"></span>编辑</a>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <div class="panel-footer">
            <?php if (!empty($pagination)): ?>
                <?php echo $pagination; ?>
            <?php endif; ?>
        </div>
    </div>

</div>
<?php echo (new View('admin/footer'));?>
