<?php
use Framework\View;
echo (new View('admin/header'));
?>


<?php echo (new View('admin/me/menu'))->set('on', 'me');?>

<div class="col-lg-10">
    <div class="panel panel-info">
        <div class="panel-heading">
            我的信息
        </div>
        <table class="table table-bordered table-hover">
            <tr>
                <th>ID</th>
                <td><?php echo $user['_id'] ?? '';?></td>
                <th>角色</th>
                <td><?php echo $user['role'] ?? '';?></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>密码</th>
                <td>******</td>
                <th>重置时间</th>
                <?php if (!empty($user['password']['last_reset_time'])):?>
                <td>
                    <?php echo date('Y-m-d H:i:s', $user['password']['last_reset_time']);?>
                </td>
                <td colspan="2">
                    <span class="label label-danger">
                        <?php echo floor((time() + 30 * 86400 - $user['password']['last_reset_time'])/86400);?>天后必须重置
                    </span>
                </td>
                <?php else:?>
                <td colspan="3"></td>
                <?php endif;?>
            </tr>
            <tr>
                <th>二步验证</th>
                <td><?php if (($user['authenticator']['bind'] ?? false)) echo '<span class="label label-success">已绑定</span>';?></td>
                <th>绑定时间</th>
                <td><?php if (!empty($user['authenticator']['bind_time'])) echo date('Y-m-d H:i:s', $user['authenticator']['bind_time']);?></td>
                <th>上次验证时间</th>
                <td><?php if (!empty($user['authenticator']['last_valid_time'])) echo date('Y-m-d H:i:s', $user['authenticator']['last_valid_time']);?></td>
            </tr>
            <tr>
                <th>账号状态</th>
                <td colspan="5"><?php echo $user['status'] ?? '';?></td>
            </tr>
            <tr>
                <th>账号添加时间</th>
                <td><?php if (!empty($user['create_time'])) echo date('Y-m-d H:i:s', $user['create_time']);?></td>
                <th>最近账号更新时间</th>
                <td colspan="3"><?php if (!empty($user['update_time'])) echo date('Y-m-d H:i:s', $user['update_time']);?></td>

            </tr>
            <tr>
                <th>最近登录时间</th>
                <td><?php if (!empty($user['login']['last_time'])) echo date('Y-m-d H:i:s', $user['login']['last_time']);?></td>
                <th>最近登录IP</th>
                <td><?php echo $user['login']['ip'] ?? '';?></td>
                <th>最近登录地址</th>
                <td><?php echo $user['login']['location'] ?? '';?></td>
            </tr>
            <tr>
                <td colspan="6">
                    <?php echo $user['login']['user_agent'] ?? '';?>
                </td>
            </tr>
        </table>
    </div>

</div>


<?php echo (new View('admin/footer'));?>
