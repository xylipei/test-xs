<?php
use Framework\View;
echo (new View('admin/header'));
?>


    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading clearfix">
                <span class="pull-left">关键词列表</span>
                <a class="pull-right" href="/kw/add"><span class="glyphicon glyphicon-plus-sign"></span></a>
            </div>
            <table class="table table-bordered">
                <thead>
                <tr class="bg-warning">
                    <td class="col-lg-1">ID</td>
                    <td>标题</td>
                    <td>浏览量</td>
                    <td>添加时间</td>
                    <td>状态</td>
                    <td>操作</td>
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($kws)) foreach ($kws as $kw):?>
                    <tr>
                        <td class="bg-warning">
                            <?php echo $kw['_id'];?>
                        </td>
                        <td><?php echo $kw['title'] ?? '';?></td>
                        <td><?php echo $kw['view_num'] ?? 0;?></td>
                        <td><?php echo empty($kw['create_time']) ? '' : date('Y-m-d H:i', $kw['create_time']);?></td>
                        <td>
                            <?php
                            switch ($kw['status'] ?? '') {
                                case 'accepted':
                                    echo '<span class="label label-success">正常</span>';
                                    break;
                                case 'deleted':
                                    echo '<span class="label label-danger">已经删除</span>';
                                    break;
                            }
                            ?>
                        </td>
                        <td>
                            <a class="btn btn-xs btn-primary" href="/kw/edit?kw_id=<?php echo $kw['_id'];?>"><span class="glyphicon glyphicon-edit"></span>修改</a>
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
<?php echo (new View('admin/footer'));?>
