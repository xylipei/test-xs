<?php
use Framework\View;
echo (new View('admin/header'));
?>

<style>
    .list_id{
        margin-bottom: 5px;
    }
</style>

<div class="col-lg-6">
    <div class="panel panel-info">
        <div class="panel-heading panel-info clearfix">
            <span class="pull-left">修改关键词</span>
            <a class="pull-right" href="javascript:history.back(-1)"><span class="glyphicon glyphicon-log-in"></span></a>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post">
                <input type="hidden" name="kw_id" value="<?php echo $kw['_id']??'';?>">
                <div class="form-group">
                    <label for="title" class="col-lg-2 control-label">关键词</label>
                    <div class="col-lg-6">
                        <input id="title" value="<?php echo $kw['title']??'';?>" autocomplete="off" class="form-control" name="title" type="text" placeholder="关键词">
                    </div>
                </div>
                <div class="form-group">
                    <label for="book_id" class="col-lg-2 control-label">小说ID</label>
                    <div class="col-lg-6">
                        <input  class="form-control" name="book_id"  placeholder="小说ID" value="<?php echo $kw['book']['book_id']??''?>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-12 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary">提交保存</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="panel-heading panel-danger">
            删除关键词
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post" action="/kw/delete">
                <input name="bl_id" type="hidden" value="<?php echo $kw['_id']??'';?>">
                <div class="form-group">
                    <div class="col-lg-12">
                        <button type="submit" id="delete" class="btn btn-danger" onclick="return confirm('删除后将无法恢复,确定要删除?')">删除关键词</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php echo (new View('admin/footer'));?>
