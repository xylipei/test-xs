<?php
use Framework\View;
echo(new View('admin/header'));
?>

<div class="col-lg-12" style="margin-bottom: 15px;">
    <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="/src">渠道管理</a></li>
        <li><a href="/src/package">打包管理</a></li>
        <li><a href="/src/package/url">域名配置</a></li>
    </ul>
</div>
<div class="col-lg-5">
    <div class="panel panel-info">
        <div class="panel-heading clearfix">
            <span class="pull-left">渠道列表</span>
            <a class="pull-right" href="/src/add"><span class="glyphicon glyphicon-plus-sign"></span></a>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr class="bg-warning">
                <td class="col-lg-1">ID</td>
                <td class="col-lg-1">渠道名称</td>
                <td class="col-lg-1">操作</td>
            </tr>
            </thead>
            <tbody>
            <?php if (!empty($srcs)) foreach ($srcs as $src):?>
                <tr>
                    <td class="bg-warning"><?php echo $src['_id'];?></td>
                    <td class=""><?php echo $src['name']??'';?></td>
                    <td>
                        <a class="btn btn-xs btn-primary" href="/src/edit?src_id=<?php echo $src['_id'];?>"><span class="glyphicon glyphicon-edit"></span>修改</a>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <div class="panel-footer">
        </div>
    </div>

</div>
<?php echo (new View('admin/footer'));?>
