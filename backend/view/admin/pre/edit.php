<?php
use Framework\View;
echo (new View('admin/header'));
?>

<div class="col-lg-6">
    <div class="panel panel-info">
        <div class="panel-heading panel-info">
            添加套餐
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post"  autocomplete="off">
                <input type="hidden" name="pre_id" value="<?php echo $pre['_id']??'';?>">
                <div class="form-group">
                    <label class="col-lg-2 control-label">选择分类</label>
                    <div class="col-lg-3">
                        <select class="selectpicker" name="action">
                            <option>请选择分类</option>
                            <option <?php if ($pre['action'] == 'vip') echo 'selected';?> value="vip">VIP</option>
                            <option <?php if ($pre['action'] == 'balance') echo 'selected';?> value="balance">金币</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">标题</label>
                    <div class="col-lg-6">
                        <input value="<?php echo $pre['name']??'';?>" class="form-control" name="name" type="text" placeholder="标题">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-2 control-label">副标题</label>
                    <div class="col-lg-6">
                        <input value="<?php echo $pre['tip']??'';?>" class="form-control" name="tip" type="text" placeholder="">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-2 control-label">简介</label>
                    <div class="col-lg-6">
                        <textarea class="form-control" name="memo"  cols="50" rows="5" placeholder=""><?php echo $pre['memo']??'';?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-2 control-label">付费（分)</label>
                    <div class="col-lg-3">
                        <input value="<?php echo $pre['amount']??'';?>" class="form-control" name="amount" type="text" placeholder="例：(22元 = 2200)">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-2 control-label">时间/金币</label>
                    <div class="col-lg-3">
                        <input value="<?php if ($pre['action'] == 'vip'):?><?php echo (int) ($pre['num'] / 86400);?>
                        <?php else:?><?php echo $pre['num']??0;?>
                        <?php endif?>" class="form-control" name="num" type="text" placeholder="例：(天数/个数)">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-2 control-label"></label>
                    <div class="col-lg-2">
                        <button type="submit" class="btn btn-primary">提交保存</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="panel-heading panel-danger">
            修改分类状态
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post" action="/pre/status">
                <input type="hidden" name="pre_id" value="<?php echo $pre['_id'];?>">
                <?php if (($pre['status'] ?? '') == 'accepted'):?>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <p class="alert alert-danger" style="margin-bottom: 0;">隐藏</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" name="status" value="trashed" class="btn btn-sm btn-danger" onclick="return confirm('确定隐藏?')">隐藏</button>
                        </div>
                    </div>
                <?php endif;?>

                <?php if (in_array($pre['status'] ?? '', ['trashed', 'draft'])):?>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <p class="alert alert-warning" style="margin-bottom: 0;">如果有修改，先保存后发布</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" name="status" value="accepted" class="btn btn-sm btn-success" onclick="return confirm('确定发布?')">发布</button>
                        </div>
                    </div>
                <?php endif;?>

            </form>
        </div>
        <?php if (($pre['status'] ?? '') == 'draft'):?>
            <div class="panel-heading panel-danger">
                删除
            </div>
            <div class="panel-body">
                <form class="form-horizontal" method="post" action="/pre/delete">
                    <input name="id" type="hidden" value="<?php echo $pre['_id']??'';?>">
                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-danger">删除</button>
                        </div>
                    </div>
                </form>
            </div>
        <?php endif;?>
    </div>
</div>

<!--<div class="col-lg-3">-->
<!--    <div class="alert alert-warning">-->
<!--        <p>上传封面大小不得超过15M；</p>-->
<!--        <p>视频时长以秒为单位；</p>-->
<!--        <p>如果没有分类可选，请先添加分类。</p>-->
<!--        <p>-->
<!--            <a class="btn btn-xs btn-warning" href="/category/add"><span class="glyphicon glyphicon-plus-sign"></span>添加分类</a>-->
<!--        </p>-->
<!--    </div>-->
<!--</div>-->

<?php echo (new View('admin/footer'));?>
