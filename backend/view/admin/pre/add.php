<?php
use Framework\View;
echo (new View('admin/header'));
?>

<div class="col-lg-6">
    <div class="panel panel-info">
        <div class="panel-heading panel-info">
            添加套餐
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post"  autocomplete="off">
                <div class="form-group">
                    <label class="col-lg-2 control-label">选择分类</label>
                    <div class="col-lg-3">
                        <select class="selectpicker" name="action">
                            <option>请选择分类</option>
                            <option value="vip">VIP</option>
                            <option value="balance">金币</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">标题</label>
                    <div class="col-lg-6">
                        <input class="form-control" name="name" type="text" placeholder="标题">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-2 control-label">副标题</label>
                    <div class="col-lg-6">
                        <input class="form-control" name="tip" type="text" placeholder="">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-2 control-label">简介</label>
                    <div class="col-lg-6">
                        <textarea class="form-control" name="memo"  cols="50" rows="5" placeholder=""></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-2 control-label">付费（分)</label>
                    <div class="col-lg-3">
                        <input class="form-control" name="amount" type="text" placeholder="例：(22元 = 2200)">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-2 control-label text-center">时间/金币</label>
                    <div class="col-lg-3">
                        <input class="form-control" name="num" type="text" placeholder="例：(天数/个数)">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-2 control-label"></label>
                    <div class="col-lg-2">
                        <button type="submit" class="btn btn-primary">提交保存</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!--<div class="col-lg-3">-->
<!--    <div class="alert alert-warning">-->
<!--        <p>上传封面大小不得超过15M；</p>-->
<!--        <p>视频时长以秒为单位；</p>-->
<!--        <p>如果没有分类可选，请先添加分类。</p>-->
<!--        <p>-->
<!--            <a class="btn btn-xs btn-warning" href="/category/add"><span class="glyphicon glyphicon-plus-sign"></span>添加分类</a>-->
<!--        </p>-->
<!--    </div>-->
<!--</div>-->

<?php echo (new View('admin/footer'));?>
