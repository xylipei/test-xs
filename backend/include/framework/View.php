<?php
namespace Framework;

use Framework\Security\Filter;

class View
{

    private static $data_global = [];
    private $data = [];
    private $file = '';

    public function __construct($file, array $data = [])
    {
        $view = ROOT.'backend/view/'.$file .'.php';
        $defaultView  = ROOT.'backend/include/view/'.$file.'.php';

        foreach ([$view, $defaultView] as $f) {
            if (file_exists($f)) {
                $this->file = $f;
                break;
            }
        }

        if (empty($this->file)) {
            die('miss:'.$view);
        }

        $this->data = $data;
    }

    public static function setGlobal($key, $value = null)
    {
        if (is_array($key) || $key instanceof \Traversable) {
            foreach ($key as $name => $value) {
                self::$data_global[$name] = $value;
            }
        } else {
            self::$data_global[$key] = $value;
        }
    }

    public static function bindGlobal($key, &$value)
    {
        self::$data_global[$key] =& $value;
    }


    public function & __get($key)
    {
        if (array_key_exists($key, $this->data)) {
            return $this->data[$key];
        } elseif (array_key_exists($key, self::$data_global)) {
            return self::$data_global[$key];
        }
        return null;
    }

    public function __set($key, $value)
    {
        $this->set($key, $value);
    }


    public function __isset($key)
    {
        return (isset($this->data[$key]) || isset(self::$data_global[$key]));
    }

    public function __unset($key)
    {
        unset($this->data[$key], self::$data_global[$key]);
    }


    public function set($key, $value = null)
    {
        if (is_array($key) || $key instanceof \Traversable) {
            foreach ($key as $name => $value) {
                $this->data[$name] = $value;
            }
        } else {
            $this->data[$key] = $value;
        }

        return $this;
    }

    public function bind($key, & $value)
    {
        $this->data[$key] =& $value;
        return $this;
    }

    public function __toString() : string
    {
        if (!file_exists($this->file)) {
            die('miss file: '.$this->file);
        }

        $data = array_merge($this->data, self::$data_global);
        $data = Filter::html($data);
        extract($data, EXTR_SKIP | EXTR_REFS);

        try {
            ob_start();
            include $this->file;

        } catch (\Throwable $e) {
        }

        return (string) ob_get_clean();
    }

    public static function img($img_path = '/dist/images/pop.jpg', $name = 'default')
    {
        return  '/'.$img_path;
    }
}
