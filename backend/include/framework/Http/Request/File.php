<?php

namespace Framework\Http\Request;

use RuntimeException;
use InvalidArgumentException;

class File
{

    public $file;
    protected $name;
    protected $type;
    protected $size;
    protected $error = UPLOAD_ERR_OK;
    protected $sapi = false;

    /**
     * @var Stream
     */
    protected $stream;
    protected $moved = false;


    public static function uploaded(array $files)
    {
        $parsed = [];
        foreach ($files as $field => $file) {
            if (!isset($uploadedFile['error'])) {
                if (is_array($file)) {
                    $parsed[$field] = static::uploaded($file);
                }
                continue;
            }

            $parsed[$field] = [];
            if (!is_array($file['error'])) {
                $parsed[$field] = new static(
                    $file['tmp_name'],
                    $file['name'] ?? null,
                    $file['type'] ?? null,
                    $file['size'] ?? null,
                    $file['error'],
                    true
                );
            } else {
                $subArray = [];
                foreach ($file['error'] as $fileIdx => $error) {
                    $subArray[$fileIdx]['name'] = $file['name'][$fileIdx];
                    $subArray[$fileIdx]['type'] = $file['type'][$fileIdx];
                    $subArray[$fileIdx]['tmp_name'] = $file['tmp_name'][$fileIdx];
                    $subArray[$fileIdx]['error'] = $file['error'][$fileIdx];
                    $subArray[$fileIdx]['size'] = $file['size'][$fileIdx];
                    $parsed[$field] = static::uploaded($subArray);
                }
            }
        }

        return $parsed;
    }


    public function __construct($file, $name = null, $type = null, $size = null, $error = UPLOAD_ERR_OK, $sapi = false)
    {
        $this->file = $file;
        $this->name = $name;
        $this->type = $type;
        $this->size = $size;
        $this->error = $error;
        $this->sapi = $sapi;
    }

    public function getStream()
    {
        if ($this->moved) {
            throw new \RuntimeException(sprintf('%s 已经转移', $this->name));
        }

        if ($this->stream === null) {
            $this->stream = new Stream(fopen($this->file, 'r'));
        }

        return $this->stream;
    }


    public function moveTo($targetPath)
    {
        if ($this->moved) {
            throw new RuntimeException('Uploaded file already moved');
        }

        $targetIsStream = strpos($targetPath, '://') > 0;
        if (!$targetIsStream && !is_writable(dirname($targetPath))) {
            throw new InvalidArgumentException('目录不可写');
        }

        if ($targetIsStream) {
            if (!copy($this->file, $targetPath)) {
                throw new RuntimeException(sprintf('移动失败 %s to %s', $this->name, $targetPath));
            }
            if (!unlink($this->file)) {
                throw new RuntimeException(sprintf('删除失败 %s', $this->name));
            }
        } elseif ($this->sapi) {
            if (!is_uploaded_file($this->file)) {
                throw new RuntimeException(sprintf('上传错误 %s', $this->file));
            }

            if (!move_uploaded_file($this->file, $targetPath)) {
                throw new RuntimeException(sprintf('移动失败 %s to %s', $this->name, $targetPath));
            }
        } else {
            if (!rename($this->file, $targetPath)) {
                throw new RuntimeException(sprintf('移动失败 %s to %s', $this->name, $targetPath));
            }
        }

        $this->moved = true;
    }


    public function getError()
    {
        return $this->error;
    }

    public function getClientFilename()
    {
        return $this->name;
    }


    public function getClientMediaType()
    {
        return $this->type;
    }

    public function getSize()
    {
        return $this->size;
    }
}
