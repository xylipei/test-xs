<?php
namespace Framework\Http\Request;

class Cookie
{

    public static $ip = '';
    public static $user_agent = '';
    public static $salt = 'ed968e840d10d2d313a870bc131a4e2c311d7ad09bdf32b3418147221f51a6e2';
    public static $expiration = '';
    public static $path = '/';
    public static $domain = null;
    public static $secure = false;
    public static $httponly = true;

    private $data = [];

    public function __construct(array $cookies = [])
    {
        foreach ($cookies as $name => $value) {
            if (isset($value[64]) && $value[64] === '~') {
                list($hash, $value) = explode('~', $value);
                if (hash_equals($hash, $this->hash($name, $value))) {
                    $this->data[$name] = $value;
                }
            }
        }
    }

    public function get($name)
    {
        return $this->data[$name] ?? null;
    }

    public function remove($name)
    {
        setcookie($name, null, -time(), static::$path, static::$domain, static::$secure, static::$httponly);
    }

    public function set($name, $value, $expire = 0)
    {
        $expire += time();
        $value = $this->hash($name, $value).'~'.$value;
        setcookie($name, $value, $expire, static::$path, static::$domain, static::$secure, static::$httponly);
    }

    private function hash($name, $value)
    {
        return hash_hmac('sha256', $name.Cookie::$ip.Cookie::$user_agent.$value.Cookie::$salt, Cookie::$salt);
    }
}
