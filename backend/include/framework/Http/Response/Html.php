<?php

namespace Framework\Http\Response;

use Framework\Http\Response;
use Framework\View;

class Html extends Response
{
    private $view;

    public function __construct($file, array $data = [])
    {
        $this->view = new View($file, $data);
    }

    public static function success($title, $content, $uri = null)
    {
        $result = [];
        $result['title'] = $title;
        $result['content'] = $content;
        $result['uri'] = $uri;
        $result['status'] = 'success';
        return new Html('alert', $result);
    }

    public static function error($title, $content, $uri = '')
    {
        $result = [];
        $result['title'] = $title;
        $result['content'] = $content;
        $result['uri'] = $uri;
        $result['status'] = 'error';
        return new Html('alert', $result);
    }

    public function __set($key, $value)
    {
        $this->view->set($key, $value);
    }

    public function & __get($key)
    {
        return $this->view->__get($key);
    }

    public function __isset($key)
    {
        return $this->view->__isset($key);
    }

    public function __toString()
    {
        return $this->view->__toString();
    }
}

