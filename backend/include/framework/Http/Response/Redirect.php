<?php

namespace Framework\Http\Response;

use Framework\Http\Response;
use Framework\Security\Encrypt;

class Redirect extends Response
{
    public static function to($url)
    {
        $response = new Redirect($url);
        return $response;
    }

    public function __construct($url)
    {
        $this->header()->add('Location: '. $url);
    }

    public function __toString() : string
    {
        return '';
    }
}

