<?php

namespace Framework\Http\Response;

use Framework\Http\Response;

class Raw extends Response
{
    private $result = '';

    public static function notFound()
    {
        $response = new Raw();
        $response->header()->add('HTTP/1.1 404 Not Found');
        return $response;
    }

    public static function success($result)
    {
        $response = new Raw();
        $response->result = $result;
        return $response;
    }

    public static function error($result)
    {
        $response = new Raw();
        $response->result = $result;
        return $response;
    }

    public function __toString()
    {
        return $this->result;
    }
}
