<?php

namespace Framework\Http\Response;

use Framework\Http\Response;
use Framework\Security\Encrypt;

class Json extends Response
{
    private $msg = 'ok';
    private $result = null;
    private $status = 0;
    private $notification = [];

    private static $aesEnabled = false;
    private static $aesKey = '';

    public static function setAes(string $key)
    {
        static::$aesEnabled = true;
        static::$aesKey = $key;
    }

    public function addNotification($action, $msg)
    {
        $this->notification[$action] = $msg;
    }

    public static function success($result = null)
    {
        $response = new Json();
        $response->result = $result;
        return $response;
    }

    public static function error($msg, $status = 1, $result = null)
    {
        $response = new Json();
        $response->msg = $msg;
        $response->status = $status;
        $response->result = $result;
        return $response;
    }

    public function __toString() : string
    {
        $result = [];
        $result['status'] = $this->status;
        $result['msg'] = $this->msg;

        if ($this->result !== null) {
            $result['result'] = $this->result;
        }

        if (!empty($this->notification)) {
            $result['notification'] = $this->notification;
        }

        $result = json_encode($result);
        if (static::$aesEnabled) {
            $this->header()->add('Content-Type:text/plain; charset=utf-8');
            $result = Encrypt::aesEncode($result, static::$aesKey);
        } else {
            $this->header()->add('Content-Type:application/json; charset=utf-8');
        }

        return $result;
    }
}

