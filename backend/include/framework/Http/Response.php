<?php

namespace Framework\Http;

use Framework\Attribute;

class Response
{
    private $header;

    public function header() : Attribute
    {
        if (! $this->header instanceof Attribute) {
            $this->header = new Attribute();
        }
        return $this->header;
    }

    public static function notFound()
    {
        $response = new Response();
        $response->header()->add('HTTP/1.1 404 Not Found');
        return $response;
    }

    public function __toString()
    {
         return '^_^';
    }
}


