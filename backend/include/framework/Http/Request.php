<?php

namespace Framework\Http;


use Framework\Attribute;
use Framework\Http\Request\Cookie;
use Framework\Http\Request\X;

use Framework\Http\Response\Html;
use Framework\Security\Filter;
use Framework\View;

class Request
{
    private $host = null;
    private $uri = null;
    private $method = null;

    /**
     * @var Attribute
     */
    private $attribute;

    private $cookie;

    /**
     * @var Attribute
     */
    private $x = null;
    private $data;
    private $user;
    private $file;

    public function host(): string
    {
        if ($this->host === null) {
            $this->host = strtolower($_SERVER['HTTP_HOST']);
        }
        return $this->host;
    }

    public function method(): string
    {
        if ($this->method === null) {
            $method = $_SERVER['REQUEST_METHOD'] ?? '';
            $method = strtolower($method);
            $this->method = $method;
        }
        return $this->method;
    }

    public function uri(): string
    {
        if ($this->uri === null) {
            $uri = $_SERVER['REQUEST_URI'] ?? '';
            if (false !== $pos = strpos($uri, '?')) {
                $uri = substr($uri, 0, $pos);
            }
            $this->uri = trim($uri, '/');
        }
        return $this->uri;
    }

    public function attribute()
    {
        if (!$this->attribute instanceof Attribute) {
            $this->attribute = new Attribute();
        }
        return $this->attribute;
    }

    public function cookie() : Cookie
    {
        if (!$this->cookie instanceof Cookie) {
            Cookie::$user_agent = $this->user_agent();
            Cookie::$ip = $this->ip();
            $this->cookie = new Cookie($_COOKIE);
        }
        return $this->cookie;
    }

    public function x($k = null)
    {
        if ($this->x === null) {
            $this->setX();
        }

        if ($k !== null) {
            return $this->x[$k] ?? null;
        }
        return $this->x;
    }

    private function setX()
    {
        $this->x = [];
        foreach ($_SERVER as $name => $value) {
            $name = strtolower($name);
            if (strpos($name, 'http_x_mmm_') === 0) {
                $name = str_replace('http_x_mmm_', '', $name);
                $this->x[$name] = $value;
            }
        }
    }

    public function user($k = null)
    {
        if ($k !== null) {
            return $this->user[$k] ?? null;
        }
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function data($k = null, $default = null)
    {
        if ($this->data === null) {
            $this->setData();
        }

        if ($k !== null) {
            if (is_array($k)) {
                $result = [];
                foreach ($k as $n) {
                    $result[$n] = $this->data[$n] ?? $default[$n] ?? null;
                }
                return $result;
            } else {
                return $this->data[$k] ?? $default[$k] ?? null;
            }
        }

        return $this->data;
    }

    public function setData($data = null)
    {
        if (!empty($data)) {
            $this->data = $data;
        } else {
            switch ($this->method()) {
                case 'post':
                case 'put':
                    $mime = 'unknown';
                    if (isset($_SERVER['HTTP_CONTENT_TYPE'])) {
                        if (preg_match('/^[a-z0-9\-]+\/(?<mime>[a-z0-9\-]+)[^a-z0-9\-]?/', $_SERVER['HTTP_CONTENT_TYPE'], $match)) {
                            $mime = $match['mime'];
                            unset($match);
                        }
                    }
                    switch ($mime) {
                        case 'json':
                            $this->data = json_decode(file_get_contents('php://input'), true);
                            break;
                        default:
                            $this->data = !empty($_POST) ? $_POST : null;
                            break;
                    }

                    break;
                case 'get':
                    $this->data = $_GET;
                    break;
                case 'delete':
                    $this->data = null;
                    break;
            }
        }

        if (!empty($this->data)) {
            $this->data = Filter::xss($this->data);
        }
    }

    public function user_agent()
    {
        return strtolower(($_SERVER['HTTP_USER_AGENT'] ?? ''));
    }

    public function ip()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        return (string) filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_RES_RANGE | FILTER_FLAG_NO_PRIV_RANGE);
    }

    private function __clone()
    {

    }

    public function html() : Html
    {
        $name = explode('\\', $this->attribute()->get('handlerName'));
        array_shift($name);
        View::setGlobal('nav', strtolower($name[1] ?? ''));
        return new Html(strtolower(implode('/', $name)));
    }

    public function response(Response $response)
    {
        foreach ($response->header() as $header) {
            header($header);
        }
        echo $response;
    }
}
