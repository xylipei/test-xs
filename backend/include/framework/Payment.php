<?php
namespace Framework;

class Payment
{
    const api = 'http://api.ccgcgk.cn:81/platform/pay/unifiedorder/video';
    //http://weixin.96ahs4.cn:81
    const app_api = 'http://api.9y665j.cn:81/platform/pay/unifiedorder/video';
    const mch_id = 'hhuio956';
    const key = '88f8e4b46b432e9d458edf661429bf90';
    const package = 'com.auybbb.ahhuspp';

    public static function make($order)
    {
        $params = [];
        $params['mch_id'] = self::mch_id;
        $params['body'] = $order['title'];
        $params['total_fee'] = $order['amount'];
        $params['out_trade_no'] = $order['_id'];

        $api = '';

        switch ($order['pm']) {
            case 'wxh5':
                $params['trade_type'] = 'WX';
                $params['action'] = 'wap';
                $api = self::api;
                break;
            case 'alih5':
                $params['trade_type'] = 'ALI';
                $api = self::api;
                break;
            case 'wxapp':
                $params['trade_type'] = 'WX_APP';
                $params['package'] = self::package;
                $api = self::app_api;
                break;
        }

        $params['spbill_create_ip'] = ip() ?: '127.0.0.1';

        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, ':60000') !== false) {
            $host = str_replace(':60000', ':50000', $host);
        }

        $params['notify_url'] = 'http://'.$host.'/notify/paid';
        $params['redirect_url'] = 'http://www.baidu.com';

        $params['sign'] = self::sign($params);
        return $api.'?'.http_build_query($params);
    }

    public static function sign(array $params)
    {
        unset($params['sign']);
        $params = array_filter($params);
        ksort($params);
        $params['key'] = self::key;
        $str = [];
        if (!empty($params)) foreach ($params as $name => $value) {
            $str[] = $name.'='.$value;
        }
        return md5(implode('&', $str));
    }
}
