<?php

namespace Framework\Security;

class Filter
{

    private static $htmlPurifier = null;

    public static function htmlPurifier()
    {
        if (self::$htmlPurifier === null) {
            $config = \HTMLPurifier_Config::createDefault();
            $config->set('Cache.SerializerPath', ROOT.'backend/var/cache');
            self::$htmlPurifier = new \HTMLPurifier($config);
        }
        return self::$htmlPurifier;
    }


    public static function xss(array $params)
    {
        $htmlPurifier = self::htmlPurifier();
        $result = null;

        if (!empty($params)) {
            if (is_array($params)) {
                $result = [];
                foreach ($params as $name => $value) {
                    if (is_string($name)) {
                        $name = $htmlPurifier->purify($name);
                    }
                    if (is_array($value)) {
                        $result[$name] = self::xss($value);
                    }
                    if (is_string($value)) {
                        $result[$name] = $htmlPurifier->purify($value);
                    } else {
                        $result[$name] = $value;
                    }
                }
            } else {
                if (is_string($params)) {
                    $result = $htmlPurifier->purify($params);
                } else {
                    $result = $params;
                }
            }
        }

        return $result;
    }

    public static function html($params)
    {
        $result = [];
        $ops = ENT_QUOTES | ENT_SUBSTITUTE | ENT_XHTML;
        if (!empty($params)) foreach ($params as $name => $value) {
            if (is_string($name)) {
                $name = htmlentities($name, $ops);
            }
            if (is_array($value)) {
                $result[$name] = self::html($value);
            }
            if (is_string($value)) {
                $result[$name] = htmlentities($value, $ops);
            } else {
                $result[$name] = $value;
            }
        }
        return $result;
    }

    public static function str(string $str)
    {
        $str = Filter::htmlPurifier()->purify($str);
        $str = iconv('UTF-8', 'GB2312//IGNORE', $str);
        $str = iconv('GB2312', 'UTF-8//IGNORE', $str);
        $str = htmlentities($str, ENT_QUOTES);
        return $str;
    }
}
