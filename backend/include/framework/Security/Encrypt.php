<?php

namespace Framework\Security;


class Encrypt
{
    public static function aesDecode($str, $password)
    {
        $str = base64_decode($str);
        $result = openssl_decrypt(substr($str, 48), 'aes-256-cbc', $password, OPENSSL_RAW_DATA, substr($str, 0, 16));
        if (hash_equals(substr($str, 16, 32), hash_hmac('sha256', $result, $password, true))) {
            return $result;
        }
        return false;
    }

    public static function aesEncode($str, $password)
    {
        $iv = openssl_random_pseudo_bytes(16);
        $hash = hash_hmac('sha256', $str, $password, true);
        return base64_encode($iv.$hash.openssl_encrypt($str, 'aes-256-cbc', $password, OPENSSL_RAW_DATA, $iv));
    }
}


