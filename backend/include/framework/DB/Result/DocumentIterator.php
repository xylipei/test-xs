<?php
namespace Framework\DB\Result;

use MongoDB\Driver\Cursor;
use JsonSerializable;

class DocumentIterator implements JsonSerializable
{
    private $result = [];

    private $fn = [];

    private function __construct(Cursor $cursor)
    {
        foreach ($cursor as $c) {
            $this->result[] = $c;
        }
    }

    public static function factory(Cursor $cursor)
    {
        return new static($cursor);
    }

    public function map(callable $fn)
    {
        $this->fn[] = $fn;
        return $this;
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    public function toArray($name = null, $value = null)
    {
        $result = [];
        foreach ($this->result as $k => $item) {
            $item = Document::factory($item)->toArray();
            if (!empty($this->fn)) foreach ($this->fn as $fn) {
                if (is_callable($fn)) {
                    $item = call_user_func($fn, $item);
                }
            }

            if ($name == null && $value == null) {
                $result[] = $item;
            }

            if ($name !== null && $value == null) {
                $result[$item[$name]][] = $item;
            }

            if ($name !== null && $value !== null) {
                $result[$item[$name]] = $item[$value];
            }

            if ($name === null && $value !== null) {
                $result[] = $item[$value];
            }
        }

        return $result;
    }
}


