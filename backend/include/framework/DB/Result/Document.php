<?php
namespace Framework\DB\Result;

use MongoDB\BSON\Binary;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime;
use MongoDB\Model\BSONDocument;

class Document
{
    private $result;

    private function __construct($result)
    {
        $this->result = $result;
    }

    public static function factory(iterable $document)
    {
        return new static((array) $document);
    }

    public function toArray() : array
    {
        array_walk_recursive($this->result, function (&$value, $name) {
            if (is_iterable($value)) {
                $value = Document::factory($value)->toArray();
            } elseif($value instanceof ObjectId) {
                $value = (string) $value;
            } elseif ($value instanceof UTCDateTime) {
                $value = $value->toDateTime()->getTimestamp();
            } elseif ($value instanceof Binary) {
                switch ($value->getType()) {
                    case Binary::TYPE_UUID:
                        $value = join("-", unpack("H8time_low/H4time_mid/H4time_hi/H4clock_seq_hi/H12clock_seq_low", $value->getData()));
                        break;
                    default:
                        $value = $value->getData();
                        break;
                }
            } elseif ($value instanceof BSONDocument) {
                $value = Document::factory($value)->toArray();
            }
        });
        return $this->result;
    }
}
