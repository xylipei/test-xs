<?php
namespace Framework\DB;

use Framework\DB\Result\Document;
use Framework\DB\Result\DocumentIterator;
use MongoDB\BSON\Binary;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime;

use MongoDB\Driver\Cursor as MongoCursor;
use MongoDB\Model\BSONDocument as MongoBSONDocument;
use MongoDB\Model\BSONArray as MongoBSONArray;

use MongoDB\InsertOneResult as MongoInsertOneResult;
use MongoDB\UpdateResult as MongoUpdateResult;
use MongoDB\DeleteResult as MongoDeleteResult;
use MongoDB\InsertManyResult as MongoInsertManyResult;
use MongoDB\BulkWriteResult as MongoBulkWriteResult;


use MongoDB\Model\CollectionInfo as MongoCollectionInfo;
use MongoDB\Model\CollectionInfoIterator as MongoCollectionInfoIterator;
use MongoDB\Model\DatabaseInfo as MongoDatabaseInfo;
use MongoDB\Model\DatabaseInfoIterator as MongoDatabaseInfoIterator;
use MongoDB\Model\IndexInfo as MongoIndexInfo;
use MongoDB\Model\IndexInfoIterator as MongoIndexInfoIterator;

use MongoDB\ChangeStream as MongoChangeStream;
use MongoDB\MapReduceResult as MongoMapReduceResult;

class Result
{

    private $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public static function factory($result)
    {
        if ($result === null || is_scalar($result)) {
            return $result;
        }

        if (is_array($result)) {
            return Document::factory($result)->toArray();
        }

        if (is_object($result)) {

            if ($result instanceof MongoInsertOneResult) {
                if ($result->getInsertedCount()) {
                    return (string) $result->getInsertedId();
                }
                return false;
            }

            if ($result instanceof MongoUpdateResult) {
                return (int) $result->getModifiedCount();
            }

            if ($result instanceof MongoDeleteResult) {
                return (int) $result->getDeletedCount();
            }

            if ($result instanceof MongoInsertManyResult) {
                if ($result->getInsertedCount() > 0) {
                    return array_map(function ($id) {
                        return (string) $id;
                    }, $result->getInsertedIds());
                }
                return false;
            }

            if ($result instanceof MongoBulkWriteResult) {
                return [
                    'insert' => array_map(function ($id) {
                        return (string) $id;
                    }, $result->getInsertedIds()),
                    'update' => $result->getModifiedCount(),
                    'delete' => $result->getDeletedCount(),
                    'upsert' => array_map(function ($id) {
                        return (string) $id;
                    }, $result->getUpsertedIds()),
                ];
            }

            if ($result instanceof MongoBSONDocument) {
                return Document::factory($result)->toArray();
            }

            if ($result instanceof MongoCursor) {
                return DocumentIterator::factory($result);
            }

            if ($result instanceof MongoCollectionInfoIterator) {
                $list = [];
                foreach ($result as $r) {
                    $list[] = $r->__debugInfo();
                }
                return Document::factory($list)->toArray();
            }
        }

        return false;
    }
}



