<?php

namespace Framework\DB;

use Framework\DB\Result\DocumentIterator;
use MongoDB\Database as MongoDatabase;

/**
 * @method Result listCollections(array $options = [])
 * @method Result drop(array $options = [])
 * @method Result createCollection($collectionName, array $options = [])
 * @method Result dropCollection($collectionName, array $options = [])
 * @method Result|DocumentIterator command($command, array $options = [])
 */

class Database
{
    /**
     * @var MongoDatabase
     */
    protected $connect;

    public function __construct(MongoDatabase $connect)
    {
        $this->connect = $connect;
    }

    public function __call($func, $arguments)
    {
        $result = call_user_func_array([$this->connect, $func], $arguments);
        return Result::factory($result);
    }




}

