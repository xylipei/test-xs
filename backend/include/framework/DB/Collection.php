<?php

namespace Framework\DB;

use Framework\DB\Result\DocumentIterator;
use MongoDB\Collection as MongoCollection;

/**
 * @method Result aggregate(array $pipeline, array $options = [])
 * @method Result bulkWrite(array $operations, array $options = [])
 * @method Result count($filter = [], array $options = [])
 * @method Result createIndex($key, array $options = [])
 * @method Result deleteMany($filter, array $options = [])
 * @method Result deleteOne($filter, array $options = [])
 * @method Result distinct($fieldName, $filter = [], array $options = [])
 * @method Result drop(array $options = [])
 * @method Result dropIndex($indexName, array $options = [])
 * @method DocumentIterator find($filter = [], array $options = [])
 * @method Result findOne($filter = [], array $options = [])
 * @method Result findOneAndDelete($filter = [], array $options = [])
 * @method Result findOneAndReplace($filter, $replacement, array $options = [])
 * @method Result findOneAndUpdate($filter, $update, array $options = [])
 * @method Result getCollectionName()
 * @method Result getDatabaseName()
 * @method Result insertMany(array $documents, array $options = [])
 * @method Result insertOne($document, array $options = [])
 * @method Result listIndexes(array $options = [])
 * @method Result updateMany($filter, $update, array $options = [])
 * @method Result updateOne($filter, $update, array $options = [])
 */

class Collection
{
    /**
     * @var MongoCollection
     */
    protected $connect;

    public function __construct(MongoCollection $connect)
    {
        $this->connect = $connect;
    }

    public function __call($func, $arguments)
    {
        $result = call_user_func_array([$this->connect, $func], $arguments);
        return Result::factory($result);
    }
}
