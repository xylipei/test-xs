<?php
namespace Framework\Middleware;

use Framework\Http\Request;
use Framework\Http\Response;

class Handler
{
    public function __construct(callable $fn)
    {
        $this->fn = $fn;
    }

    public function handle(Request $request) : Response
    {
        return call_user_func($this->fn, $request);
    }

    public function next(Request $request, Handler $next) : Response
    {
        return call_user_func_array($this->fn, [$request, $next]);
    }
}


