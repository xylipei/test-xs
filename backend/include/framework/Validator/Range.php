<?php
namespace Framework\Validator;

use Respect\Validation\Rules\AbstractRule;


class Range extends AbstractRule
{
    public $min = null;
    public $max = null;

    public function __construct($min = null, $max = null)
    {
        $this->min = $min;
        $this->max = $max;
    }

    public function validate($input)
    {
        if (isset($input['min']) && isset($input['max'])) {
            if ($input['min'] >= $this->min && $input['min'] <= $this->max) {
                if ($input['max'] >= $this->min && $input['max'] <= $this->max) {
                    if ($input['min'] < $input['max']) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
