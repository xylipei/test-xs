<?php
namespace Framework\Validator;

use Respect\Validation\Rules\AbstractRule;

class DeviceId extends AbstractRule
{
    public function validate($input)
    {
        return preg_match('/^[a-z0-9\-]{36}$/', $input);
    }
}
