<?php
namespace Framework\Validator;

use Respect\Validation\Exceptions\ValidationException;

class XException extends ValidationException
{
    public static $defaultTemplates = [
        self::MODE_DEFAULT => [
            self::STANDARD => '公共参数错误',
        ],
        self::MODE_NEGATIVE => [
            self::STANDARD => '公共参数错误',
        ],
    ];
}