<?php
namespace Framework\Validator;

use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Rules\AbstractRule;
use Respect\Validation\Validator;

class X extends AbstractRule
{
    public function validate($input)
    {
        $validator = Validator::keySet(
            Validator::key('request_id', Validator::id(), true),
            Validator::key('device_id', Validator::deviceId(), true),
            Validator::key('package_name', Validator::regex('/^[a-z][a-z0-9_]*(\.[a-z0-9_]+)+[0-9a-z_]$/i'), true),
            Validator::key('package_id', Validator::id(), true),
            Validator::key('os', Validator::in(['android', 'ios']), true),
            Validator::key('encrypt', Validator::in([0, 1]), true),
            Validator::key('version', Validator::version(), true),
            Validator::key('session', Validator::oneOf(Validator::id(), Validator::equals('')), true),
            Validator::key('src', Validator::id(), true),
            Validator::key('timestamp', Validator::timestamp(), true),
            Validator::key('sign', Validator::regex('/^[a-z0-9]{64}$/'), true)
        );

        try {
            $validator->assert($input);
            $sign = $input['sign'];
            unset($input['sign']);
            $salt = $input['request_id'];
            ksort($input);
            $str = http_build_query($input);
            if (strcasecmp(hash_hmac('sha256', $str, $salt), $sign) === 0) {
                return true;
            }
        } catch (NestedValidationException $exception) {

        }
        return false;
    }
}
