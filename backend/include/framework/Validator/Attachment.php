<?php
namespace Framework\Validator;

use Respect\Validation\Rules\AbstractRule;
use Respect\Validation\Validator;

class Attachment extends AbstractRule
{
    public function validate($input)
    {
        $validator = Validator::oneOf(
            Validator::keySet(
                Validator::key('mime', Validator::stringType(), true),
                Validator::key('uri', Validator::stringType(), true),
                Validator::key('w', Validator::intVal(), false),
                Validator::key('h', Validator::intVal(), false),
                Validator::key('ave', Validator::stringType(), false)
            )
        );
        return $validator->validate($input);
    }
}
