<?php
namespace Framework\Validator;

use Respect\Validation\Rules\AbstractRule;

class Timestamp extends AbstractRule
{
    public function validate($input)
    {
        return strtotime(date('Y-m-d H:i:s', $input)) === (int) $input;
    }
}
