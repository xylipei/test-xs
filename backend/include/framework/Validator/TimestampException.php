<?php
namespace Framework\Validator;

use Respect\Validation\Exceptions\ValidationException;

class TimestampException extends ValidationException
{
    public static $defaultTemplates = [
        self::MODE_DEFAULT => [
            self::STANDARD => '{{name}} must be an Timestamp',
        ],
        self::MODE_NEGATIVE => [
            self::STANDARD => '{{name}} must not be an Timestamp',
        ],
    ];
}
