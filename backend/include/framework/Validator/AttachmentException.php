<?php
namespace Framework\Validator;

use Respect\Validation\Exceptions\ValidationException;

class AttachmentException extends ValidationException
{
    public static $defaultTemplates = [
        self::MODE_DEFAULT => [
            self::STANDARD => '附件数据不合法',
        ],
        self::MODE_NEGATIVE => [
            self::STANDARD => '附件数据不合法',
        ],
    ];
}
