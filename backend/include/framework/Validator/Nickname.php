<?php
namespace Framework\Validator;

use Respect\Validation\Rules\AbstractRule;
use Respect\Validation\Validator;

class Nickname extends AbstractRule
{
    public function validate($input)
    {
        $input = preg_replace('/[\x{4e00}-\x{9fa5}]/u', 'aa', $input);
        return Validator::noWhitespace()->length(4, 20)->validate($input);
    }
}
