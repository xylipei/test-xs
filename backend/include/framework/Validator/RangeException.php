<?php
namespace Framework\Validator;

use Respect\Validation\Exceptions\ValidationException;

class RangeException extends ValidationException
{
    public static $defaultTemplates = [
        self::MODE_DEFAULT => [
            self::STANDARD => '{{name}} must be an Id',
        ],
        self::MODE_NEGATIVE => [
            self::STANDARD => '{{name}} must not be an Id',
        ],
    ];
}
