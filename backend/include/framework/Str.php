<?php
namespace Framework;

class Str
{
    public static function htmlentities($str)
    {
        $str = iconv('UTF-8', 'gb2312//IGNORE', $str);
        $str = htmlentities($str, ENT_QUOTES, 'gb2312');
        return iconv('gb2312', 'UTF-8//IGNORE', $str);
    }

    public static function multiexplode ($delimiters,$string) {

        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return  $launch;
    }
}
