<?php

namespace Framework;

use Framework\DB\Result;
use Framework\DB\Result\DocumentIterator;
use MongoDB\BSON\ObjectId;

/**
 * @method static DocumentIterator aggregate(array $pipeline, array $options = [])
 * @method static Result bulkWrite(array $operations, array $options = [])
 * @method static Result count($filter = [], array $options = [])
 * @method static Result createIndex($key, array $options = [])
 * @method static Result deleteMany($filter, array $options = [])
 * @method static Result deleteOne($filter, array $options = [])
 * @method static Result distinct($fieldName, $filter = [], array $options = [])
 * @method static Result drop(array $options = [])
 * @method static Result dropIndex($indexName, array $options = [])
 * @method static DocumentIterator find($filter = [], array $options = [])
 * @method static Result findOne($filter = [], array $options = [])
 * @method static Result findOneAndDelete($filter = [], array $options = [])
 * @method static Result findOneAndReplace($filter, $replacement, array $options = [])
 * @method static Result findOneAndUpdate($filter, $update, array $options = [])
 * @method static Result getCollectionName()
 * @method static Result getDatabaseName()
 * @method static Result insertMany(array $documents, array $options = [])
 * @method static Result insertOne($document, array $options = [])
 * @method static Result listIndexes(array $options = [])
 * @method static Result updateMany($filter, $update, array $options = [])
 * @method static Result updateOne($filter, $update, array $options = [])
 */

class Model
{
    public static function __callStatic($name, $arguments)
    {
        $database = !empty(static::$database) ? static::$database : 'default';
        $collection = !empty(static::$collection) ? static::$collection : strtolower(get_called_class());
        $db = DB::collection($collection, $database);

        return call_user_func_array([$db, $name], $arguments);
    }


    public static function findIds(array $ids, $options = [])
    {
        $ids = array_map(function($id){
            return new ObjectId($id);
        }, array_values($ids));

        return self::find(['_id' => ['$in' => $ids]], $options);
    }


    public static function updateIds(array $ids, array $update, array $options)
    {
        $ids = array_map(function($id){
            return new ObjectId($id);
        }, $ids);
        return self::updateMany(['_id' => ['$in' => $ids]], $update, $options);
    }
}


