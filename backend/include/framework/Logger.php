<?php
namespace Framework;

class Logger
{
    private $dateFormat = 'Y-m-d H:i:s';
    private static $instance;

    private static function instance() : Logger
    {
        if (!static::$instance instanceof Logger) {
            static::$instance = new Logger();
        }
        return static::$instance;
    }

    public static function log($message)
    {
        $logger = static::instance();
        $logger->write($message);
    }

    private function file()
    {
        $dir = ROOT.'backend/var/log/';
        if (!is_dir($dir) && !mkdir($dir, 0777, true)) {
            return false;
        }
        return $dir.date('Ymd').'.txt';
    }

    private function write($message)
    {
        $now = date($this->dateFormat);
        if ($file = $this->file()) {
            file_put_contents($file, $now."\n".$message."\n\n", FILE_APPEND);
        }
    }
}
