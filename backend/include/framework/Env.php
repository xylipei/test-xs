<?php

namespace Framework;


class Env extends Attribute
{
    private static $env = [];

    public static function write($name, $params)
    {
        $file = ROOT . 'etc'.'/'.ENVIRONMENT.'/'.strtolower($name).'.php';
        $data = '<?php'."\n";
        $data.= 'return '.var_export($params, true).';';
        $data.= "\n";

        return file_put_contents($file, $data);
    }

    public static function read(string $name) : Env
    {

        if (!isset(static::$env[$name]) || !static::$env[$name] instanceof Env) {
            $files = [
                ROOT.'backend/env/'.$name.'.php',
                ROOT.'backend/env/'.ENVIRONMENT.'/'.$name.'.php'
            ];
            $env = new static();
            foreach ($files as $file) {
                if (!file_exists($file)) continue;
                $params = include $file;
                if (is_array($params)) {
                    $env->merge($params);
                }
            }
            self::$env[$name] = $env;
        }

        return self::$env[$name];
    }
}
