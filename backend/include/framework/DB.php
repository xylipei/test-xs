<?php
namespace Framework;


use Framework\DB\Database;
use Framework\DB\Collection;
use MongoDB\Client as MongoClient;


class DB
{
    /**
     * @var DB
     */
    private static $db;
    private $connections;
    private static $env;

    const options = [];


    public static function instance()
    {
        if (!isset(static::$db) || !static::$db instanceof DB) {
            static::$db = new DB();
        }
        return static::$db;
    }

    public function connection($name, $env) : MongoClient
    {

        if (!isset($this->connections[$name]) || !$this->connections[$name] instanceof MongoClient) {
            $this->connections[$name] = new MongoClient($env['uri'] ?? '', $env['uriOptions'] ?? [], $env['driverOptions'] ?? []);
        }
        return $this->connections[$name];
    }

    public static function database(string $database = 'default') : Database
    {
        $db = DB::instance();
        $env = Env::read('mongodb')->get($database);
        $connection = $db->connection($database, $env)->selectDatabase($env['database'])->withOptions(static::options);
        return new Database($connection);
    }

    public static function collection(string $collection, string $database = 'default') : Collection
    {
        $db = DB::instance();
        $env = Env::read('mongodb')->get($database);
        $connection = $db->connection($database, $env)->selectDatabase($env['database'])->selectCollection($collection)->withOptions(static::options);
        return new Collection($connection);
    }


    public function all()
    {
        $env = Env::read('mongodb')->all();
        $result = [];
        if (!empty($env)) foreach ($env as $dbName => $dbEnv) {
            $result[$dbName]['env'] = $dbEnv;
            $collections = DB::database($dbName)->listCollections();
            if (!empty($collections)) foreach ($collections as  $collectionInfo) {
                $collectionName = $collectionInfo['name'] ?? '';
                $info = DB::database($dbName)->command([
                    'collStats' => $collectionName,
                    'scale' => 1
                ])->toArray();
                $result[$dbName]['collections'][$collectionName] = $info[0] ?? [];
            }
        }
        return $result;
    }

}
