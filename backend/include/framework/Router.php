<?php

declare(strict_types=1);

namespace Framework;

use Framework\Http\Request;
use Framework\Http\Response;

use Framework\Middleware\Handler;


class Router
{
    public function setRouterMiddleware(Middleware $middleware) : void
    {
        $middleware->add(function (Request $request, Handler $next) : Response {
            $application = $request->attribute()->get('application');
            if (!class_exists($application)) {
                return Response::notFound();
            }
            $handler = str_replace('/', '\\', ucwords($request->uri(), '/'));
            $handler = !empty($handler) ? $application.'\\'.$handler : $application;
            $request->attribute()->set('handlerName', $handler);
            return $next->handle($request);
        });
    }

    final public function __invoke(Request $request) : Response
    {
        $application = $request->attribute()->get('application');
        $application = new $application();
        return Middleware::call($application, ['setApplicationMiddleware'])->run($request);
    }

    public static function run(callable $dispatcher) : void
    {
        $router = new Router();
        $middleware = Middleware::call($router);
        $middleware->add(function (Request $request, Handler $next) use ($dispatcher) :Response {
            $application = $dispatcher($request);
            $application = 'Application\\'.ucfirst($application);
            $request->attribute()->set('application', $application);
            return $next->handle($request);
        });
        $middleware->register([$router, 'setRouterMiddleware']);

        $request = new Request;
        $response = $middleware->run($request);
        $request->response($response);
    }
}
