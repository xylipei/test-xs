<?php

namespace Framework\View;

use Framework\View;

class Pagination
{
    const start = 1;
    const first = 'first';
    const less = 'less';
    const prev = 'prev';
    const current = 'current';
    const next = 'next';
    const more = 'more';
    const last = 'last';

    private $count;
    private $current;
    private $per;
    private $max;
    private $offset;
    private $num;

    public function __construct($count, $per = 50)
    {
        $this->count = (int) $count;
        $this->current = (int) ($_GET['page'] ?? 1);
        $this->per = (int) $per;
        $this->num = (int) 4;

        if ($this->current < self::start) {
            $this->current = self::start;
        }

        $this->max = (int) ceil($this->count / $this->per);
        if ($this->current > $this->max) {
            $this->current = $this->max;
        }
        $this->offset = abs(intval($this->current * $this->per - $this->per));
    }

    public function skip()
    {
        return (int) $this->offset;
    }

    public function limit()
    {
        return (int) $this->per;
    }

    public function current()
    {
        return (int) $this->current;
    }

    public function max()
    {
        return (int) $this->max;
    }

    public function build()
    {
        if ($this->count == 0 || $this->max == 1) {
            return [];
        }
        $output = [];
        $offset = $this->current - 1;
        $limit = $this->current - $this->num;
        $limit = $limit < self::start ? self::start : $limit;
        for ($i = $offset; $i >= $limit; $i--) {
            $output[$i] = self::prev;
        }

        if ($limit - self::start >= 2) {
            $output[$limit - 1] = self::less;
        }

        if ($this->current - $this->num > self::start) {
            $output[self::start] = self::first;
        }

        $offset = $this->current + 1;
        $limit = $this->current + $this->num;
        $limit = $limit > $this->max ? $this->max : $limit;
        for ($i = $offset; $i <= $limit; $i++) {
            $output[$i] = self::next;
        }

        if ($this->max - $limit > 0) {
            $output[$limit + 1] = self::more;
        }

        if ($this->current + $this->num < $this->max) {
            $output[$this->max] = self::last;
        }

        $output[$this->current] = self::current;
        ksort($output);
        return $output;
    }

    public function __toString()
    {
        $view = new View('pagination');
        $view->set('pagination', $this->build());
        return (string) $view;
    }
}
