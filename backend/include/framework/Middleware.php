<?php

namespace Framework;


use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Middleware\Handler;

class Middleware
{
    protected $stack = [];
    protected $handler;

    public static function call(callable $handler, $registers = []) : Middleware
    {
        $middleware = new Middleware($handler);
        if (!empty($registers)) foreach ($registers as $register) {
            if (method_exists($handler, $register)) {
                $middleware->register([$handler, $register]);
            }
        }
        return $middleware;
    }

    private function __construct(callable $handler)
    {
        $this->handler = $handler;
    }

    private function __clone(){}

    public function register($fn) : Middleware
    {
        if (is_callable($fn)) {
            call_user_func($fn, $this);
        }
        return $this;
    }

    public function add(callable $fn) : Middleware
    {
        $this->stack[] = new Handler($fn);
        return $this;
    }

    public function run(Request $request): Response
    {
        $this->add($this->handler);
        $resolved = $this->resolve(0);
        return $resolved->handle($request);
    }

    private function resolve(int $index): Handler
    {
        return new Handler(function (Request $request) use ($index) {
            $middleware = $this->stack[$index];
            $next = $index + 1;
            if (isset($this->stack[$next])) {
                return $middleware->next($request, $this->resolve($next));
            } else {
                return $middleware->handle($request);
            }
        });
    }
}
