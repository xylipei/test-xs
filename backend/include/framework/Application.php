<?php

namespace Framework;

use Framework\Http\Request;
use Framework\Http\Response;

class Application
{
    final public function __invoke(Request $request) : Response
    {
        $handler = $request->attribute()->get('handler');
        $method = strtolower($request->method());

        $middleware = Middleware::call([$handler, $method]);
        $middleware->register([$handler, 'setMiddleware']);
        $middleware->register([$handler, 'set'.ucfirst($method).'Middleware']);
        return $middleware->run($request);
    }
}
