<?php
if (!empty($pagination)):?>
<ul class="pagination">
    <?php foreach ($pagination as $num => $name):?>
    <li<?php if ($name == 'current'):?> class="active"<?php endif;?>>
        <a href="?<?php $_GET['page'] = $num; echo http_build_query($_GET);?>"><?php echo $num;?></a>
    </li>
    <?php endforeach;?>
</ul>
<?php endif;?>
