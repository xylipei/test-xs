<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <?php if (!empty($uri)) :?>
    <meta http-equiv="refresh" content="3; url=<?php echo $uri;?>">
    <?php else :?>
    <meta http-equiv="refresh" content="3; url=javascript:window.history.go(-1);">
    <?php endif;?>
    <title>(..•˘_˘•..)</title>
    <link href="/dist/css/bootstrap.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="col-lg-offset-3 col-lg-6 alert <?php if (isset($status) && $status == 'error') :
        ?> alert-danger<?php else:?>alert-success<?php endif;?>" style="margin-top: 150px; padding: 0 15px 10px;">
        <h2 class=""><?php echo $title;?></h2>
        <?php if (!empty($uri)) :?>
            <h5><a href="<?php echo $uri;?>"><?php echo $content;?></a></h5>
        <?php else :?>
            <h5><a href="javascript:void(0)" onclick="window.history.go(-1)"><?php echo $content;?></a></h5>
        <?php endif;?>
    </div>
</div>
</body>
</html>