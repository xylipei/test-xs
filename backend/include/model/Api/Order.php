<?php


namespace Model\Api;


use Framework\Model;

class Order extends Model
{
    protected static $database = 'default';
    protected static $collection = 'order';
}