<?php


namespace Model\Api;


use Framework\Model;
use MongoDB\BSON\ObjectId;

class Bill extends Model
{
    protected static $database = 'default';
    protected static $collection = 'bill';

    public static function paid($uid, $cid)
    {
        return (bool) Bill::findOne([
            'user_id' => new ObjectId($uid),
            'chapter.chapter_id' => new ObjectId($cid)
        ]);
    }
}