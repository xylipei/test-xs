<?php
namespace Model\Api\Order;

use Framework\Model;
use MongoDB\BSON\ObjectId;

class Pre extends Model
{
    protected static $database = 'default';
    protected static $collection = 'order_pre';


    public static function vip($id = null)
    {
        $vip = parent::find(
            [
                'action' => 'vip',
                'status' => 'accepted'
            ],
            [
                'projection' => [
                    '_id' => 1,
                    'name' => 1,
                    'tip' => 1,
                    'amount' => 1,
                    'memo' => 1,
                    'num' => 1
                ]
            ])->map(function ($item) {
            $item['pre_id'] = $item['_id'];
            unset($item['_id']);
            return $item;
        })->toArray();


        $list = $vip;

        if ($id !== null) {
            $result = parent::findOne([
                '_id' => new ObjectId($id)
            ]);
            return $result;
        }

        return $list;
    }

    public static function balance($id = null)
    {
        $balance = parent::find(
            [
                'action' => 'balance',
                'status' => 'accepted'
            ],
            [
                'projection' => [
                    '_id' => 1,
                    'name' => 1,
                    'amount' => 1,
                    'memo' => 1,
                    'num' => 1
                ]
            ])->map(function ($item) {
            $item['pre_id'] = $item['_id'];
            unset($item['_id']);
            return $item;
        })->toArray();
        $list = $balance;

        if ($id !== null) {
            $result = parent::findOne([
                '_id' => new ObjectId($id)
            ]);
            return $result;
        }

        return $list;
    }
}