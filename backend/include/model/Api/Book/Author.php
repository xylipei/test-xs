<?php


namespace Model\Api\Book;


use Framework\Model;

class Author extends Model
{
    protected static $database = 'default';
    protected static $collection = 'book_author';

    public static function findList(array $filter, array $options, array $tables)
    {
        $books = parent::find($filter, $options)->map(function ($item) use($tables){
            foreach ($item as $k => $v) {
                if (!in_array($k, $tables)) {
                    unset($item[$k]);
                }
            }
            $item['is_hot'] = isset($item['is_hot']) ? $item['is_hot'] : 0;
            $item['author_id'] = $item['_id'];
            unset($item['_id']);
            return $item;
        })->toArray();
        return $books;
    }
}