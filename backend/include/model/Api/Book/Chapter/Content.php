<?php
namespace Model\Api\Book\Chapter;

use Framework\Model;

class Content extends Model
{
    protected static $database = 'default';
    protected static $collection = 'book_chapter_content';

}