<?php


namespace Model\Api\Book;


use Framework\Model;
use Model\Api\Book;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime;

class Bookshelf extends Model
{
    protected static $database = 'default';
    protected static $collection = 'book_shelf';

    public static function updateHistory(ObjectId $chapter_id, ObjectId $book_id, $user)
    {
        $history = parent::findOneAndUpdate(
            [
                'book_id' => $book_id,
                'user_id' => new ObjectId($user['_id']),
            ],
            [
                 '$setOnInsert' => [
                     'user_id' => new ObjectId($user['_id']),
                     'book_id' => $book_id,
                     'is_collect' => 0,
                     'create_time' => new UTCDateTime(),
                     'status' => 'accepted'
                 ],
                 '$set' => [
                     'chapter_id' => $chapter_id,
                     'update_time' => new UTCDateTime()
                 ]
            ],
            [
                 'returnDocument' => 2,
                 'upsert' => true
            ]
        );
        return $history;
    }

    public static function change($book_id_array, $user, $action)
    {
        switch ($action){
            case 'unset':
                $is_collect = 0;
                break;
            default:
                $is_collect = 1;
                break;
        }
        foreach ($book_id_array as $book_id){
            $book = [
                'is_collect' => $is_collect,
                'update_time' => new UTCDateTime()
            ];
            parent::findOneAndUpdate(
                [
                    'user_id' => new ObjectId($user['_id']),
                    'book_id' => $book_id
                ],
                [
                    '$setOnInsert' => [
                        'user_id' => new ObjectId($user['_id']),
                        'book_id' => $book_id,
                        'create_time' => new UTCDateTime(),
                        'status' => 'accepted'
                    ],
                    '$set' => $book
                ],
                [
                    'returnDocument' => 2,
                    'upsert' => true
                ]
            );
        }
        if ($action == 'unset'){
            return self::success('取消成功');
        }else{
            return self::success('已加入书架');
        }
    }


    public static function findHistory(array $filter, array $options)
    {
        $histories = parent::find($filter, $options)->toArray('book_id');
        $book_array = [];
        $chapter_array = [];
        foreach ($histories as $history){
            $book_array[] = new ObjectId($history[0]['book_id']);
            if (!empty($history[0]['chapter_id'])){
                $chapter_array[] = new ObjectId($history[0]['chapter_id']);
            }
        }
        $books = Book::find(
            ['_id' => ['$in' => $book_array]],
            ['projection' => ['_id' => 1, 'title' => 1, 'thumb' => 1, 'content_update_time' => 1, 'num' => 1, 'vip' => 1, 'free_chapter_num' => 1, 'free' => 1, 'chapter_balance_price' => 1]]
        )->map(function($item){
            $item['book_id'] = $item['_id'];
            unset($item['_id']);
            return $item;
        })->toArray();

        $chapters = Chapter::find(
            ['_id' => ['$in' => $chapter_array]],
            ['projection' => ['_id' => 1, 'title' => 1, 'create_time' => 1, 'book_id' => 1]]
        )->map(function($item){
            $item['chapter_id'] = $item['_id'];
            unset($item['_id']);
            return $item;
        })->toArray('book_id');

        if (!empty($books) && !empty($chapters)){
            foreach ($books as $k => $book){
                $history = current($histories[$book['book_id']]);
                $books[$k]['shelf_time'] = $history['create_time'];
                $books[$k]['read_time'] = $history['update_time'];
                $books[$k]['is_collect'] = $history['is_collect'] ?? 0;
                $books[$k]['vip'] = isset($books[$k]['vip']) ? (int) $books[$k]['vip'] : 0;
                $books[$k]['chapter_balance_price'] = isset($books[$k]['chapter_balance_price']) ? $books[$k]['chapter_balance_price'] : CHAPTER_PRICE;
                $books[$k]['free_chapter_num'] = isset($books[$k]['free_chapter_num']) ? $books[$k]['free_chapter_num'] : 200;
                $books[$k]['free'] = isset($books[$k]['free']) ? $books[$k]['free'] : 1;
                $books[$k]['chapter'] = new \stdClass();
                if (!empty($chapters[$book['book_id']])){
                    $chapter =  current($chapters[$book['book_id']]);
                    unset($chapter['book_id']);
                    $books[$k]['chapter'] = $chapter;
                }
            }
        }
        $last_names = array_column($books,'read_time');
        array_multisort($last_names,SORT_DESC, $books);
        return $books;
    }


    public static function success($success)
    {
        return ['error' => 0, 'message' => $success];
    }

    public static function error($message = '操作失败')
    {
        return ['error' => 1, 'message' => $message];
    }
}