<?php


namespace Model\Api\Book;


use Framework\Model;
use MongoDB\BSON\ObjectId;

class Chapter extends Model
{
    protected static $database = 'default';
    protected static $collection = 'book_chapter';

    public static function newChapter($book_id){
        $chapter = parent::findOne(
            ['book_id' => new ObjectId($book_id)],
            [
                'sort' => [
                    'create_time' => -1
                ]
            ]
        );
        return $chapter;
    }

    public static function findWithJoin(array $filter, array $options,array $tables)
    {
        $chapters = parent::find($filter, $options)->map(function ($item) use ($tables){
            foreach ($item as $k => $v) {
                if (!in_array($k, $tables)) {
                    unset($item[$k]);
                }
            }
            $item['chapter_id'] = $item['_id'];
            $item['free'] = isset($item['free']) ? $item['free'] : 0;
            unset($item['_id']);
            return $item;
        })->toArray();

        return $chapters;
    }
}