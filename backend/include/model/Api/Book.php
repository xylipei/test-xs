<?php


namespace Model\Api;

use Framework\Model;
use MongoDB\BSON\ObjectId;

use Model\Api\Book\Bookshelf;
use Model\Api\Book\Chapter;

class Book extends Model
{
    protected static $database = 'default';
    protected static $collection = 'book';

    public static function findWithJoin(array $filter, array $options)
    {
        $books = parent::find($filter, $options)->map(function ($item){
            $item['book_id'] = $item['_id'];
            unset($item['_id']);
            return $item;
        })->toArray();
        return $books;
    }

    public static function findList(array $filter, array $options,$user)
    {
        $tables = ['title', 'thumb', '_id', 'num', 'author', 'memo', 'category', 'finish', 'update_time', 'category_id', 'free', 'free_chapter_num', 'vip','chapter_balance_price'];
        $books = parent::find($filter, $options)->map(function ($item) use($tables, $user){
            foreach ($item as $k => $v) {
                if (!in_array($k, $tables)) {
                    unset($item[$k]);
                }
            }
            $history = Bookshelf::findOne([
                'book_id' => new ObjectId($item['_id']),
                'user_id' => new ObjectId($user['_id'])
            ]);
            $item['is_collect'] = (int) ($history['is_collect']??0);
            $item['history'] = new \stdClass();
            if (!empty($history['chapter_id'])){
                $chapter = Chapter::findOne(['_id' => new ObjectId($history['chapter_id'])]);
                $item['history'] = [
                    'chapter_id' => $history['chapter_id'],
                    'title' => $chapter['title']
                ];
            }
            $item['chapter_balance_price'] = isset($item['chapter_balance_price']) ? $item['chapter_balance_price'] : CHAPTER_PRICE;
            $item['free_chapter_num'] = isset($item['free_chapter_num']) ? $item['free_chapter_num'] : 200;
            $item['free'] = isset($item['free']) ? $item['free'] : 1;
            $item['vip'] = isset($item['vip']) ? (int) $item['vip'] : 0;
            $item['book_id'] = $item['_id'];
            unset($item['_id']);
            return $item;
        })->toArray();
        return $books;
    }

    public static function findOneInfo(array $filter){
        $book = parent::findOneAndUpdate($filter,
            [
                '$inc' => [
                    'num.view' => 1
                ]
            ],
            [
                'returnDocument' => 2,
                'upsert' => true
            ]
        );
        $book['book_id'] = $book['_id'];
        unset($book['_id'], $book['create_time'], $book['update_time'], $book['status']);
        $history = Bookshelf::findOne([
            'book_id' => new ObjectId($book['book_id'])
        ]);
        $book['is_collect'] = (int)$history['is_collect'];
        $new_chapter = Chapter::newChapter($book['book_id']);
        $book['new_chapter'] = [
            'title' => $new_chapter['title'],
            'chapter_id' => $new_chapter['_id'],
            'create_time' => $new_chapter['create_time']
        ];
        $book['history'] = new \stdClass();
        if (!empty($history['chapter_id'])){
            $chapter = Chapter::findOne(['_id' => new ObjectId($history['chapter_id'])]);
            $book['history'] = [
                'chapter_id' => $history['chapter_id'],
                'title' => $chapter['title']
            ];
        }
        $book[' chapter_balance_price'] = isset($book[' chapter_balance_price']) ? $book[' chapter_balance_price'] : CHAPTER_PRICE;
        $book['free_chapter_num'] = isset($book['free_chapter_num']) ? $book['free_chapter_num'] : 200;
        $book['free'] = isset($book['free']) ? $book['free'] : 1;
        return $book;
    }

    public static function randomDate($begintime, $endtime = "", $now = true)
    {
        $begin = strtotime($begintime);
        $end = $endtime == "" ? mktime() : strtotime($endtime);
        $timestamp = rand($begin, $end);
        return $now ? date("Y-m-d H:i:s", $timestamp) : $timestamp;
    }
}