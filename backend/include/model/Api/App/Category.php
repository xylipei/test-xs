<?php
namespace Model\Api\App;

use Framework\Model;


class Category extends Model
{
    protected static $database = 'default';
    protected static $collection = 'app_category';

    public static function findWithJoin(array $filter, array $options, array $tables)
    {
        $books = parent::find($filter, $options)->map(function($item) use ($tables) {
            foreach ($item as $k => $v) {
                if (!in_array($k, $tables)) {
                    unset($item[$k]);
                }
            }
            $item['category_id'] = $item['_id'];
            unset($item['_id']);
            return $item;
        })->toArray();
        return $books;
    }
}