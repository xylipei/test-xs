<?php


namespace Model\Admin;


use Framework\Model;

class App extends Model
{
    protected static $database = 'default';
    protected static $collection = 'app';

    public static function findList(array $filter, array $options)
    {
        $tables = ['app_name', 'apk_url', '_id', 'icon', 'memo', 'package_name'];
        $books = parent::find($filter, $options)->map(function ($item) use($tables){
            foreach ($item as $k => $v) {
                if (!in_array($k, $tables)) {
                    unset($item[$k]);
                }
            }
            $item['app_id'] = $item['_id'];
            unset($item['_id']);
            return $item;
        })->toArray();
        return $books;
    }
}