<?php

namespace Model\Admin;

use Framework\Model;
use Framework\Security\Authenticator;
use MongoDB\BSON\UTCDateTime;

class Account extends Model
{
    protected static $database = 'admin';
    protected static $collection = 'account';

    public static function new(array $account)
    {
        $default = [
            'role' => 'editor',
            'nickname' => '',
            'password' => [
                'str' => ''
            ],
            'authenticator' => [
                'secret' => Authenticator::secret(),
                'bind' => false
            ],
            'status' => 'ok',
            'create_time' => new UTCDateTime(),
            'update_time' => new UTCDateTime(),
        ];

        $account = array_replace_recursive($default, $account);

        return Account::insertOne($account);
    }
}


