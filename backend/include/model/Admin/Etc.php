<?php


namespace Model\Admin;


use Framework\Model;

class Etc extends Model
{
    protected static $database = 'default';
    protected static $collection = 'etc';
}