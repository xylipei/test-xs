<?php

namespace Model\Admin;

use Framework\Model;

class Package extends Model
{
    protected static $database = 'default';
    protected static $collection = 'package';
}