<?php

namespace Model\Admin\History;

use Framework\Model;
use Framework\Security\Authenticator;
use MongoDB\BSON\UTCDateTime;

class Action extends Model
{
    protected static $database = 'admin';
    protected static $collection = 'action';

    public static function new(array $login)
    {
        $default = [
            'account_id' => '',
            'session' => '',
            'action' => '',
            'create_time' => new UTCDateTime(),
        ];

        $login = array_replace_recursive($default, $login);
        $login = array_filter($login);
        return Action::insertOne($login);
    }
}


