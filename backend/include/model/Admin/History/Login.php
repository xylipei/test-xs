<?php

namespace Model\Admin\History;

use Framework\Model;
use Framework\Security\Authenticator;
use MongoDB\BSON\UTCDateTime;

class Login extends Model
{
    protected static $database = 'admin';
    protected static $collection = 'login';

    public static function new(array $login)
    {
        $default = [
            'account_id' => '',
            'nickname' => '',
            'password' => '',
            'ip' => '',
            'location' => '',
            'user_agent' => '',
            'status' => 'fail',
            'create_time' => new UTCDateTime(),
        ];

        $login = array_replace_recursive($default, $login);
        $login = array_filter($login);
        return Login::insertOne($login);
    }
}


